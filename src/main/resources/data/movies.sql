INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('25545084-6b02-4643-87fb-1c58b6251c32','The Shawshank Redemption','1994','MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1_SY500_CR0,0,336,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('25545084-6b02-4643-87fb-1c58b6251c32','Chronicles the experiences of a formerly successful banker as a prisoner in the gloomy jailhouse of Shawshank after being found guilty of a crime he did not commit. The film portrays the man''s unique way of dealing with his new, torturous life; along the way he befriends a number of fellow prisoners, most notably a wise long-term inmate named Red.                Written by J-S-Golden tags: Crime Drama Tim Robbins Morgan Freeman Bob Gunton ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('094499d3-5cf0-4fe9-ac8d-e62c1a243a96','The Godfather','1972','MV5BZTRmNjQ1ZDYtNDgzMy00OGE0LWE4N2YtNTkzNWQ5ZDhlNGJmL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,352,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('094499d3-5cf0-4fe9-ac8d-e62c1a243a96','When the aging head of a famous crime family decides to transfer his position to one of his subalterns, a series of unfortunate events start happening to the family, and a war begins between all the well-known families leading to insolence, deportation, murder and revenge, and ends with the favorable successor being finally chosen.                Written by J. S. Golden tags: Crime Drama Marlon Brando Al Pacino James Caan ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('875d4555-4e4f-49a2-abd3-c17fe5d1cc8c','The Godfather: Part II','1974','MV5BMjZiNzIxNTQtNDc5Zi00YWY1LThkMTctMDgzYjY4YjI1YmQyL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,351,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('875d4555-4e4f-49a2-abd3-c17fe5d1cc8c','The continuing saga of the Corleone crime family tells the story of a young Vito Corleone growing up in Sicily and in 1910s New York; and follows Michael Corleone in the 1950s as he attempts to expand the family business into Las Vegas, Hollywood and Cuba.                Written by Keith Loh <loh@sfu.ca> tags: Crime Drama Al Pacino Robert De Niro Robert Duvall ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('5fe68a14-bc83-4d7c-b80c-b3b0c2f529f5','The Dark Knight','2008','MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('5fe68a14-bc83-4d7c-b80c-b3b0c2f529f5','Set within a year after the events of Batman Begins, Batman, Lieutenant James Gordon, and new district attorney Harvey Dent successfully begin to round up the criminals that plague Gotham City until a mysterious and sadistic criminal mastermind known only as the Joker appears in Gotham, creating a new wave of chaos. Batman''s struggle against the Joker becomes deeply personal, forcing him to "confront everything he believes" and improve his technology to stop him. A love triangle develops between Bruce Wayne, Dent and Rachel Dawes.                Written by Leon Lombardi tags: Action Crime Drama Christian Bale Heath Ledger Aaron Eckhart ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('310365fd-7bba-4e94-808d-e06f7fb85191','12 Angry Men','1957','MV5BODQwOTc5MDM2N15BMl5BanBnXkFtZTcwODQxNTEzNA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('310365fd-7bba-4e94-808d-e06f7fb85191','The defense and the prosecution have rested and the jury is filing into the jury room to decide if a young man is guilty or innocent of murdering his father. What begins as an open-and-shut case of murder soon becomes a detective story that presents a succession of clues creating doubt, and a mini-drama of each of the jurors'' prejudices and preconceptions about the trial, the accused, and each other. Based on the play, all of the action takes place on the stage of the jury room.                Written by pjk <PETESID@VNET.IBM.COM> tags: Crime Drama Henry Fonda Lee J. Cobb Martin Balsam ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('b0028749-a32b-4385-850a-144ad5b052db','Schindler''s List','1993','MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('b0028749-a32b-4385-850a-144ad5b052db','Oskar Schindler is a vainglorious and greedy German businessman who becomes an unlikely humanitarian amid the barbaric German Nazi reign when he feels compelled to turn his factory into a refuge for Jews. Based on the true story of Oskar Schindler who managed to save about 1100 Jews from being gassed at the Auschwitz concentration camp, it is a testament to the good in all of us.                Written by Harald Mayr <marvin@bike.augusta.de> tags: Biography Drama History Liam Neeson Ralph Fiennes Ben Kingsley ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('9000afd7-602e-447e-bb0c-f9bd30ee5932','Pulp Fiction','1994','MV5BMTkxMTA5OTAzMl5BMl5BanBnXkFtZTgwNjA5MDc3NjE@._V1_SY500_CR0,0,336,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('9000afd7-602e-447e-bb0c-f9bd30ee5932','Jules Winnfield (Samuel L. Jackson) and Vincent Vega (John Travolta) are two hit men who are out to retrieve a suitcase stolen from their employer, mob boss Marsellus Wallace (Ving Rhames). Wallace has also asked Vincent to take his wife Mia (Uma Thurman) out a few days later when Wallace himself will be out of town. Butch Coolidge (Bruce Willis) is an aging boxer who is paid by Wallace to lose his weight. The lives of these seemingly unrelated people are woven together comprising of a series of funny, bizarre and uncalled-for incidents.                Written by Soumitra tags: Crime Drama John Travolta Uma Thurman Samuel L. Jackson ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('c3f26999-8566-433e-b9d8-e550d2f58f5d','The Lord of the Rings: The Return of the King','2003','MV5BYWY1ZWQ5YjMtMDE0MS00NWIzLWE1M2YtODYzYTk2OTNlYWZmXkEyXkFqcGdeQXVyNDUyOTg3Njg@._V1_SY500_SX334_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('c3f26999-8566-433e-b9d8-e550d2f58f5d','The final confrontation between the forces of good and evil fighting for control of the future of Middle-earth. Hobbits Frodo and Sam reach Mordor in their quest to destroy the "one ring", while Aragorn leads the forces of good against Sauron''s evil army at the stone city of Minas Tirith.                Written by Jwelch5742 tags: Adventure Drama Fantasy Elijah Wood Viggo Mortensen Ian McKellen ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('8de7bf0c-ad58-447c-84ce-489227a02402','Il buono, il brutto, il cattivo','1966','MV5BOTQ5NDI3MTI4MF5BMl5BanBnXkFtZTgwNDQ4ODE5MDE@._V1_SY500_CR0,0,328,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('8de7bf0c-ad58-447c-84ce-489227a02402','Blondie (The Good) is a professional gunslinger who is out trying to earn a few dollars. Angel Eyes (The Bad) is a hit man who always commits to a task and sees it through, as long as he is paid to do so. And Tuco (The Ugly) is a wanted outlaw trying to take care of his own hide. Tuco and Blondie share a partnership together making money off Tuco''s bounty, but when Blondie unties the partnership, Tuco tries to hunt down Blondie. When Blondie and Tuco come across a horse carriage loaded with dead bodies, they soon learn from the only survivor (Bill Carson) that he and a few other men have buried a stash of gold in a cemetery. Unfortunately Carson dies and Tuco only finds out the name of the cemetery, while Blondie finds out the name on the grave. Now the two must keep each other alive in order to find the gold. Angel Eyes (who had been looking for Bill Carson) discovers that Tuco and Blondie met with Carson and knows they know the location of the gold. All he needs is for the two to ...                Written by Jeremy Thomson tags: Western Clint Eastwood Eli Wallach Lee Van Cleef ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('51fa6e32-50af-4186-87b5-679badb0247b','Fight Club','1999','MV5BZGY5Y2RjMmItNDg5Yy00NjUwLThjMTEtNDc2OGUzNTBiYmM1XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX290_CR0,0,290,429_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('51fa6e32-50af-4186-87b5-679badb0247b','A nameless first person narrator (Edward Norton) attends support groups in attempt to subdue his emotional state and relieve his insomniac state. When he meets Marla (Helena Bonham Carter), another fake attendee of support groups, his life seems to become a little more bearable. However when he associates himself with Tyler (Brad Pitt) he is dragged into an underground fight club and soap making scheme. Together the two men spiral out of control and engage in competitive rivalry for love and power. When the narrator is exposed to the hidden agenda of Tyler''s fight club, he must accept the awful truth that Tyler may not be who he says he is.                Written by Rhiannon tags: Drama Brad Pitt Edward Norton Meat Loaf ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('a6208e6c-3914-423a-a1aa-bfa22fe8c768','The Lord of the Rings: The Fellowship of the Ring','2001','MV5BNmFmZDdkODMtNzUyMy00NzhhLWFjZmEtMGMzYjNhMDA1NTBkXkEyXkFqcGdeQXVyNDUyOTg3Njg@._V1_SY500_SX342_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('a6208e6c-3914-423a-a1aa-bfa22fe8c768','An ancient Ring thought lost for centuries has been found, and through a strange twist in fate has been given to a small Hobbit named Frodo. When Gandalf discovers the Ring is in fact the One Ring of the Dark Lord Sauron, Frodo must make an epic quest to the Cracks of Doom in order to destroy it! However he does not go alone. He is joined by Gandalf, Legolas the elf, Gimli the Dwarf, Aragorn, Boromir and his three Hobbit friends Merry, Pippin and Samwise. Through mountains, snow, darkness, forests, rivers and plains, facing evil and danger at every corner the Fellowship of the Ring must go. Their quest to destroy the One Ring is the only hope for the end of the Dark Lords reign!                Written by Paul Twomey <toomsp@hotmail.com> tags: Adventure Drama Fantasy Elijah Wood Ian McKellen Orlando Bloom ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('495f64e3-3aa9-4a84-b72b-855187296f35','Star Wars: Episode V - The Empire Strikes Back','1980','MV5BYmViY2M2MTYtY2MzOS00YjQ1LWIzYmEtOTBiNjhlMGM0NjZjXkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY500_CR0,0,322,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('495f64e3-3aa9-4a84-b72b-855187296f35','Luke Skywalker, Han Solo, Princess Leia and Chewbacca face attack by the Imperial forces and its AT-AT walkers on the ice planet Hoth. While Han and Leia escape in the Millennium Falcon, Luke travels to Dagobah in search of Yoda. Only with the Jedi master''s help will Luke survive when the dark side of the Force beckons him into the ultimate duel with Darth Vader.                Written by Jwelch5742 tags: Action Adventure Fantasy Mark Hamill Harrison Ford Carrie Fisher ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('3089bf6e-4e9c-4afe-a607-576dcf59cf08','Forrest Gump','1994','MV5BYThjM2MwZGMtMzg3Ny00NGRkLWE4M2EtYTBiNWMzOTY0YTI4XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY500_CR0,0,378,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('3089bf6e-4e9c-4afe-a607-576dcf59cf08','Forrest Gump is a simple man with a low I.Q. but good intentions. He is running through childhood with his best and only friend Jenny. His ''mama'' teaches him the ways of life and leaves him to choose his destiny. Forrest joins the army for service in Vietnam, finding new friends called Dan and Bubba, he wins medals, creates a famous shrimp fishing fleet, inspires people to jog, starts a ping-pong craze, creates the smiley, writes bumper stickers and songs, donates to people and meets the president several times. However, this is all irrelevant to Forrest who can only think of his childhood sweetheart Jenny Curran, who has messed up her life. Although in the end all he wants to prove is that anyone can love anyone.                Written by aliw135 tags: Comedy Drama Romance Tom Hanks Robin Wright Gary Sinise ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('92ea1208-6482-4c4b-8531-46bb77984a02','Inception','2010','MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('92ea1208-6482-4c4b-8531-46bb77984a02','Dom Cobb is a skilled thief, the absolute best in the dangerous art of extraction, stealing valuable secrets from deep within the subconscious during the dream state, when the mind is at its most vulnerable. Cobb''s rare ability has made him a coveted player in this treacherous new world of corporate espionage, but it has also made him an international fugitive and cost him everything he has ever loved. Now Cobb is being offered a chance at redemption. One last job could give him his life back but only if he can accomplish the impossible - inception. Instead of the perfect heist, Cobb and his team of specialists have to pull off the reverse: their task is not to steal an idea but to plant one. If they succeed, it could be the perfect crime. But no amount of careful planning or expertise can prepare the team for the dangerous enemy that seems to predict their every move. An enemy that only Cobb could have seen coming.                Written by Warner Bros. Pictures tags: Action Adventure Sci-Fi Leonardo DiCaprio Joseph Gordon-Levitt Ellen Page ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('9bc21cb0-21b3-4323-806e-c76d39709d96','The Lord of the Rings: The Two Towers','2002','MV5BMTAyNDU0NjY4NTheQTJeQWpwZ15BbWU2MDk4MTY2Nw@@._V1_SY256_SX175_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('9bc21cb0-21b3-4323-806e-c76d39709d96','While Frodo and Sam, now accompanied by a new guide, continue their hopeless journey towards the land of shadow to destroy the One Ring, each member of the broken fellowship plays their part in the battle against the evil wizard Saruman and his armies of Isengard. tags: Adventure Drama Fantasy Elijah Wood Ian McKellen Viggo Mortensen ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d29c23bc-3c20-4aed-9090-53c25e9b228d','One Flew Over the Cuckoo''s Nest','1975','MV5BZjA0OWVhOTAtYWQxNi00YzNhLWI4ZjYtNjFjZTEyYjJlNDVlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,336,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d29c23bc-3c20-4aed-9090-53c25e9b228d','McMurphy has a criminal past and has once again gotten himself into trouble and is sentenced by the court. To escape labor duties in prison, McMurphy pleads insanity and is sent to a ward for the mentally unstable. Once here, McMurphy both endures and stands witness to the abuse and degradation of the oppressive Nurse Ratched, who gains superiority and power through the flaws of the other inmates. McMurphy and the other inmates band together to make a rebellious stance against the atrocious Nurse.                Written by Jacob Oberfrank tags: Drama Jack Nicholson Louise Fletcher Michael Berryman ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('5a1bc126-7de7-40aa-93ce-d78191180175','Goodfellas','1990','MV5BNThjMzczMjctZmIwOC00NTQ4LWJhZWItZDdhNTk5ZTdiMWFlXkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY500_CR0,0,334,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('5a1bc126-7de7-40aa-93ce-d78191180175','Henry Hill might be a small time gangster, who may have taken part in a robbery with Jimmy Conway and Tommy De Vito, two other gangsters who might have set their sights a bit higher. His two partners could kill off everyone else involved in the robbery, and slowly start to think about climbing up through the hierarchy of the Mob. Henry, however, might be badly affected by his partners'' success, but will he consider stooping low enough to bring about the downfall of Jimmy and Tommy?                Written by Colin Tinto <cst@imdb.com> tags: Biography Crime Drama Robert De Niro Ray Liotta Joe Pesci ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('7954f2da-c406-4fd1-8ff9-18e1a8064dac','The Matrix','1999','MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('7954f2da-c406-4fd1-8ff9-18e1a8064dac','Thomas A. Anderson is a man living two lives. By day he is an average computer programmer and by night a hacker known as Neo. Neo has always questioned his reality, but the truth is far beyond his imagination. Neo finds himself targeted by the police when he is contacted by Morpheus, a legendary computer hacker branded a terrorist by the government. Morpheus awakens Neo to the real world, a ravaged wasteland where most of humanity have been captured by a race of machines that live off of the humans'' body heat and electrochemical energy and who imprison their minds within an artificial reality known as the Matrix. As a rebel against the machines, Neo must return to the Matrix and confront the agents: super-powerful computer programs devoted to snuffing out Neo and the entire human rebellion.                Written by redcommander27 tags: Action Sci-Fi Keanu Reeves Laurence Fishburne Carrie-Anne Moss ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('8f5fb870-a2e2-45b9-be18-cc7b8c937158','Shichinin no samurai','1954','MV5BMTc5MDY1MjU5MF5BMl5BanBnXkFtZTgwNDM2OTE4MzE@._V1_SY500_CR0,0,356,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('8f5fb870-a2e2-45b9-be18-cc7b8c937158','A veteran samurai, who has fallen on hard times, answers a village''s request for protection from bandits. He gathers 6 other samurai to help him, and they teach the townspeople how to defend themselves, and they supply the samurai with three small meals a day. The film culminates in a giant battle when 40 bandits attack the village.                Written by Colin Tinto <cst@imdb.com> tags: Adventure Drama Toshirô Mifune Takashi Shimura Keiko Tsushima ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('9cd593e0-d1fa-4d69-aca6-4cf974a91b49','Star Wars','1977','MV5BYzQ2OTk4N2QtOGQwNy00MmI3LWEwNmEtOTk0OTY3NDk2MGJkL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('9cd593e0-d1fa-4d69-aca6-4cf974a91b49','The Imperial Forces, under orders from cruel Darth Vader, hold Princess Leia hostage in their efforts to quell the rebellion against the Galactic Empire. Luke Skywalker and Han Solo, captain of the Millennium Falcon, work together with the companionable droid duo R2-D2 and C-3PO to rescue the beautiful princess, help the Rebel Alliance and restore freedom and justice to the Galaxy.                Written by Jwelch5742 tags: Action Adventure Fantasy Mark Hamill Harrison Ford Carrie Fisher ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('cd55e24d-65be-4f6f-8d33-fb3f28f02433','Cidade de Deus','2002','MV5BMjA4ODQ3ODkzNV5BMl5BanBnXkFtZTYwOTc4NDI3._V1_SX200_CR0,0,200,296_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('cd55e24d-65be-4f6f-8d33-fb3f28f02433','Brazil, 1960s, City of God. The Tender Trio robs motels and gas trucks. Younger kids watch and learn well...too well. 1970s: Li''l Zé has prospered very well and owns the city. He causes violence and fear as he wipes out rival gangs without mercy. His best friend Bené is the only one to keep him on the good side of sanity. Rocket has watched these two gain power for years, and he wants no part of it. Yet he keeps getting swept up in the madness. All he wants to do is take pictures. 1980s: Things are out of control between the last two remaining gangs...will it ever end? Welcome to the City of God.                Written by Jeff Mellinger <jeffmellinger@astound.net> tags: Crime Drama Alexandre Rodrigues Matheus Nachtergaele Leandro Firmino ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('dd31b52d-4873-4c22-a398-2a5c630bdb68','Se7en','1995','MV5BOTUwODM5MTctZjczMi00OTk4LTg3NWUtNmVhMTAzNTNjYjcyXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,319,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('dd31b52d-4873-4c22-a398-2a5c630bdb68','A film about two homicide detectives'' (Morgan Freeman and (Brad Pitt desperate hunt for a serial killer who justifies his crimes as absolution for the world''s ignorance of the Seven Deadly Sins. The movie takes us from the tortured remains of one victim to the next as the sociopathic "John Doe" (Kevin Spacey) sermonizes to Detectives Somerset and Mills -- one sin at a time. The sin of Gluttony comes first and the murderer''s terrible capacity is graphically demonstrated in the dark and subdued tones characteristic of film noir. The seasoned and cultured but jaded Somerset researches the Seven Deadly Sins in an effort to understand the killer''s modus operandi while the bright but green and impulsive Detective Mills (Pitt) scoffs at his efforts to get inside the mind of a killer...                Written by Mark Fleetwood <mfleetwo@mail.coin.missouri.edu> tags: Crime Drama Mystery Morgan Freeman Brad Pitt Kevin Spacey ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('6322c7e0-4649-43b1-af1a-2701d95ee787','The Silence of the Lambs','1991','MV5BMTQ2NzkzMDI4OF5BMl5BanBnXkFtZTcwMDA0NzE1NA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('6322c7e0-4649-43b1-af1a-2701d95ee787','FBI trainee Clarice Starling works hard to advance her career, including trying to hide or put behind her West Virginia roots, of which if some knew would automatically classify her as being backward or white trash. After graduation, she aspires to work in the agency''s Behavioral Science Unit under the leadership of Jack Crawford. While she is still a trainee, Crawford does ask her to question Dr. Hannibal Lecter, a psychiatrist imprisoned thus far for eight years in maximum security isolation for being a serial killer, he who cannibalized his victims. Clarice is able to figure out the assignment is to pick Lecter''s brains to help them solve another serial murder case, that of someone coined by the media as Buffalo Bill who has so far killed five victims, all located in the eastern US, all young women who are slightly overweight especially around the hips, all who were drowned in natural bodies of water, and all who were stripped of large swaths of skin. She also figures that Crawford...                Written by Huggo tags: Crime Drama Thriller Jodie Foster Anthony Hopkins Lawrence A. Bonney ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('bca56d92-ee97-4ede-b39b-5c10eca4bef1','It''s a Wonderful Life','1946','MV5BMTMzMzY5NDc4M15BMl5BanBnXkFtZTcwMzc4NjIxNw@@._V1_SY500_CR0,0,334,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('bca56d92-ee97-4ede-b39b-5c10eca4bef1','George Bailey has spent his entire life giving of himself to the people of Bedford Falls. He has always longed to travel but never had the opportunity in order to prevent rich skinflint Mr. Potter from taking over the entire town. All that prevents him from doing so is George''s modest building and loan company, which was founded by his generous father. But on Christmas Eve, George''s Uncle Billy loses the business''s $8,000 while intending to deposit it in the bank. Potter finds the misplaced money and hides it from Billy. When the bank examiner discovers the shortage later that night, George realizes that he will be held responsible and sent to jail and the company will collapse, finally allowing Potter to take over the town. Thinking of his wife, their young children, and others he loves will be better off with him dead, he contemplates suicide. But the prayers of his loved ones result in a gentle angel named Clarence coming to earth to help George, with the promise of earning his ...                Written by alfiehitchie tags: Drama Family Fantasy James Stewart Donna Reed Lionel Barrymore ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('09869df4-5f5c-48b2-83e3-c0ee547b7dec','The Usual Suspects','1995','MV5BMzI1MjI5MDQyOV5BMl5BanBnXkFtZTcwNzE4Mjg3NA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('09869df4-5f5c-48b2-83e3-c0ee547b7dec','Following a truck hijack in New York, five conmen are arrested and brought together for questioning. As none of them are guilty, they plan a revenge operation against the police. The operation goes well, but then the influence of a legendary mastermind criminal called Keyser Söze is felt. It becomes clear that each one of them has wronged Söze at some point and must pay back now. The payback job leaves 27 men dead in a boat explosion, but the real question arises now: Who actually is Keyser Söze?                Written by Soumitra tags: Crime Drama Mystery Kevin Spacey Gabriel Byrne Chazz Palminteri ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('3dcafaf0-1b97-4747-8feb-9b16cdff5250','La vita è bella','1997','MV5BYmJmM2Q4NmMtYThmNC00ZjRlLWEyZmItZTIwOTBlZDQ3NTQ1XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_SX335_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('3dcafaf0-1b97-4747-8feb-9b16cdff5250','In 1930s Italy, a carefree Jewish book keeper named Guido starts a fairy tale life by courting and marrying a lovely woman from a nearby city. Guido and his wife have a son and live happily together until the occupation of Italy by German forces. In an attempt to hold his family together and help his son survive the horrors of a Jewish Concentration Camp, Guido imagines that the Holocaust is a game and that the grand prize for winning is a tank.                Written by Anthony Hughes <husnock31@hotmail.com> tags: Comedy Drama War Roberto Benigni Nicoletta Braschi Giorgio Cantarini ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('35715110-2d14-472d-b377-332e9896058a','Léon','1994','MV5BMjdjMGU3MGYtN2Y5ZC00MTE4LWE4YWQtMTBhMjc3MTk0ZDUwXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_SX332_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('35715110-2d14-472d-b377-332e9896058a','After her father, mother, older sister and little brother are killed by her father''s employers, the 12-year-old daughter of an abject drug dealer is forced to take refuge in the apartment of a professional hitman who at her request teaches her the methods of his job so she can take her revenge on the corrupt DEA agent who ruined her life by killing her beloved brother.                Written by J. S. Golden tags: Crime Drama Thriller Jean Reno Gary Oldman Natalie Portman ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('1fabb1d1-cf56-4f05-a54b-ea7b147d96c6','Sen to Chihiro no kamikakushi','2001','MV5BNmU5OTQ0OWQtOTY0OS00Yjg4LWE1NDYtNDRhYWMxYWY4OTMwXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,352,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('1fabb1d1-cf56-4f05-a54b-ea7b147d96c6','Chihiro and her parents are moving to a small Japanese town in the countryside, much to Chihiro''s dismay. On the way to their new home, Chihiro''s father makes a wrong turn and drives down a lonely one-lane road which dead-ends in front of a tunnel. Her parents decide to stop the car and explore the area. They go through the tunnel and find an abandoned amusement park on the other side, with its own little town. When her parents see a restaurant with great-smelling food but no staff, they decide to eat and pay later. However, Chihiro refuses to eat and decides to explore the theme park a bit more. She meets a boy named Haku who tells her that Chihiro and her parents are in danger, and they must leave immediately. She runs to the restaurant and finds that her parents have turned into pigs. In addition, the theme park turns out to be a town inhabited by demons, spirits, and evil gods. At the center of the town is a bathhouse where these creatures go to relax. The owner of the bathhouse ...                Written by Zachary Harper tags: Animation Adventure Family Daveigh Chase Suzanne Pleshette Miyu Irino ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('b1f054e1-2cd3-4c57-a4b5-c8a688e97732','Saving Private Ryan','1998','MV5BZjhkMDM4MWItZTVjOC00ZDRhLThmYTAtM2I5NzBmNmNlMzI1XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY500_CR0,0,339,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('b1f054e1-2cd3-4c57-a4b5-c8a688e97732','Opening with the Allied invasion of Normandy on 6 June 1944, members of the 2nd Ranger Battalion under Cpt. Miller fight ashore to secure a beachhead. Amidst the fighting, two brothers are killed in action. Earlier in New Guinea, a third brother is KIA. Their mother, Mrs. Ryan, is to receive all three of the grave telegrams on the same day. The United States Army Chief of Staff, George C. Marshall, is given an opportunity to alleviate some of her grief when he learns of a fourth brother, Private James Ryan, and decides to send out 8 men (Cpt. Miller and select members from 2nd Rangers) to find him and bring him back home to his mother...                Written by J.Zelman tags: Drama War Tom Hanks Matt Damon Tom Sizemore ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('8f7eab8b-c6fb-449b-9850-5e984275f240','C''era una volta il West','1968','MV5BYjFiOTlmMzgtOGZlYi00NjY0LThmMzEtNmQ0OTgxNGViOTZmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY500_CR0,0,329,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('8f7eab8b-c6fb-449b-9850-5e984275f240','Story of a young woman, Mrs. McBain, who moves from New Orleans to frontier Utah, on the very edge of the American West. She arrives to find her new husband and family slaughtered, but by whom? The prime suspect, coffee-lover Cheyenne, befriends her and offers to go after the real killer, assassin gang leader Frank, in her honor. He is accompanied by Harmonica, a man already on a quest to get even.                Written by DrGoodBeat / edited by statmanjeff tags: Western Henry Fonda Charles Bronson Claudia Cardinale ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('41b04dd3-481f-43c2-8489-779efeacac6a','American History X','1998','MV5BY2E2MWU5ZDktOTQ0OS00NGIzLTg4YTgtYjFhNTRlMjA1NzFjL2ltYWdlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY500_SX340_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('41b04dd3-481f-43c2-8489-779efeacac6a','Derek Vineyard is paroled after serving 3 years in prison for brutally killing two black men who tried to break into/steal his truck. Through his brother, Danny Vineyard''s narration, we learn that before going to prison, Derek was a skinhead and the leader of a violent white supremacist gang that committed acts of racial crime throughout L.A. and his actions greatly influenced Danny. Reformed and fresh out of prison, Derek severs contact with the gang and becomes determined to keep Danny from going down the same violent path as he did.                Written by Nitesh D.(nmxpa7@msn.com) tags: Crime Drama Edward Norton Edward Furlong Beverly D''Angelo ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('16a6da6c-a9ed-4c01-b31f-b9e58fe74ea4','Interstellar','2014','MV5BMjIxNTU4MzY4MF5BMl5BanBnXkFtZTgwMzM4ODI3MjE@._V1_SY500_CR0,0,320,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('16a6da6c-a9ed-4c01-b31f-b9e58fe74ea4','Earth''s future has been riddled by disasters, famines, and droughts. There is only one way to ensure mankind''s survival: Interstellar travel. A newly discovered wormhole in the far reaches of our solar system allows a team of astronauts to go where no man has gone before, a planet that may have the right environment to sustain human life. tags: Adventure Drama Sci-Fi Matthew McConaughey Anne Hathaway Jessica Chastain ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('bc4b5c7a-f442-40e4-b446-d956d1326c2f','Casablanca','1942','MV5BY2IzZGY2YmEtYzljNS00NTM5LTgwMzUtMzM1NjQ4NGI0OTk0XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY377_SX250_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('bc4b5c7a-f442-40e4-b446-d956d1326c2f','In World War II Casablanca, Rick Blaine, exiled American and former freedom fighter, runs the most popular nightspot in town. The cynical lone wolf Blaine comes into the possession of two valuable letters of transit. When Nazi Major Strasser arrives in Casablanca, the sycophantic police Captain Renault does what he can to please him, including detaining a Czechoslovak underground leader Victor Laszlo. Much to Rick''s surprise, Lazslo arrives with Ilsa, Rick''s one time love. Rick is very bitter towards Ilsa, who ran out on him in Paris, but when he learns she had good reason to, they plan to run off together again using the letters of transit. Well, that was their original plan....                Written by Gary Jackson <garyjack5@cogeco.ca> tags: Drama Romance War Humphrey Bogart Ingrid Bergman Paul Henreid ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('0508de18-9223-425f-859c-df1c621cde0b','Psycho','1960','MV5BMDI3OWRmOTEtOWJhYi00N2JkLTgwNGItMjdkN2U0NjFiZTYwXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,331,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('0508de18-9223-425f-859c-df1c621cde0b','Phoenix officeworker Marion Crane is fed up with the way life has treated her. She has to meet her lover Sam in lunch breaks and they cannot get married because Sam has to give most of his money away in alimony. One Friday Marion is trusted to bank $40,000 by her employer. Seeing the opportunity to take the money and start a new life, Marion leaves town and heads towards Sam''s California store. Tired after the long drive and caught in a storm, she gets off the main highway and pulls into The Bates Motel. The motel is managed by a quiet young man called Norman who seems to be dominated by his mother.                Written by Col Needham <col@imdb.com> tags: Horror Mystery Thriller Anthony Perkins Janet Leigh Vera Miles ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('e02243fa-6c1f-4ab8-9b27-f328297a50d1','City Lights','1931','MV5BZDRjMmI3ZjgtMGE3Mi00NjY5LTg0NWMtMmU3YzgyMjhmMjIzL2ltYWdlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY500_CR0,0,330,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('e02243fa-6c1f-4ab8-9b27-f328297a50d1','A tramp falls in love with a beautiful blind girl. Her family is in financial trouble. The tramp''s on-and-off friendship with a wealthy man allows him to be the girl''s benefactor and suitor.                Written by John J. Magee <magee@helix.mgh.harvard.edu> tags: Comedy Drama Romance Charles Chaplin Virginia Cherrill Florence Lee ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('a34349c1-a090-4785-bcbe-b5aee7b80b9c','The Green Mile','1999','MV5BMTUxMzQyNjA5MF5BMl5BanBnXkFtZTYwOTU2NTY3._V1_SY370_SX250_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('a34349c1-a090-4785-bcbe-b5aee7b80b9c','Death Row guards at a penitentiary, in the 1930''s, have a moral dilemma with their job when they discover one of their prisoners, a convicted murderer, has a special gift.                Written by Guy Johns tags: Crime Drama Fantasy Tom Hanks Michael Clarke Duncan David Morse ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('e38bf670-7cd8-449c-aece-9fb45e0b04db','Intouchables','2011','MV5BMTYxNDA3MDQwNl5BMl5BanBnXkFtZTcwNTU4Mzc1Nw@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('e38bf670-7cd8-449c-aece-9fb45e0b04db','In Paris, the aristocratic and intellectual Philippe is a quadriplegic millionaire who is interviewing candidates for the position of his carer, with his red-haired secretary Magalie. Out of the blue, the rude African Driss cuts the line of candidates and brings a document from the Social Security and asks Phillipe to sign it to prove that he is seeking a job position so he can receive his unemployment benefit. Philippe challenges Driss, offering him a trial period of one month to gain experience helping him. Then Driss can decide whether he would like to stay with him or not. Driss accepts the challenge and moves to the mansion, changing the boring life of Phillipe and his employees.                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Biography Comedy Drama François Cluzet Omar Sy Anne Le Ny ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('8d939f4a-aa4c-4ab4-b8c1-93e6f723c11b','Modern Times','1936','MV5BYjJiZjMzYzktNjU0NS00OTkxLWEwYzItYzdhYWJjN2QzMTRlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY450_SX289_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('8d939f4a-aa4c-4ab4-b8c1-93e6f723c11b','Chaplins last ''silent'' film, filled with sound effects, was made when everyone else was making talkies. Charlie turns against modern society, the machine age, (The use of sound in films ?) and progress. Firstly we see him frantically trying to keep up with a production line, tightening bolts. He is selected for an experiment with an automatic feeding machine, but various mishaps leads his boss to believe he has gone mad, and Charlie is sent to a mental hospital... When he gets out, he is mistaken for a communist while waving a red flag, sent to jail, foils a jailbreak, and is let out again. We follow Charlie through many more escapades before the film is out.                Written by Colin Tinto <cst@imdb.com> tags: Comedy Drama Family Charles Chaplin Paulette Goddard Henry Bergman ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('8d3570b6-dac7-4cef-8a22-07fa17c9a6e5','Raiders of the Lost Ark','1981','MV5BMjA0ODEzMTc1Nl5BMl5BanBnXkFtZTcwODM2MjAxNA@@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('8d3570b6-dac7-4cef-8a22-07fa17c9a6e5','The year is 1936. An archeology professor named Indiana Jones is venturing in the jungles of South America searching for a golden statue. Unfortunately, he sets off a deadly trap but miraculously escapes. Then, Jones hears from a museum curator named Marcus Brody about a biblical artifact called The Ark of the Covenant, which can hold the key to humanly existence. Jones has to venture to vast places such as Nepal and Egypt to find this artifact. However, he will have to fight his enemy Rene Belloq and a band of Nazis in order to reach it.                Written by John Wiggins tags: Action Adventure Harrison Ford Karen Allen Paul Freeman ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d97e1c5d-7e59-47c0-8050-ffdfc3a03fc9','Rear Window','1954','MV5BNGUxYWM3M2MtMGM3Mi00ZmRiLWE0NGQtZjE5ODI2OTJhNTU0XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,341,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d97e1c5d-7e59-47c0-8050-ffdfc3a03fc9','Professional photographer L.B. "Jeff" Jeffries breaks his leg while getting an action shot at an auto race. Confined to his New York apartment, he spends his time looking out of the rear window observing the neighbors. He begins to suspect that a man across the courtyard may have murdered his wife. Jeff enlists the help of his high society fashion-consultant girlfriend Lisa Freemont and his visiting nurse Stella to investigate.                Written by Col Needham <col@imdb.com> tags: Mystery Thriller James Stewart Grace Kelly Wendell Corey ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('5d681820-6fbd-4175-80ec-48672473d2ef','The Pianist','2002','MV5BOWRiZDIxZjktMTA1NC00MDQ2LWEzMjUtMTliZmY3NjQ3ODJiXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,362,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('5d681820-6fbd-4175-80ec-48672473d2ef','In this adaptation of the autobiography "The Pianist: The Extraordinary True Story of One Man''s Survival in Warsaw, 1939-1945," Wladyslaw Szpilman, a Polish Jewish radio station pianist, sees Warsaw change gradually as World War II begins. Szpilman is forced into the Warsaw Ghetto, but is later separated from his family during Operation Reinhard. From this time until the concentration camp prisoners are released, Szpilman hides in various locations among the ruins of Warsaw.                Written by Jwelch5742 tags: Biography Drama War Adrien Brody Thomas Kretschmann Frank Finlay ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('69b76bc7-2dc9-4c9b-8f70-69175698af9b','The Departed','2006','MV5BMTI1MTY2OTIxNV5BMl5BanBnXkFtZTYwNjQ4NjY3._V1_SX225_CR0,0,225,332_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('69b76bc7-2dc9-4c9b-8f70-69175698af9b','In South Boston, the state police force is waging war on Irish-American organized crime. Young undercover cop Billy Costigan is assigned to infiltrate the mob syndicate run by gangland chief Frank Costello. While Billy quickly gains Costello''s confidence, Colin Sullivan, a hardened young criminal who has infiltrated the state police as an informer for the syndicate is rising to a position of power in the Special Investigation Unit. Each man becomes deeply consumed by their double lives, gathering information about the plans and counter-plans of the operations they have penetrated. But when it becomes clear to both the mob and the police that there is a mole in their midst, Billy and Colin are suddenly in danger of being caught and exposed to the enemy - and each must race to uncover the identity of the other man in time to save themselves. But is either willing to turn on their friends and comrades they''ve made during their long stints undercover?                Written by Anonymous tags: Crime Drama Thriller Leonardo DiCaprio Matt Damon Jack Nicholson ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('c042ffd0-5d35-439e-b2b5-fadaf84cffd2','Terminator 2: Judgment Day','1991','MV5BZDM2YjYwYWMtMWZlNi00ZDQxLWExMDctMDAzNzQ0OTkzZjljXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('c042ffd0-5d35-439e-b2b5-fadaf84cffd2','Over 10 years have passed since the first cyborg called The Terminator tried to kill Sarah Connor and her unborn son, John Connor. John Connor, the future leader of the human resistance, is now a healthy young boy. However another Terminator is sent back through time called the T-1000, which is more advanced and more powerful than its predecessor. The Mission: to kill John Connor when he''s still a child. However, Sarah and John do not have to face this threat of a Terminator alone. Another Terminator is also sent back through time. The mission: to protect John and Sarah Connor at all costs. The battle for tomorrow has begun...                Written by Eric ggg tags: Action Sci-Fi Thriller Arnold Schwarzenegger Linda Hamilton Edward Furlong ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('42f28c80-91e3-4050-a4f6-d643dfa29045','Back to the Future','1985','MV5BZmU0M2Y1OGUtZjIxNi00ZjBkLTg1MjgtOWIyNThiZWIwYjRiXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,321,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('42f28c80-91e3-4050-a4f6-d643dfa29045','Marty McFly, a typical American teenager of the Eighties, is accidentally sent back to 1955 in a plutonium-powered DeLorean "time machine" invented by a slightly mad scientist. During his often hysterical, always amazing trip back in time, Marty must make certain his teenage parents-to-be meet and fall in love - so he can get back to the future.                Written by Robert Lynch <docrlynch@yahoo.com> tags: Adventure Comedy Sci-Fi Michael J. Fox Christopher Lloyd Lea Thompson ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('eb453d35-6e80-407b-bab4-302cad341899','Whiplash','2014','MV5BOTVhMWQ5MDktMGE3OS00MjVlLWExZWYtMzY2MTg4ZGFiZDQ5L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('eb453d35-6e80-407b-bab4-302cad341899','A young and talented drummer attending a prestigious music academy finds himself under the wing of the most respected professor at the school, one who does not hold back on abuse towards his students. The two form an odd relationship as the student wants to achieve greatness, and the professor pushes him.                Written by andrewhodkinson tags: Drama Music Miles Teller J.K. Simmons Melissa Benoist ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('4a9dc333-fa87-4f77-8396-0c4ebb8a762c','Gladiator','2000','MV5BMTgwMzQzNTQ1Ml5BMl5BanBnXkFtZTgwMDY2NTYxMTE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('4a9dc333-fa87-4f77-8396-0c4ebb8a762c','Maximus is a powerful Roman general, loved by the people and the aging Emperor, Marcus Aurelius. Before his death, the Emperor chooses Maximus to be his heir over his own son, Commodus, and a power struggle leaves Maximus and his family condemned to death. The powerful general is unable to save his family, and his loss of will allows him to get captured and put into the Gladiator games until he dies. The only desire that fuels him now is the chance to rise to the top so that he will be able to look into the eyes of the man who will feel his revenge.                Written by Chris "Morphy" Terry tags: Action Adventure Drama Russell Crowe Joaquin Phoenix Connie Nielsen ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('1c80a0c0-d211-4791-b5fc-a767b0c32764','Memento','2000','MV5BZTcyNjk1MjgtOWI3Mi00YzQwLWI5MTktMzY4ZmI2NDAyNzYzXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,340,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('1c80a0c0-d211-4791-b5fc-a767b0c32764','Memento chronicles two separate stories of Leonard, an ex-insurance investigator who can no longer build new memories, as he attempts to find the murderer of his wife, which is the last thing he remembers. One story line moves forward in time while the other tells the story backwards revealing more each time.                Written by Scion013 tags: Mystery Thriller Guy Pearce Carrie-Anne Moss Joe Pantoliano ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('3f2f14f7-bd05-44dd-9e2f-a4c5dc670c2b','Apocalypse Now','1979','MV5BODViM2VjYWYtMzU5Ny00ZjczLWEzNjMtODZmNGMwZDAxNzJkXkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SY370_CR0,0,242,370_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('3f2f14f7-bd05-44dd-9e2f-a4c5dc670c2b','It is the height of the war in Vietnam, and U.S. Army Captain Willard is sent by Colonel Lucas and a General to carry out a mission that, officially, ''does not exist - nor will it ever exist''. The mission: To seek out a mysterious Green Beret Colonel, Walter Kurtz, whose army has crossed the border into Cambodia and is conducting hit-and-run missions against the Viet Cong and NVA. The army believes Kurtz has gone completely insane and Willard''s job is to eliminate him! Willard, sent up the Nung River on a U.S. Navy patrol boat, discovers that his target is one of the most decorated officers in the U.S. Army. His crew meets up with surfer-type Lt-Colonel Kilgore, head of a U.S Army helicopter cavalry group which eliminates a Viet Cong outpost to provide an entry point into the Nung River. After some hair-raising encounters, in which some of his crew are killed, Willard, Lance and Chef reach Colonel Kurtz''s outpost, beyond the Do Lung Bridge. Now, after becoming prisoners of Kurtz, will...                Written by Derek O''Cain tags: Drama War Martin Sheen Marlon Brando Robert Duvall ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('39042ccd-851d-4374-acdd-01056a8e07bd','The Prestige','2006','MV5BMjA4NDI0MTIxNF5BMl5BanBnXkFtZTYwNTM0MzY2._V1_SX225_CR0,0,225,333_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('39042ccd-851d-4374-acdd-01056a8e07bd','In the end of the Nineteenth Century, in London, Robert Angier, his beloved wife Julia McCullough and Alfred Borden are friends and assistants of a magician. When Julia accidentally dies during a performance, Robert blames Alfred for her death and they become enemies. Both become famous and rival magicians, sabotaging the performance of the other on the stage. When Alfred performs a successful trick, Robert becomes obsessed trying to disclose the secret of his competitor with tragic consequences.                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Drama Mystery Sci-Fi Christian Bale Hugh Jackman Scarlett Johansson ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('896dc3d1-cb3e-4156-906a-38853980ac5e','The Lion King','1994','MV5BYTYxNGMyZTYtMjE3MS00MzNjLWFjNmYtMDk3N2FmM2JiM2M1XkEyXkFqcGdeQXVyNjY5NDU4NzI@._V1_SY500_CR0,0,336,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('896dc3d1-cb3e-4156-906a-38853980ac5e','A young lion Prince is cast out of his pride by his cruel uncle, who claims he killed his father. While the uncle rules with an iron paw, the prince grows up beyond the Savannah, living by a philosophy: No worries for the rest of your days. But when his past comes to haunt him, the young Prince must decide his fate: will he remain an outcast, or face his demons and become what he needs to be?                Written by femaledragon1234 tags: Animation Adventure Drama Matthew Broderick Jeremy Irons James Earl Jones ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('f90e086f-b2fc-4df5-ad25-70ba1bc5d27a','Alien','1979','MV5BNDNhN2IxZWItNGEwYS00ZDNhLThiM2UtODU3NWJlZjBkYjQxXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,340,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('f90e086f-b2fc-4df5-ad25-70ba1bc5d27a','A commercial crew aboard the deep space towing vessel, Nostromo is on its way home when they pick up an SOS warning from a distant moon. What they don''t know is that the SOS warning is not like any other ordinary warning call. Picking up the signal, the crew realize that they are not alone on the spaceship when an alien stowaway is on the cargo ship.                Written by blazesnakes9 tags: Horror Sci-Fi Sigourney Weaver Tom Skerritt John Hurt ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('963d2d52-3793-4852-8f70-4d48bdf5c70e','Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb','1964','MV5BNTkxYjUxNDYtZGY0My00NTc2LThiZmYtNmNmNmU0NGVkZWYwXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SY500_CR0,0,326,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('963d2d52-3793-4852-8f70-4d48bdf5c70e','Paranoid Brigadier General Jack D. Ripper of Burpelson Air Force Base, believing that fluoridation of the American water supply is a Soviet plot to poison the U.S. populace, is able to deploy through a back door mechanism a nuclear attack on the Soviet Union without the knowledge of his superiors, including the Chair of the Joint Chiefs of Staff, General Buck Turgidson, and President Merkin Muffley. Only Ripper knows the code to recall the B-52 bombers and he has shut down communication in and out of Burpelson as a measure to protect this attack. Ripper''s executive officer, RAF Group Captain Lionel Mandrake (on exchange from Britain), who is being held at Burpelson by Ripper, believes he knows the recall codes if he can only get a message to the outside world. Meanwhile at the Pentagon War Room, key persons including Muffley, Turgidson and nuclear scientist and adviser, a former Nazi named Dr. Strangelove, are discussing measures to stop the attack or mitigate its blow-up into an all ...                Written by Huggo tags: Comedy Peter Sellers George C. Scott Sterling Hayden ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('b8e991ff-678b-433c-a00c-b8f23749e617','Sunset Blvd.','1950','MV5BMTc3NDYzODAwNV5BMl5BanBnXkFtZTgwODg1MTczMTE@._V1_SY250_SX166_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('b8e991ff-678b-433c-a00c-b8f23749e617','In Hollywood of the 50''s, the obscure screenplay writer Joe Gillis is not able to sell his work to the studios, is full of debts and is thinking in returning to his hometown to work in an office. While trying to escape from his creditors, he has a flat tire and parks his car in a decadent mansion in Sunset Boulevard. He meets the owner and former silent-movie star Norma Desmond, who lives alone wit her butler and driver Max von Mayerling. Norma is demented and believes she will return to the cinema industry, and is protected and isolated from the world by Max, who was his director and husband in the past and still loves her. Norma proposes Joe to move to the mansion and help her in writing a screenplay for her comeback to the cinema, and the small-time writer becomes her lover and gigolo. When Joe falls in love for the young aspirant writer Betty Schaefer, Norma becomes jealous and completely insane and her madness leads to a tragic end.                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Drama Film-Noir William Holden Gloria Swanson Erich von Stroheim ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('418eb5ec-8756-430c-bb92-47eb8c94309a','The Great Dictator','1940','MV5BMmExYWJjNTktNGUyZS00ODhmLTkxYzAtNWIzOGEyMGNiMmUwXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,338,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('418eb5ec-8756-430c-bb92-47eb8c94309a','Twenty years after the end of WWI in which the nation of Tomainia was on the losing side, Adenoid Hynkel has risen to power as the ruthless dictator of the country. He believes in a pure Aryan state, and the decimation of the Jews. This situation is unknown to a simple Jewish-Tomainian barber who has since been hospitalized the result of a WWI battle. Upon his release, the barber, who had been suffering from memory loss about the war, is shown the new persecuted life of the Jews by many living in the Jewish ghetto, including a washerwoman named Hannah, with whom he begins a relationship. The barber is ultimately spared such persecution by Commander Schultz, who he saved in that WWI battle. The lives of all Jews in Tomainia are eventually spared with a policy shift by Hynkel himself, who is doing so for ulterior motives. But those motives include a want for world domination, starting with the invasion of neighboring Osterlich, which may be threatened by Benzino Napaloni, the dictator ...                Written by Huggo tags: Comedy Drama War Charles Chaplin Paulette Goddard Jack Oakie ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2b7dc4bf-9331-4927-b867-a33254e45649','Nuovo Cinema Paradiso','1988','MV5BMjIzMTgzOTEwOF5BMl5BanBnXkFtZTgwNTUxNjcxMTE@._V1_SY249_CR0,0,168,249_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2b7dc4bf-9331-4927-b867-a33254e45649','A famous film director remembers his childhood at the Cinema Paradiso where Alfredo, the projectionist, first brought about his love of films. He returns home to his Sicilian village for the first time after almost 30 years and is reminded of his first love, Elena, who disappeared from his life before he left for Rome.                Written by Graeme Roy <gsr@cbmamiga.demon.co.uk> tags: Drama Philippe Noiret Enzo Cannavale Antonella Attili ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2e69a0b3-d334-4a2b-87eb-a29f7f006671','Das Leben der Anderen','2006','MV5BNDUzNjYwNDYyNl5BMl5BanBnXkFtZTcwNjU3ODQ0MQ@@._V1_SX225_CR0,0,225,337_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2e69a0b3-d334-4a2b-87eb-a29f7f006671','Gerd Wiesler is an officer with the Stasi, the East German secret police. The film begins in 1984 when Wiesler attends a play written by Georg Dreyman, who is considered by many to be the ultimate example of the loyal citizen. Wiesler has a gut feeling that Dreyman can''t be as ideal as he seems and believes surveillance is called for. The Minister of Culture agrees but only later does Wiesler learn that the Minister sees Dreyman as a rival and lusts after his partner Christa-Maria. The more time he spends listening in on them, the more he comes to care about them. The once rigid Stasi officer begins to intervene in their lives, in a positive way, protecting them whenever possible. Eventually, Wiesler activities catch up to him and while there is no proof of wrongdoing, he finds himself in menial jobs - until the unbelievable happens.                Written by garykmcd tags: Drama Thriller Ulrich Mühe Martina Gedeck Sebastian Koch ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('22964c7f-8bff-4ecd-8bb2-aaeb9a18fc68','Hotaru no haka','1988','MV5BZjEwNDVhZjMtYzA1MS00ZWUxLThjOGUtZTliNGZiNGYyMjA3XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,353,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('22964c7f-8bff-4ecd-8bb2-aaeb9a18fc68','The story of Seita and Satsuko, two young Japanese siblings, living in the declining days of World War II. When an American firebombing separates the two children from their parents, the two siblings must rely completely on one another while they struggle to fight for their survival.                Written by Kyle Perez tags: Animation Drama War Tsutomu Tatsumi Ayano Shiraishi Akemi Yamaguchi ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d3fad5cb-0631-4d9b-9e6f-4453d13df4cf','Paths of Glory','1957','MV5BOTI5Nzc0OTMtYzBkMS00NjkxLThmM2UtNjM2ODgxN2M5NjNkXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_SY500_CR0,0,326,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d3fad5cb-0631-4d9b-9e6f-4453d13df4cf','The futility and irony of the war in the trenches in WWI is shown as a unit commander in the French army must deal with the mutiny of his men and a glory-seeking general after part of his force falls back under fire in an impossible attack.                Written by Keith Loh <loh@sfu.ca> tags: Drama War Kirk Douglas Ralph Meeker Adolphe Menjou ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('c3fab0fa-a0f2-4e13-8eff-2d443ffbc699','Django Unchained','2012','MV5BMjIyNTQ5NjQ1OV5BMl5BanBnXkFtZTcwODg1MDU4OA@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('c3fab0fa-a0f2-4e13-8eff-2d443ffbc699','A German dentist buys the freedom of a slave and trains him with the intent to make him his deputy bounty hunter. Instead, he is led to the site of the slave''s wife who belongs to a ruthless plantation owner.                Written by BenLobel tags: Drama Western Jamie Foxx Christoph Waltz Leonardo DiCaprio ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('5d61eca8-d9de-41cf-99e1-a62b4c0e6e74','The Shining','1980','MV5BZWFlYmY2MGEtZjVkYS00YzU4LTg0YjQtYzY1ZGE3NTA5NGQxXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY476_CR0,0,313,476_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('5d61eca8-d9de-41cf-99e1-a62b4c0e6e74','Signing a contract, Jack Torrance, a normal writer and former teacher agrees to take care of a hotel which has a long, violent past that puts everyone in the hotel in a nervous situation. While Jack slowly gets more violent and angry of his life, his son, Danny, tries to use a special talent, the "Shining", to inform the people outside about whatever that is going on in the hotel.                Written by J. S. Golden tags: Drama Horror Jack Nicholson Shelley Duvall Danny Lloyd ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('bbd3e062-9fa4-4fc4-982b-8dfa3b928b14','La La Land','2016','MV5BMzUzNDM2NzM2MV5BMl5BanBnXkFtZTgwNTM3NTg4OTE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('bbd3e062-9fa4-4fc4-982b-8dfa3b928b14','Mia, an aspiring actress, serves lattes to movie stars in between auditions and Sebastian, a jazz musician, scrapes by playing cocktail party gigs in dingy bars, but as success mounts they are faced with decisions that begin to fray the fragile fabric of their love affair, and the dreams they worked so hard to maintain in each other threaten to rip them apart.                Written by Eirini tags: Comedy Drama Musical Ryan Gosling Emma Stone Rosemarie DeWitt ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('4bdb611c-ffcb-4bcd-8636-bd9d2ba37438','WALL·E','2008','MV5BMjExMTg5OTU0NF5BMl5BanBnXkFtZTcwMjMxMzMzMw@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('4bdb611c-ffcb-4bcd-8636-bd9d2ba37438','In a distant, but not so unrealistic, future where mankind has abandoned earth because it has become covered with trash from products sold by the powerful multi-national Buy N Large corporation, WALL-E, a garbage collecting robot has been left to clean up the mess. Mesmerized with trinkets of Earth''s history and show tunes, WALL-E is alone on Earth except for a sprightly pet cockroach. One day, EVE, a sleek (and dangerous) reconnaissance robot, is sent to Earth to find proof that life is once again sustainable. WALL-E falls in love with EVE. WALL-E rescues EVE from a dust storm and shows her a living plant he found amongst the rubble. Consistent with her "directive", EVE takes the plant and automatically enters a deactivated state except for a blinking green beacon. WALL-E, doesn''t understand what has happened to his new friend, but, true to his love, he protects her from wind, rain, and lightning, even as she is unresponsive. One day a massive ship comes to reclaim EVE, but WALL-E, ... tags: Animation Adventure Family Ben Burtt Elissa Knight Jeff Garlin ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d8e893e1-8cb9-41e4-ac5d-7fd9c479076e','American Beauty','1999','MV5BMjM4NTI5NzYyNV5BMl5BanBnXkFtZTgwNTkxNTYxMTE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d8e893e1-8cb9-41e4-ac5d-7fd9c479076e','Lester and Carolyn Burnham are, on the outside, a perfect husband and wife in a perfect house in a perfect neighborhood. But inside, Lester is slipping deeper and deeper into a hopeless depression. He finally snaps when he becomes infatuated with one of his daughter''s friends. Meanwhile, his daughter Jane is developing a happy friendship with a shy boy-next-door named Ricky, who lives with an abusive father.                Written by Jessie Skinner <eietherbinge@hotmail.com> tags: Drama Romance Kevin Spacey Annette Bening Thora Birch ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('00815924-e9be-41b9-9be1-b08c1a38dd39','The Dark Knight Rises','2012','MV5BMTk4ODQzNDY3Ml5BMl5BanBnXkFtZTcwODA0NTM4Nw@@._V1_SY360_SX243_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('00815924-e9be-41b9-9be1-b08c1a38dd39','Despite his tarnished reputation after the events of The Dark Knight, in which he took the rap for Dent''s crimes, Batman feels compelled to intervene to assist the city and its police force which is struggling to cope with Bane''s plans to destroy the city.                Written by WellardRockard tags: Action Thriller Christian Bale Tom Hardy Anne Hathaway ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('765b1091-cc9a-4e0f-91dd-e15d3714fa9f','Mononoke-hime','1997','MV5BMTVlNWM4NTAtNDQxYi00YWU5LWIwM2MtZmVjYWFmODZiODE5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,353,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('765b1091-cc9a-4e0f-91dd-e15d3714fa9f','While protecting his village from rampaging boar-god/demon, a confident young warrior, Ashitaka, is stricken by a deadly curse. To save his life, he must journey to the forests of the west. Once there, he''s embroiled in a fierce campaign that humans were waging on the forest. The ambitious Lady Eboshi and her loyal clan use their guns against the gods of the forest and a brave young woman, Princess Mononoke, who was raised by a wolf-god. Ashitaka sees the good in both sides and tries to stem the flood of blood. This is met be animosity by both sides as they each see him as supporting the enemy.                Written by Christopher Taguchi tags: Animation Adventure Fantasy Yôji Matsuda Yuriko Ishida Yûko Tanaka ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d8e0e00e-a2df-4e5e-b61f-fa9637114600','Aliens','1986','MV5BNGYxMTA0M2EtYjg0Yy00NzI5LTg4NjEtZDA2MTcyOWM0YTVjL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY377_CR0,0,247,377_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d8e0e00e-a2df-4e5e-b61f-fa9637114600','Fifty seven years after Ellen Ripley survived her disastrous ordeal, her escape vessel is recovered after drifting across the galaxy as she slept in cryogenic stasis. Back on Earth, nobody believed her story about the "Aliens" on the moon LV-426. After the "Company" orders the colony on LV-426 to investigate, however, all communication with the colony is lost. The Company enlists Ripley to aid a team of tough, rugged space marines on a rescue mission to the now partially terraformed moon to find out if there are aliens or survivors. As the mission unfolds, Ripley will be forced to come to grips with her worst nightmare, but even as she does, she finds that the worst is yet to come.                Written by Brian Rawlings tags: Action Adventure Sci-Fi Sigourney Weaver Michael Biehn Carrie Henn ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('a96e5e3f-3f5f-454a-9dbf-079cfa6e9a83','Oldeuboi','2003','MV5BMTI3NTQyMzU5M15BMl5BanBnXkFtZTcwMTM2MjgyMQ@@._V1_SY333_SX225_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('a96e5e3f-3f5f-454a-9dbf-079cfa6e9a83','An average man is kidnapped and imprisoned in a shabby cell for 15 years without explanation. He then is released, equipped with money, a cellphone and expensive clothes. As he strives to explain his imprisonment and get his revenge, Oh Dae-Su soon finds out that his kidnapper has a greater plan for him and is set onto a path of pain and suffering in an attempt to uncover the motive of his mysterious tormentor.                Written by Jacksrevenge tags: Drama Mystery Thriller Min-sik Choi Ji-tae Yu Hye-jeong Kang ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('37453144-6668-4180-bd9c-0bc8ae986145','Once Upon a Time in America','1984','MV5BMGFkNWI4MTMtNGQ0OC00MWVmLTk3MTktOGYxN2Y2YWVkZWE2XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,319,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('37453144-6668-4180-bd9c-0bc8ae986145','Epic tale of a group of Jewish gangsters in New York, from childhood, through their glory years during prohibition, and their meeting again 35 years later.                Written by Andrew Welsh <andreww@bnr.ca> tags: Crime Drama Robert De Niro James Woods Elizabeth McGovern ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('78fd107d-246f-4007-8f13-c617772451dc','Citizen Kane','1941','MV5BMTQ2Mjc1MDQwMl5BMl5BanBnXkFtZTcwNzUyOTUyMg@@._V1_SY250_SX169_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('78fd107d-246f-4007-8f13-c617772451dc','A group of reporters are trying to decipher the last word ever spoken by Charles Foster Kane, the millionaire newspaper tycoon: "Rosebud." The film begins with a news reel detailing Kane''s life for the masses, and then from there, we are shown flashbacks from Kane''s life. As the reporters investigate further, the viewers see a display of a fascinating man''s rise to fame, and how he eventually fell off the top of the world.                Written by Zack H. tags: Drama Mystery Orson Welles Joseph Cotten Dorothy Comingore ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('9df8905d-a91d-439e-b45e-9b1eb1f902e2','Das Boot','1981','MV5BNGRmOWY0MGUtNTVhMy00NzRlLTljNDAtNTBiNTRlODgxNmY2XkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY406_SX290_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('9df8905d-a91d-439e-b45e-9b1eb1f902e2','It is 1942 and the German submarine fleet is heavily engaged in the so-called "Battle of the Atlantic" to harass and destroy British shipping. With better escorts of the Destroyer Class, however, German U-Boats have begun to take heavy losses. "Das Boot" is the story of one such U-Boat crew, with the film examining how these submariners maintained their professionalism as soldiers and attempted to accomplish impossible missions, all the while attempting to understand and obey the ideology of the government under which they served.                Written by Anthony Hughes <husnock31@hotmail.com> tags: Adventure Drama Thriller Jürgen Prochnow Herbert Grönemeyer Klaus Wennemann ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('ebd4ad14-6b7a-45aa-ba36-0c4fdba35ab0','Witness for the Prosecution','1957','MV5BMTc0MjgyNTUyNF5BMl5BanBnXkFtZTcwNDQzMDg0Nw@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('ebd4ad14-6b7a-45aa-ba36-0c4fdba35ab0','It''s Britain, 1953. Upon his return to work following a heart attack, irrepressible barrister Sir Wilfrid Robarts, known as a barrister for the hopeless, takes on a murder case, much to the exasperation of his medical team, led by his overly regulated private nurse, Miss Plimsoll, who tries her hardest to ensure that he not return to his hard living ways - including excessive cigar smoking and drinking - while he takes his medication and gets his much needed rest. That case is defending American war veteran Leonard Vole, a poor, out of work, struggling inventor who is accused of murdering his fifty-six year old lonely and wealthy widowed acquaintance, Emily French. The initial evidence is circumstantial but points to Leonard as the murderer. Despite being happily married to East German former beer hall performer Christine Vole, he fostered that friendship with Mrs. French in the hopes that she would finance one of his many inventions to the tune of a few hundred pounds. It thus does ...                Written by Huggo tags: Crime Drama Mystery Tyrone Power Marlene Dietrich Charles Laughton ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('e8d7bc75-9713-4ce5-93a4-c4df2b33f8dd','North by Northwest','1959','MV5BMjQwMTQ0MzgwNl5BMl5BanBnXkFtZTgwNjc4ODE4MzE@._V1_SY500_CR0,0,319,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('e8d7bc75-9713-4ce5-93a4-c4df2b33f8dd','Madison Avenue advertising man Roger Thornhill finds himself thrust into the world of spies when he is mistaken for a man by the name of George Kaplan. Foreign spy Philip Vandamm and his henchman Leonard try to eliminate him but when Thornhill tries to make sense of the case, he is framed for murder. Now on the run from the police, he manages to board the 20th Century Limited bound for Chicago where he meets a beautiful blond, Eve Kendall, who helps him to evade the authorities. His world is turned upside down yet again when he learns that Eve isn''t the innocent bystander he thought she was. Not all is as it seems however, leading to a dramatic rescue and escape at the top of Mt. Rushmore.                Written by garykmcd tags: Action Adventure Mystery Cary Grant Eva Marie Saint James Mason ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('60c317fa-093f-4fc3-b5ab-80a8882fec1a','Vertigo','1958','MV5BYTE4ODEwZDUtNDFjOC00NjAxLWEzYTQtYTI1NGVmZmFlNjdiL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY500_CR0,0,322,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('60c317fa-093f-4fc3-b5ab-80a8882fec1a','John "Scottie" Ferguson is a retired San Francisco police detective who suffers from acrophobia and Madeleine is the lady who leads him to high places. A wealthy shipbuilder who is an acquaintance from college days approaches Scottie and asks him to follow his beautiful wife, Madeleine. He fears she is going insane, maybe even contemplating suicide, he believes she is possessed by a dead ancestor. Scottie is skeptical, but agrees after he sees the beautiful Madeleine.                Written by filmfactsman tags: Mystery Romance Thriller James Stewart Kim Novak Barbara Bel Geddes ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('7b64d5f1-21c4-4e42-abcc-2797c0a0338b','Star Wars: Episode VI - Return of the Jedi','1983','MV5BODllZjg2YjUtNWEzNy00ZGY2LTgyZmQtYTkxNDYyOWM3OTUyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,318,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('7b64d5f1-21c4-4e42-abcc-2797c0a0338b','Luke Skywalker battles horrible Jabba the Hut and cruel Darth Vader to save his comrades in the Rebel Alliance and triumph over the Galactic Empire. Han Solo and Princess Leia reaffirm their love and team with Chewbacca, Lando Calrissian, the Ewoks and the androids C-3PO and R2-D2 to aid in the disruption of the Dark Side and the defeat of the evil emperor.                Written by Jwelch5742 tags: Action Adventure Fantasy Mark Hamill Harrison Ford Carrie Fisher ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('90daebb4-5a8d-46fb-b822-2f1a9fc2b23b','Reservoir Dogs','1992','MV5BNDc0YWFhMmQtOTFhZC00MzViLTlkNjctMWUzYTI2YWFkNzVlXkEyXkFqcGdeQXVyNjE5MjUyOTM@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('90daebb4-5a8d-46fb-b822-2f1a9fc2b23b','Six criminals, who are strangers to each other, are hired by a crime boss, Joe Cabot, to carry out a diamond robbery. Right at the outset, they are given false names with the intention that they won''t get too close and will concentrate on the job instead. They are completely sure that the robbery is going to be a success. But, when the police show up right at the time and the site of the robbery, panic spreads amongst the group members, and two of them are killed in the subsequent shootout, along with a few policemen and civilians. When the remaining people assemble at the premeditated rendezvous point (a warehouse), they begin to suspect that one of them is an undercover cop.                Written by Soumitra tags: Crime Drama Thriller Harvey Keitel Tim Roth Michael Madsen ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('490bfe8d-2d3e-4c5d-94e8-929e537476fd','M','1931','MV5BMTQyNjA5NzU5MV5BMl5BanBnXkFtZTgwMDk1MTA5MTE@._V1_SY250_SX175_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('490bfe8d-2d3e-4c5d-94e8-929e537476fd','In Germany, Hans Beckert is an unknown killer of girls. He whistles Edvard Grieg''s ''In The Hall of the Mountain King'', from the ''Peer Gynt'' Suite I Op. 46 while attracting the little girls for death. The police force pressed by the Minister give its best effort trying unsuccessfully to arrest the serial killer. The organized crime has great losses due to the intense search and siege of the police and decides to chase the murderer, with the support of the beggars association. They catch Hans and briefly judge him.                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Crime Drama Mystery Peter Lorre Ellen Widmann Inge Landgut ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('6d7ddf36-5ef4-40d7-a24d-020e1abcb564','Braveheart','1995','MV5BNTMyNGE1ODQtYTNiNS00ZTUyLThhZjktMTgyOGZkZThlYTc3XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,338,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('6d7ddf36-5ef4-40d7-a24d-020e1abcb564','William Wallace is a Scottish rebel who leads an uprising against the cruel English ruler Edward the Longshanks, who wishes to inherit the crown of Scotland for himself. When he was a young boy, William Wallace''s father and brother, along with many others, lost their lives trying to free Scotland. Once he loses another of his loved ones, William Wallace begins his long quest to make Scotland free once and for all, along with the assistance of Robert the Bruce.                Written by Anonymous tags: Biography Drama History Mel Gibson Sophie Marceau Patrick McGoohan ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('fe504a66-9184-4488-a59e-7ea9a6b3bb06','Requiem for a Dream','2000','MV5BOTdiNzJlOWUtNWMwNS00NmFlLWI0YTEtZmI3YjIzZWUyY2Y3XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('fe504a66-9184-4488-a59e-7ea9a6b3bb06','Sara Goldfarb (Ellen Burstyn) is a retired widow, living in a small apartment. She spends most of her time watching TV, especially a particular self-help show. She has delusions of rising above her current dull existence by being a guest on that show. Her son, Harry (Jared Leto) is a junkie but along with his friend Tyrone (Marlon Wayans) has visions of making it big by becoming a drug dealer. Harry''s girlfriend Marion (Jennifer Connelly) could be fashion designer or artist but is swept along in Harry''s drug-centric world. Meanwhile Sara has developed an addiction of her own. She desperately wants to lose weight and so goes on a crash course involving popping pills, pills which turn out to be very addictive and harmful to her mental state.                Written by grantss tags: Drama Ellen Burstyn Jared Leto Jennifer Connelly ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('8e17b32b-39a1-404a-91fd-f4b1665657cc','Le fabuleux destin d''Amélie Poulain','2001','MV5BMTA3MjVkMWMtYTQ4ZC00ODczLWFjYmQtMDFkZDQ2Y2M0NDVmXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,336,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('8e17b32b-39a1-404a-91fd-f4b1665657cc','Amélie is a story about a girl named Amélie whose childhood was suppressed by her Father''s mistaken concerns of a heart defect. With these concerns Amélie gets hardly any real life contact with other people. This leads Amélie to resort to her own fantastical world and dreams of love and beauty. She later on becomes a young woman and moves to the central part of Paris as a waitress. After finding a lost treasure belonging to the former occupant of her apartment, she decides to return it to him. After seeing his reaction and his new found perspective - she decides to devote her life to the people around her. Such as, her father who is obsessed with his garden-gnome, a failed writer, a hypochondriac, a man who stalks his ex girlfriends, the "ghost", a suppressed young soul, the love of her life and a man whose bones are as brittle as glass. But after consuming herself with these escapades - she finds out that she is disregarding her own life and damaging her quest for love. Amélie then ...                Written by spragg_s tags: Comedy Romance Audrey Tautou Mathieu Kassovitz Rufus ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('b5bfb740-012d-4fea-baf9-13808cf4fd25','A Clockwork Orange','1971','MV5BMTY3MjM1Mzc4N15BMl5BanBnXkFtZTgwODM0NzAxMDE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('b5bfb740-012d-4fea-baf9-13808cf4fd25','Protagonist Alex DeLarge is an "ultraviolent" youth in futuristic Britain. As with all luck, his eventually runs out and he''s arrested and convicted of murder and rape. While in prison, Alex learns of an experimental program in which convicts are programmed to detest violence. If he goes through the program, his sentence will be reduced and he will be back on the streets sooner than expected. But Alex''s ordeals are far from over once he hits the mean streets of Britain that he had a hand in creating.                Written by Nikki Carlyle tags: Crime Drama Sci-Fi Malcolm McDowell Patrick Magee Michael Bates ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('7a30ba7f-8c3f-412d-be61-5f5bcea92ae6','Taxi Driver','1976','MV5BNGQxNDgzZWQtZTNjNi00M2RkLWExZmEtNmE1NjEyZDEwMzA5XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,327,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('7a30ba7f-8c3f-412d-be61-5f5bcea92ae6','Travis Bickle is an ex-Marine and Vietnam War veteran living in New York City. As he suffers from insomnia, he spends his time working as a taxi driver at night, watching porn movies at seedy cinemas during the day, or thinking about how the world, New York in particular, has deteriorated into a cesspool. He''s a loner who has strong opinions about what is right and wrong with mankind. For him, the one bright spot in New York humanity is Betsy, a worker on the presidential nomination campaign of Senator Charles Palantine. He becomes obsessed with her. After an incident with her, he believes he has to do whatever he needs to make the world a better place in his opinion. One of his priorities is to be the savior for Iris, a twelve-year-old runaway and prostitute who he believes wants out of the profession and under the thumb of her pimp and lover Matthew.                Written by Huggo tags: Crime Drama Robert De Niro Jodie Foster Cybill Shepherd ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('1f227616-499b-4309-9657-46a3305afd46','Taare Zameen Par','2007','MV5BNTVmYTk2NjAtYzY3MS00YjFjLTlkYzktYzg3YzMyZDQyOWRiXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_SY500_CR0,0,346,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('1f227616-499b-4309-9657-46a3305afd46','Ishaan Awasthi is an eight-year-old whose world is filled with wonders that no one else seems to appreciate; colours, fish, dogs and kites are just not important in the world of adults, who are much more interested in things like homework, marks and neatness. And Ishaan just cannot seem to get anything right in class. When he gets into far more trouble than his parents can handle, he is packed off to a boarding school to ''be disciplined''. Things are no different at his new school, and Ishaan has to contend with the added trauma of separation from his family. One day a new art teacher bursts onto the scene, Ram Shankar Nikumbh, who infects the students with joy and optimism. He breaks all the rules of ''how things are done'' by asking them to think, dream and imagine, and all the children respond with enthusiasm, all except Ishaan. Nikumbh soon realizes that Ishaan is very unhappy, and he sets out to discover why. With time, patience and care, he ultimately helps Ishaan find himself.                Written by Anonymous tags: Drama Family Music Darsheel Safary Aamir Khan Tanay Chheda ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('0f76bf4e-e82e-4e52-b9ea-3019f934be84','Lawrence of Arabia','1962','MV5BYWY5ZjhjNGYtZmI2Ny00ODM0LWFkNzgtZmI1YzA2N2MxMzA0XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SY500_CR0,0,346,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('0f76bf4e-e82e-4e52-b9ea-3019f934be84','Due to his knowledge of the native Bedouin tribes, British Lieutenant T.E. Lawrence is sent to Arabia to find Prince Faisal and serve as a liaison between the Arabs and the British in their fight against the Turks. With the aid of native Sherif Ali, Lawrence rebels against the orders of his superior officer and strikes out on a daring camel journey across the harsh desert to attack a well-guarded Turkish port.                Written by Jwelch5742 tags: Adventure Biography Drama Peter O''Toole Alec Guinness Anthony Quinn ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('31a72116-37aa-42d0-a474-94ef034fa2fd','Double Indemnity','1944','MV5BZmRmYmZkZTktZDc5ZC00ZjZmLTg4NWMtYjM3MjcyNTVjNGQ5L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('31a72116-37aa-42d0-a474-94ef034fa2fd','In 1938, Walter Neff, an experienced salesman of the Pacific All Risk Insurance Co., meets the seductive wife of one of his clients, Phyllis Dietrichson, and they have an affair. Phyllis proposes to kill her husband to receive the proceeds of an accident insurance policy and Walter devises a scheme to receive twice the amount based on a double indemnity clause. When Mr. Dietrichson is found dead on a train track, the police accept the determination of accidental death. However, the insurance analyst and Walter''s best friend Barton Keyes does not buy the story and suspects that Phyllis has murdered her husband with the help of another man.                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Crime Drama Film-Noir Fred MacMurray Barbara Stanwyck Edward G. Robinson ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('76d2b850-ba92-4990-86db-99066172466a','Eternal Sunshine of the Spotless Mind','2004','MV5BMTY4NzcwODg3Nl5BMl5BanBnXkFtZTcwNTEwOTMyMw@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('76d2b850-ba92-4990-86db-99066172466a','A man, Joel Barish, heartbroken that his girlfriend Clementine underwent a procedure to erase him from her memory, decides to do the same. However, as he watches his memories of her fade away, he realizes that he still loves her, and may be too late to correct his mistake.                Written by anonymous tags: Drama Romance Sci-Fi Jim Carrey Kate Winslet Tom Wilkinson ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('01d0bceb-6deb-46bc-b095-37546b880c1e','To Kill a Mockingbird','1962','MV5BMjA4MzI1NDY2Nl5BMl5BanBnXkFtZTcwMTcyODc5Mw@@._V1_SY250_SX164_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('01d0bceb-6deb-46bc-b095-37546b880c1e','Small-town Alabama, 1932. Atticus Finch (played by Gregory Peck) is a lawyer and a widower. He has two young children, Jem and Scout. Atticus Finch is currently defending Tom Robinson, a black man accused of raping a white woman. Meanwhile, Jem and Scout are intrigued by their neighbours, the Radleys, and the mysterious, seldom-seen Boo Radley in particular.                Written by grantss tags: Crime Drama Gregory Peck John Megna Frank Overton ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('42edf31e-dbb9-4c3d-89cc-65af6a529fb4','Amadeus','1984','MV5BMTg5NDkwMTk5N15BMl5BanBnXkFtZTYwODg3MDk2._V1_SY352_CR0,0,237,352_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('42edf31e-dbb9-4c3d-89cc-65af6a529fb4','Antonio Salieri believes that Wolfgang Amadeus Mozart''s music is divine and miraculous. He wishes he was himself as good a musician as Mozart so that he can praise the Lord through composing. He began his career as a devout man who believes his success and talent as a composer are God''s rewards for his piety. He''s also content as the respected, financially well-off, court composer of Austrian Emperor Joseph II. But he''s shocked to learn that Mozart is such a vulgar creature, and can''t understand why God favored Mozart to be his instrument. Salieri''s envy has made him an enemy of God whose greatness was evident in Mozart. He is ready to take revenge against God and Mozart for his own musical mediocrity.                Written by Khaled Salem tags: Biography Drama History F. Murray Abraham Tom Hulce Elizabeth Berridge ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('369ddc6a-7733-4e0d-bee9-a8bcb2ff33e6','Toy Story 3','2010','MV5BMTgxOTY4Mjc0MF5BMl5BanBnXkFtZTcwNTA4MDQyMw@@._V1_SY500_CR0,0,353,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('369ddc6a-7733-4e0d-bee9-a8bcb2ff33e6','Woody, Buzz and the whole gang are back. As their owner Andy prepares to depart for college, his loyal toys find themselves in daycare where untamed tots with their sticky little fingers do not play nice. So, it''s all for one and one for all as they join Barbie''s counterpart Ken, a thespian hedgehog named Mr. Pricklepants and a pink, strawberry-scented teddy bear called Lots-o''-Huggin'' Bear to plan their great escape.                Written by Walt Disney Studios tags: Animation Adventure Comedy Tom Hanks Tim Allen Joan Cusack ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d44e5df9-a00f-498c-81ed-024b27ebc0fe','Full Metal Jacket','1987','MV5BNzc2ZThkOGItZGY5YS00MDYwLTkyOTAtNDRmZWIwMGRhYTc0L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,328,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d44e5df9-a00f-498c-81ed-024b27ebc0fe','A two-segment look at the effect of the military mindset and war itself on Vietnam era Marines. The first half follows a group of recruits in boot camp under the command of the punishing Gunnery Sergeant Hartman. The second half shows one of those recruits, Joker, covering the war as a correspondent for Stars and Stripes, focusing on the Tet offensive.                Written by Scott Renshaw <as.idc@forsythe.stanford.edu> tags: Drama War Matthew Modine R. Lee Ermey Vincent D''Onofrio ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('8eab9c95-4ae4-4046-857b-f23e5f6a2902','Dangal','2016','MV5BMTQ4MzQzMzM2Nl5BMl5BanBnXkFtZTgwMTQ1NzU3MDI@._V1_SY500_CR0,0,356,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('8eab9c95-4ae4-4046-857b-f23e5f6a2902','Biopic of Mahavir Singh Phogat, who taught wrestling to his daughters Babita Kumari and Geeta Phogat. Geeta Phogat was India''s first female wrestler to win at the 2010 Commonwealth Games, where she won the gold medal (55 kg) while her sister Babita Kumari won the silver (52 kg).                Written by Dibyayan_Chakravorty tags: Action Biography Drama Aamir Khan Sakshi Tanwar Fatima Sana Shaikh ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('e0cc0575-4082-4048-a8f2-04f70c11e013','The Sting','1973','MV5BMTY5MjM1OTAyOV5BMl5BanBnXkFtZTgwMDkwODg4MDE@._V1._CR52,57,915,1388_SY500_CR0,0,329,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('e0cc0575-4082-4048-a8f2-04f70c11e013','When a mutual friend is killed by a mob boss, two con men, one experienced and one young try to get even by pulling off the big con on the mob boss. The story unfolds with several twists and last minute alterations.                Written by John Vogel <jlvogel@comcast.net> tags: Comedy Crime Drama Paul Newman Robert Redford Robert Shaw ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('174e3373-286b-4033-b734-6a56a6d3f876','2001: A Space Odyssey','1968','MV5BMTZkZTBhYmUtMTIzNy00YTViLTg1OWItNGUwMmVlN2FjZTVkXkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('174e3373-286b-4033-b734-6a56a6d3f876','"2001" is a story of evolution. Sometime in the distant past, someone or something nudged evolution by placing a monolith on Earth (presumably elsewhere throughout the universe as well). Evolution then enabled humankind to reach the moon''s surface, where yet another monolith is found, one that signals the monolith placers that humankind has evolved that far. Now a race begins between computers (HAL) and human (Bowman) to reach the monolith placers. The winner will achieve the next step in evolution, whatever that may be.                Written by Larry Cousins tags: Adventure Sci-Fi Keir Dullea Gary Lockwood William Sylvester ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('aef28316-d044-439c-9b57-9b0e1adeecea','Babam ve Oglum','2005','MV5BNjAzMzEwYzctNjc1MC00Nzg5LWFmMGItMTgzYmMyNTY2OTQ4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_SX347_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('aef28316-d044-439c-9b57-9b0e1adeecea','Sadik is one of the rebellious youth who has been politically active as a university student and became a left-wing journalist in the 70''s, despite his father''s expectations of him becoming an agricultural engineer and taking control of their family farm in an Aegean village. On the dawn of September 12, 1980, when a merciless military coup hits the country, they cannot find access to any hospital or a doctor and his wife dies while giving birth to their only child, Deniz. After a long-lasting period of torture, trials, and jail time, Sadik returns to his village with 7-8 years old Deniz, knowing that it will be hard to correct things with his father, Huseyin.                Written by Ali Riza Bolukbasi tags: Drama Fikret Kuskan Çetin Tekindor Hümeyra ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('339fdb5b-15d4-488d-ba9d-c7685fcad30c','Singin'' in the Rain','1952','MV5BZDRjNGViMjQtOThlMi00MTA3LThkYzQtNzJkYjBkMGE0YzE1XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY396_SX271_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('339fdb5b-15d4-488d-ba9d-c7685fcad30c','1927 Hollywood. Monumental Pictures'' biggest stars, glamorous on-screen couple Lina Lamont and Don Lockwood, are also an off-screen couple if the trade papers and gossip columns are to be believed. Both perpetuate the public perception if only to please their adoring fans and bring people into the movie theaters. In reality, Don barely tolerates her, while Lina, despite thinking Don beneath her, simplemindedly believes what she sees on screen in order to bolster her own stardom and sense of self-importance. R.F. Simpson, Monumental''s head, dismisses what he thinks is a flash in the pan: talking pictures. It isn''t until Jazzsångaren (1927) becomes a bona fide hit which results in all the movie theaters installing sound equipment that R.F. knows Monumental, most specifically in the form of Don and Lina, have to jump on the talking picture bandwagon, despite no one at the studio knowing anything about the technology. Musician Cosmo Brown, Don''s best friend, gets hired as ...                Written by Huggo tags: Comedy Musical Romance Gene Kelly Donald O''Connor Debbie Reynolds ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('e1db95dd-5e5f-40e9-8908-d2e2851061a9','Toy Story','1995','MV5BMDU2ZWJlMjktMTRhMy00ZTA5LWEzNDgtYmNmZTEwZTViZWJkXkEyXkFqcGdeQXVyNDQ2OTk4MzI@._V1_SY500_SX335_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('e1db95dd-5e5f-40e9-8908-d2e2851061a9','A little boy named Andy loves to be in his room, playing with his toys, especially his doll named "Woody". But, what do the toys do when Andy is not with them, they come to life. Woody believes that he has life (as a toy) good. However, he must worry about Andy''s family moving, and what Woody does not know is about Andy''s birthday party. Woody does not realize that Andy''s mother gave him an action figure known as Buzz Lightyear, who does not believe that he is a toy, and quickly becomes Andy''s new favorite toy. Woody, who is now consumed with jealousy, tries to get rid of Buzz. Then, both Woody and Buzz are now lost. They must find a way to get back to Andy before he moves without them, but they will have to pass through a ruthless toy killer, Sid Phillips.                Written by John Wiggins tags: Animation Adventure Comedy Tom Hanks Tim Allen Don Rickles ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('28629868-99ec-408e-aec4-ea13783b5897','Ladri di biciclette','1948','MV5BOTI3NTcwNzEtNDA1MS00ZjE0LThkNDEtMmU4MjVmNTQ1ZDBmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY450_SX312_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('28629868-99ec-408e-aec4-ea13783b5897','Ricci, an unemployed man in the depressed post-WWII economy of Italy, gets at last a good job - for which he needs a bike - hanging up posters. But soon his bicycle is stolen. He and his son walk the streets of Rome, looking for the bicycle. Ricci finally manages to locate the thief but with no proof, he has to abandon his cause. But he and his son know perfectly well that without a bike, Ricci won''t be able to keep his job.                Written by jolusoma tags: Drama Lamberto Maggiorani Enzo Staiola Lianella Carell ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('bf8d0b02-7298-4ab3-805f-bcbf97e00034','The Kid','1921','MV5BZjhhMThhNDItNTY2MC00MmU1LTliNDEtNDdhZjdlNTY5ZDQ1XkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('bf8d0b02-7298-4ab3-805f-bcbf97e00034','The opening title reads: "A comedy with a smile--and perhaps a tear". As she leaves the charity hospital and passes a church wedding, Edna deposits her new baby with a pleading note in a limousine and goes off to commit suicide. The limo is stolen by thieves who dump the baby by a garbage can. Charlie the Tramp finds the baby and makes a home for him. Five years later Edna has become an opera star but does charity work for slum youngsters in hope of finding her boy. A doctor called by Edna discovers the note with the truth about the Kid and reports it to the authorities who come to take him away from Charlie. Before he arrives at the Orphan Asylum Charlie steals him back and takes him to a flophouse. The proprietor reads of a reward for the Kid and takes him to Edna. Charlie is later awakened by a kind policeman who reunites him with the Kid at Edna''s mansion.                Written by Ed Stephan <stephan@cc.wwu.edu> tags: Comedy Drama Family Charles Chaplin Edna Purviance Jackie Coogan ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('4d8c1a2b-65be-4e46-9be6-0578c8bfcee8','Inglourious Basterds','2009','MV5BOTJiNDEzOWYtMTVjOC00ZjlmLWE0NGMtZmE1OWVmZDQ2OWJhXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('4d8c1a2b-65be-4e46-9be6-0578c8bfcee8','In German-occupied France, young Jewish refugee Shosanna Dreyfus witnesses the slaughter of her family by Colonel Hans Landa. Narrowly escaping with her life, she plots her revenge several years later when German war hero Fredrick Zoller takes a rapid interest in her and arranges an illustrious movie premiere at the theater she now runs. With the promise of every major Nazi officer in attendance, the event catches the attention of the "Basterds", a group of Jewish-American guerrilla soldiers led by the ruthless Lt. Aldo Raine. As the relentless executioners advance and the conspiring young girl''s plans are set in motion, their paths will cross for a fateful evening that will shake the very annals of history.                Written by The Massie Twins tags: Adventure Drama War Brad Pitt Diane Kruger Eli Roth ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('c61ee9c6-be46-423f-a67d-e7b795865766','Snatch','2000','MV5BMTA2NDYxOGYtYjU1Mi00Y2QzLTgxMTQtMWI1MGI0ZGQ5MmU4XkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_SY500_SX342_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('c61ee9c6-be46-423f-a67d-e7b795865766','Turkish and his close friend/accomplice Tommy get pulled into the world of match fixing by the notorious Brick Top. Things get complicated when the boxer they had lined up gets badly beaten by Pitt, a ''pikey'' ( slang for an Irish Gypsy)- who comes into the equation after Turkish, an unlicensed boxing promoter wants to buy a caravan off the Irish Gypsies. They then try to convince Pitt not only to fight for them, but to lose for them too. Whilst all this is going on, a huge diamond heist takes place, and a fistful of motley characters enter the story, including ''Cousin Avi'', ''Boris The Blade'', ''Franky Four Fingers'' and ''Bullet Tooth Tony''. Things go from bad to worse as it all becomes about the money, the guns, and the damned dog!                Written by Filmtwob <webmaster@filmfreak.co.za> tags: Comedy Crime Jason Statham Brad Pitt Benicio Del Toro ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d0f71a87-4f3a-41d0-990c-834869d91290','Monty Python and the Holy Grail','1975','MV5BNmNmZjViNTQtNDQ5Mi00MDYzLWI5YWMtNDUyZGNmMGZhNDg4XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SY384_SX260_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d0f71a87-4f3a-41d0-990c-834869d91290','History is turned on its comic head when, in 10th century England, King Arthur travels the countryside to find knights who will join him at the Round Table in Camelot. Gathering up the men is a tale in itself but after a bit of a party at Camelot, many decide to leave only to be stopped by God who sends them on a quest: to find the Holy Grail. After a series of individual adventures, the knights are reunited but must face a wizard named Tim, killer rabbits and lessons in the use of holy hand grenades. Their quest comes to an end however when the police intervene - just what you would expect in a Monty Python movie.                Written by garykmcd tags: Adventure Comedy Fantasy Graham Chapman John Cleese Eric Idle ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('a903168e-74a2-42f2-abae-a90578efbaae','3 Idiots','2009','MV5BZWRlNDdkNzItMzhlZC00YTdmLWIwNjktYjY5NjQ1ZmQ3N2FkXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,373,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('a903168e-74a2-42f2-abae-a90578efbaae','Farhan Qureshi and Raju Rastogi want to re-unite with their fellow collegian, Rancho, after faking a stroke aboard an Air India plane, and excusing himself from his wife - trouser less - respectively. Enroute, they encounter another student, Chatur Ramalingam, now a successful businessman, who reminds them of a bet they had undertaken 10 years ago. The trio, while recollecting hilarious antics, including their run-ins with the Dean of Delhi''s Imperial College of Engineering, Viru Sahastrabudhe, race to locate Rancho, at his last known address - little knowing the secret that was kept from them all this time.                Written by rAjOo (gunwanti@hotmail.com) tags: Comedy Drama Aamir Khan Madhavan Mona Singh ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('4de3fe21-266c-444c-a0d3-8ada5bbd34bc','L.A. Confidential','1997','MV5BMDBlYzAwZDktNzM2MS00YzBlLWI4ODQtZTlkNmMxZDc3NGRkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY429_SX290_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('4de3fe21-266c-444c-a0d3-8ada5bbd34bc','1950''s Los Angeles is the seedy backdrop for this intricate noir-ish tale of police corruption and Hollywood sleaze. Three very different cops are all after the truth, each in their own style: Ed Exley, the golden boy of the police force, willing to do almost anything to get ahead, except sell out; Bud White, ready to break the rules to seek justice, but barely able to keep his raging violence under control; and Jack Vincennes, always looking for celebrity and a quick buck until his conscience drives him to join Exley and White down the one-way path to find the truth behind the dark world of L.A. crime.                Written by Greg Bole <bole@life.bio.sunysb.edu> tags: Crime Drama Mystery Kevin Spacey Russell Crowe Guy Pearce ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('c0b8ded0-820e-47d6-8d9d-e554e0d116c9','Per qualche dollaro in più','1965','MV5BMTQzMjIzOTEzMF5BMl5BanBnXkFtZTcwMTUzNTk3NA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('c0b8ded0-820e-47d6-8d9d-e554e0d116c9','Monco is a bounty killer chasing El Indio and his gang. During his hunting, he meets Col. Douglas Mortimer, another bounty killer, and they decide to make a partnership, chase the bad guys together and split the reward. During their enterprise, there will be lots of bullets and funny situations. In the end, one of the bounty hunters shows the real intention of his hunting.                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Western Clint Eastwood Lee Van Cleef Gian Maria Volontè ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('7073cda3-90b9-48c7-945e-7ddfb28cf226','Scarface','1983','MV5BMjAzOTM4MzEwNl5BMl5BanBnXkFtZTgwMzU1OTc1MDE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('7073cda3-90b9-48c7-945e-7ddfb28cf226','Tony Montana manages to leave Cuba during the Mariel exodus of 1980. He finds himself in a Florida refugee camp but his friend Manny has a way out for them: undertake a contract killing and arrangements will be made to get a green card. He''s soon working for drug dealer Frank Lopez and shows his mettle when a deal with Colombian drug dealers goes bad. He also brings a new level of violence to Miami. Tony is protective of his younger sister but his mother knows what he does for a living and disowns him. Tony is impatient and wants it all however, including Frank''s empire and his mistress Elvira Hancock. Once at the top however, Tony''s outrageous actions make him a target and everything comes crumbling down.                Written by garykmcd tags: Crime Drama Al Pacino Michelle Pfeiffer Steven Bauer ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('76a7a6e4-09b5-4b17-ac7d-06e797405b27','Jagten','2012','MV5BMTg2NDg3ODg4NF5BMl5BanBnXkFtZTcwNzk3NTc3Nw@@._V1_SY500_CR0,0,349,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('76a7a6e4-09b5-4b17-ac7d-06e797405b27','Lucas is a Kindergarten teacher who takes great care of his students. Unfortunately for him, young Klara has a run-away imagination and concocts a lie about her teacher. Before Lucas is even able to understand the consequences, he has become the outcast of the town. The hunt is on to prove his innocence before it''s taken from him for good.                Written by napierslogs tags: Drama Mads Mikkelsen Thomas Bo Larsen Annika Wedderkopp ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('667f5c22-ac92-40f6-bd64-86d1de8d2f16','Rashômon','1950','MV5BMTk1MDU5MjQ5NF5BMl5BanBnXkFtZTgwMDM2OTE4MzE@._V1_SY500_CR0,0,351,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('667f5c22-ac92-40f6-bd64-86d1de8d2f16','A priest, a woodcutter and another man are taking refuge from a rainstorm in the shell of a former gatehouse called Rashômon. The priest and the woodcutter are recounting the story of a murdered samurai whose body the woodcutter discovered three days earlier in a forest grove. Both were summoned to testify at the murder trial, the priest who ran into the samurai and his wife traveling through the forest just before the murder occurred. Three other people who testified at the trial are supposedly the only direct witnesses: a notorious bandit named Tajômaru, who allegedly murdered the samurai and raped his wife; the white veil cloaked wife of the samurai; and the samurai himself who testifies through the use of a medium. The three tell a similarly structured story - that Tajômaru kidnapped and bound the samurai so that he could rape the wife - but which ultimately contradict each other, the motivations and the actual killing being what differ. The woodcutter reveals at Rashômon that he ...                Written by Huggo tags: Crime Drama Mystery Toshirô Mifune Machiko Kyô Masayuki Mori ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('008154ad-7136-4096-9697-0544cd7e0c82','The Apartment','1960','MV5BN2YxYmUyZGItZWEzYy00NTA3LThhM2UtZThhNDM5NWYxZGQ1L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,323,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('008154ad-7136-4096-9697-0544cd7e0c82','As of November 1, 1959, mild mannered C.C. Baxter has been working at Consolidated Life, an insurance company, for close to four years, and is one of close to thirty-two thousand employees located in their Manhattan head office. To distinguish himself from all the other lowly cogs in the company in the hopes of moving up the corporate ladder, he often works late, but only because he can''t get into his apartment, located off of Central Park West, since he has provided it to a handful of company executives - Mssrs. Dobisch, Kirkeby, Vanderhoff and Eichelberger - on a rotating basis for their extramarital liaisons in return for a good word to the personnel director, Jeff D. Sheldrake. When Baxter is called into Sheldrake''s office for the first time, he learns that it isn''t just to be promoted as he expects, but also to add married Sheldrake to the list to who he will lend his apartment. What Baxter is unaware of is that Sheldrake''s mistress is Fran Kubelik, an elevator girl in the ...                Written by Huggo tags: Comedy Drama Romance Jack Lemmon Shirley MacLaine Fred MacMurray ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('effdb6d9-dc8f-4494-8073-645ef4b5e276','Good Will Hunting','1997','MV5BOTI0MzcxMTYtZDVkMy00NjY1LTgyMTYtZmUxN2M3NmQ2NWJhXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,327,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('effdb6d9-dc8f-4494-8073-645ef4b5e276','A touching tale of a wayward young man who struggles to find his identity, living in a world where he can solve any problem, except the one brewing deep within himself, until one day he meets his soul mate who opens his mind and his heart.                Written by Dima & Danielle tags: Drama Robin Williams Matt Damon Ben Affleck ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('dafde607-f23c-467c-ad78-177e0fbc4457','Jodaeiye Nader az Simin','2011','MV5BMTYzMzU4NDUwOF5BMl5BanBnXkFtZTcwMTM5MjA5Ng@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('dafde607-f23c-467c-ad78-177e0fbc4457','Nader (Peyman Moaadi) and Simin (Leila Hatami) argue about living abroad. Simin prefers to live abroad to provide better opportunities for their only daughter, Termeh. However, Nader refuses to go because he thinks he must stay in Iran and take care of his father (Ali-Asghar Shahbazi), who suffers from Alzheimers. However, Simin is determined to get a divorce and leave the country with her daughter.                Written by Amin Davoodi tags: Drama Mystery Peyman Moaadi Leila Hatami Sareh Bayat ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('bb5e9968-eb34-455b-9181-c3ac2bd024cc','Indiana Jones and the Last Crusade','1989','MV5BMjNkMzc2N2QtNjVlNS00ZTk5LTg0MTgtODY2MDAwNTMwZjBjXkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_SY500_CR0,0,339,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('bb5e9968-eb34-455b-9181-c3ac2bd024cc','An art collector appeals to Jones to embark on a search for the Holy Grail. He learns that another archaeologist has disappeared while searching for the precious goblet, and the missing man is his own father, Dr. Henry Jones. The artifact is much harder to find than they expected, and its powers are too much for those impure in heart.                Written by Jwelch5742 tags: Action Adventure Fantasy Harrison Ford Sean Connery Alison Doody ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('77c67d93-d628-4a52-b8c5-63dc6687d8ac','Metropolis','1927','MV5BNDAzNTkyODg1MF5BMl5BanBnXkFtZTgwMDA3NDkwMDE@._V1_SY250_CR0,0,161,250_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('77c67d93-d628-4a52-b8c5-63dc6687d8ac','Sometime in the future, the city of Metropolis is home to a Utopian society where its wealthy residents live a carefree life. One of those is Freder Fredersen. One day, he spots a beautiful woman with a group of children, she and the children who quickly disappear. Trying to follow her, he, oblivious to such, is horrified to find an underground world of workers, apparently who run the machinery which keeps the above ground Utopian world functioning. One of the few people above ground who knows about the world below is Freder''s father, Joh Fredersen, who is the founder and master of Metropolis. Freder learns that the woman is Maria, who espouses the need to join the "hands" - the workers - to the "head" - those in power above - by a mediator or the "heart". Freder wants to help the plight of the workers in the want for a better life. But when Joh learns of what Maria is espousing and that Freder is joining their cause, Joh, with the assistance of an old colleague and now nemesis named ...                Written by Huggo tags: Drama Sci-Fi Brigitte Helm Alfred Abel Gustav Fröhlich ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('1fb399d1-a1e0-45eb-aba8-108608c1c8aa','All About Eve','1950','MV5BMTY2MTAzODI5NV5BMl5BanBnXkFtZTgwMjM4NzQ0MjE@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('1fb399d1-a1e0-45eb-aba8-108608c1c8aa','Eve (Anne Baxter) is waiting backstage to meet her "idol" aging Broadway Star, Margo Channing (Bette Davis). It all seems innocent enough as Eve explains that she has seen Margo in EVERY performance of the current play she is in. Only Playright critic DeWitt (George Sanders) sees through Eve''s evil plan, which is to take her parts and fiancé, Bill Simpson (Gary Merrill) When the fiancé shows no interest, she tries for playwright Hugh Marlowe (Lloyd Richards) but DeWitt stops her. After she accepts her award, she decides to skip the after-party and goes to her room, where we find a young woman named Phoebe, who snuck into her room and fell asleep. This is where the "Circle of Life" now comes to fruition as Eve is going to get played the way she did Margo. tags: Drama Bette Davis Anne Baxter George Sanders ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('10b7b520-5a29-4da2-ad3d-eafd112359d0','Yôjinbô','1961','MV5BZThiZjAzZjgtNDU3MC00YThhLThjYWUtZGRkYjc2ZWZlOTVjXkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('10b7b520-5a29-4da2-ad3d-eafd112359d0','Sanjuro, a wandering samurai enters a rural town in nineteenth century Japan. After learning from the innkeeper that the town is divided between two gangsters, he plays one side off against the other. His efforts are complicated by the arrival of the wily Unosuke, the son of one of the gangsters, who owns a revolver. Unosuke has Sanjuro beaten after he reunites an abducted woman with her husband and son, then massacres his father''s opponents. During the slaughter, the samurai escapes with the help of the innkeeper; but while recuperating at a nearby temple, he learns of innkeeper''s abduction by Unosuke, and returns to the town to confront him.                Written by Bernard Keane <BKeane2@email.dot.gov.au> tags: Action Drama Thriller Toshirô Mifune Eijirô Tôno Tatsuya Nakadai ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2f2b029f-87de-4303-8d64-46ec3a34e741','Batman Begins','2005','MV5BNTM3OTc0MzM2OV5BMl5BanBnXkFtZTYwNzUwMTI3._V1_SY360_CR0,0,242,360_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2f2b029f-87de-4303-8d64-46ec3a34e741','When his parents are killed, billionaire playboy Bruce Wayne relocates to Asia where he is mentored by Henri Ducard and Ra''s Al Ghul in how to fight evil. When learning about the plan to wipe out evil in Gotham City by Ducard, Bruce prevents this plan from getting any further and heads back to his home. Back in his original surroundings, Bruce adopts the image of a bat to strike fear into the criminals and the corrupt as the icon known as ''Batman''. But it doesn''t stay quiet for long.                Written by konstantinwe tags: Action Adventure Christian Bale Michael Caine Ken Watanabe ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('63852536-bbba-4d5f-8b2f-bc38ed57dcc6','Up','2009','MV5BMTk3NDE2NzI4NF5BMl5BanBnXkFtZTgwNzE1MzEyMTE@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('63852536-bbba-4d5f-8b2f-bc38ed57dcc6','Carl Fredricksen as a boy wanted to explore South America and find the forbidden Paradise Falls. About 64 years later he gets to begin his journey along with a Boy Scout named Russel with help from 500 balloons. On their journey they discover many new friends including a talking dog and Carl and Russel figure out that someone evil plans. Carl soon realizes that this evildoer is his childhood idol. Will they be able to defeat him and will they find Paradise Falls? tags: Animation Adventure Comedy Edward Asner Jordan Nagai John Ratzenberger ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('f535038b-00b4-4153-8ec0-12a4b6c5de21','Some Like It Hot','1959','MV5BNzAyOGIxYjAtMGY2NC00ZTgyLWIwMWEtYzY0OWQ4NDFjOTc5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,322,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('f535038b-00b4-4153-8ec0-12a4b6c5de21','When two Chicago musicians, Joe and Jerry, witness the the St. Valentine''s Day massacre, they want to get out of town and get away from the gangster responsible, Spats Colombo. They''re desperate to get a gig out of town but the only job they know of is in an all-girl band heading to Florida. They show up at the train station as Josephine and Daphne, the replacement saxophone and bass players. They certainly enjoy being around the girls, especially Sugar Kane Kowalczyk who sings and plays the ukulele. Joe in particular sets out to woo her while Jerry/Daphne is wooed by a millionaire, Osgood Fielding III. Mayhem ensues as the two men try to keep their true identities hidden and Spats Colombo and his crew show up for a meeting with several other crime lords.                Written by garykmcd tags: Comedy Romance Marilyn Monroe Tony Curtis Jack Lemmon ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d4adb98f-9ea4-4826-8b56-1ecab0da6083','The Treasure of the Sierra Madre','1948','MV5BMTQ4MzUzOTYwOV5BMl5BanBnXkFtZTgwNDA4MzgyMjE@._V1_SY250_SX169_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d4adb98f-9ea4-4826-8b56-1ecab0da6083','Fred C. Dobbs and Bob Curtin, both down on their luck in Tampico, Mexico in 1925, meet up with a grizzled prospector named Howard and decide to join with him in search of gold in the wilds of central Mexico. Through enormous difficulties, they eventually succeed in finding gold, but bandits, the elements, and most especially greed threaten to turn their success into disaster.                Written by Jim Beaver <jumblejim@prodigy.net> tags: Adventure Drama Western Humphrey Bogart Walter Huston Tim Holt ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('07049665-9b73-43a9-bf86-e5851a49e0c8','Unforgiven','1992','MV5BODM3YWY4NmQtN2Y3Ni00OTg0LWFhZGQtZWE3ZWY4MTJlOWU4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('07049665-9b73-43a9-bf86-e5851a49e0c8','The town of Big Whisky is full of normal people trying to lead quiet lives. Cowboys try to make a living. Sheriff ''Little Bill'' tries to build a house and keep a heavy-handed order. The town whores just try to get by.Then a couple of cowboys cut up a whore. Dissatisfied with Bill''s justice, the prostitutes put a bounty on the cowboys. The bounty attracts a young gun billing himself as ''The Schofield Kid'', and aging killer William Munny. Munny reformed for his young wife, and has been raising crops and two children in peace. But his wife is gone. Farm life is hard. And Munny is no good at it. So he calls his old partner Ned, saddles his ornery nag, and rides off to kill one more time, blurring the lines between heroism and villainy, man and myth.                Written by Charlie Ness tags: Drama Western Clint Eastwood Gene Hackman Morgan Freeman ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('5e1881a8-4017-430f-a979-fbbb4879d7d2','Der Untergang','2004','MV5BMTM1OTI1MjE2Nl5BMl5BanBnXkFtZTcwMTEwMzc4NA@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('5e1881a8-4017-430f-a979-fbbb4879d7d2','In April of 1945, Germany stands at the brink of defeat with the Soviet Armies closing in from the west and south. In Berlin, capital of the Third Reich, Adolf Hitler proclaims that Germany will still achieve victory and orders his Generals and advisers to fight to the last man. "Downfall" explores these final days of the Reich, where senior German leaders (such as Himmler and Goring) began defecting from their beloved Fuhrer, in an effort to save their own lives, while still others (Joseph Goebbels) pledge to die with Hitler. Hitler, himself, degenerates into a paranoid shell of a man, full of optimism one moment and suicidal depression the next. When the end finally does comes, and Hitler lies dead by his own hand, what is left of his military must find a way to end the killing that is the Battle of Berlin, and lay down their arms in surrender.                Written by Anthony Hughes {husnock31@hotmail.com} tags: Biography Drama History Bruno Ganz Alexandra Maria Lara Ulrich Matthes ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('693cfce8-729c-46a8-b174-fe4138dcabc4','Raging Bull','1980','MV5BMjIxOTg3OTc5MF5BMl5BanBnXkFtZTcwNzkwNjMwNA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('693cfce8-729c-46a8-b174-fe4138dcabc4','When Jake LaMotta steps into a boxing ring and obliterates his opponent, he''s a prizefighter. But when he treats his family and friends the same way, he''s a ticking time bomb, ready to go off at any moment. Though LaMotta wants his family''s love, something always seems to come between them. Perhaps it''s his violent bouts of paranoia and jealousy. This kind of rage helped make him a champ, but in real life, he winds up in the ring alone.                Written by alfiehitchie tags: Biography Drama Sport Robert De Niro Cathy Moriarty Joe Pesci ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('18452c6a-8507-41cc-b09b-1e3956c5f780','The Third Man','1949','MV5BZjQxYmRkMjgtMmZkZi00ZDYyLTk5OTktZWZjZTEzNGZlMWEwXkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY500_CR0,0,335,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('18452c6a-8507-41cc-b09b-1e3956c5f780','An out of work pulp fiction novelist, Holly Martins, arrives in a post war Vienna divided into sectors by the victorious allies, and where a shortage of supplies has lead to a flourishing black market. He arrives at the invitation of an ex-school friend, Harry Lime, who has offered him a job, only to discover that Lime has recently died in a peculiar traffic accident. From talking to Lime''s friends and associates Martins soon notices that some of the stories are inconsistent, and determines to discover what really happened to Harry Lime.                Written by Mark Thompson <mrt@oasis.icl.co.uk> tags: Film-Noir Mystery Thriller Orson Welles Joseph Cotten Alida Valli ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('3e16bd4c-96fc-4b85-bf94-eb97a24d29bb','Die Hard','1988','MV5BMzNmY2IwYzAtNDQ1NC00MmI4LThkOTgtZmVhYmExOTVhMWRkXkEyXkFqcGdeQXVyMTk5NDA3Nw@@._V1_SX290_CR0,0,290,429_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('3e16bd4c-96fc-4b85-bf94-eb97a24d29bb','NYPD cop John McClane goes on a Christmas vacation to visit his wife Holly in Los Angeles where she works for the Nakatomi Corporation. While they are at the Nakatomi headquarters for a Christmas party, a group of bank robbers led by Hans Gruber take control of the building and hold everyone hostage, with the exception of John, while they plan to perform a lucrative heist. Unable to escape and with no immediate police response, John is forced to take matters into his own hands.                Written by Sam tags: Action Thriller Bruce Willis Alan Rickman Bonnie Bedelia ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('7147c3f2-b826-482f-9303-3a48899e49ad','Bacheha-Ye aseman','1997','MV5BZTYwZWQ4ZTQtZWU0MS00N2YwLWEzMDItZWFkZWY0MWVjODVhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY446_SX290_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('7147c3f2-b826-482f-9303-3a48899e49ad','Zahra''s shoes are gone; her older brother Ali lost them. They are poor, there are no shoes for Zahra until they come up with an idea: they will share one pair of shoes, Ali''s. School awaits. Will the plan succeed?                Written by Eileen Berdon <eberdon@aol.com> tags: Drama Family Mohammad Amir Naji Amir Farrokh Hashemian Bahare Seddiqi ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('45ef88da-3a04-4eb6-9f6b-622828be5e81','Heat','1995','MV5BNGMwNzUwNjYtZWM5NS00YzMyLWI4NjAtNjM0ZDBiMzE1YWExXkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_SY500_CR0,0,339,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('45ef88da-3a04-4eb6-9f6b-622828be5e81','Hunters and their prey--Neil and his professional criminal crew hunt to score big money targets (banks, vaults, armored cars) and are, in turn, hunted by Lt. Vincent Hanna and his team of cops in the Robbery/Homicide police division. A botched job puts Hanna onto their trail while they regroup and try to put together one last big ''retirement'' score. Neil and Vincent are similar in many ways, including their troubled personal lives. At a crucial moment in his life, Neil disobeys the dictum taught to him long ago by his criminal mentor--''Never have anything in your life that you can''t walk out on in thirty seconds flat, if you spot the heat coming around the corner''--as he falls in love. Thus the stage is set for the suspenseful ending....                Written by Tad Dibbern <DIBBERN_D@a1.mscf.upenn.edu> tags: Action Crime Drama Al Pacino Robert De Niro Val Kilmer ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('5b6611dd-bbab-47b3-a028-7466cc121750','The Great Escape','1963','MV5BMjI2MTQwNDI3Nl5BMl5BanBnXkFtZTcwNDk4MTkzNA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('5b6611dd-bbab-47b3-a028-7466cc121750','Based on a true story, a group of allied escape artist-type prisoners-of-war (POWs) are all put in an ''escape proof'' camp. Their leader decides to try to take out several hundred all at once. The first half of the film is played for comedy as the prisoners mostly outwit their jailers to dig the escape tunnel. The second half is high adventure as they use boats and trains and planes to get out of occupied Europe.                Written by John Vogel <jlvogel@comcast.net> tags: Adventure Drama History Steve McQueen James Garner Richard Attenborough ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('01a5b434-b073-47b5-b606-65c9af191d81','Chinatown','1974','MV5BMTZiZTA5MWItNTgyMy00ZGZkLThjNGQtOWI0MTU5ZDI4NmJmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('01a5b434-b073-47b5-b606-65c9af191d81','JJ ''Jake'' Gittes is a private detective who seems to specialize in matrimonial cases. He is hired by Evelyn Mulwray when she suspects her husband Hollis, builder of the city''s water supply system, of having an affair. Gittes does what he does best and photographs him with a young girl but in the ensuing scandal, it seems he was hired by an impersonator and not the real Mrs. Mulwray. When Mr. Mulwray is found dead, Jake is plunged into a complex web of deceit involving murder, incest and municipal corruption all related to the city''s water supply.                Written by garykmcd tags: Drama Mystery Thriller Jack Nicholson Faye Dunaway John Huston ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('de5a4b0a-866d-438c-b3cc-eaa424b092f3','El laberinto del fauno','2006','MV5BMTU3ODg2NjQ5NF5BMl5BanBnXkFtZTcwMDEwODgzMQ@@._V1_SY500_CR0,0,339,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('de5a4b0a-866d-438c-b3cc-eaa424b092f3','In 1944 falangist Spain, a girl, fascinated with fairy-tales, is sent along with her pregnant mother to live with her new stepfather, a ruthless captain of the Spanish army. During the night, she meets a fairy who takes her to an old faun in the center of the labyrinth. He tells her she''s a princess, but must prove her royalty by surviving three gruesome tasks. If she fails, she will never prove herself to be the the true princess and will never see her real father, the king, again.                Written by Tim tags: Drama Fantasy War Ivana Baquero Ariadna Gil Sergi López ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('4c760df5-ccd0-4973-9666-e414d70fea43','Inside Out','2015','MV5BOTgxMDQwMDk0OF5BMl5BanBnXkFtZTgwNjU5OTg2NDE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('4c760df5-ccd0-4973-9666-e414d70fea43','Growing up can be a bumpy road, and it''s no exception for Riley, who is uprooted from her Midwest life when her father starts a new job in San Francisco. Like all of us, Riley is guided by her emotions - Joy, Fear, Anger, Disgust and Sadness. The emotions live in Headquarters, the control center inside Riley''s mind, where they help advise her through everyday life. As Riley and her emotions struggle to adjust to a new life in San Francisco, turmoil ensues in Headquarters. Although Joy, Riley''s main and most important emotion, tries to keep things positive, the emotions conflict on how best to navigate a new city, house and school.                Written by Pixar tags: Animation Adventure Comedy Amy Poehler Bill Hader Lewis Black ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('808020e6-f2b0-4bba-abd1-95772afeaae8','Ikiru','1952','MV5BMWU4NmM5OTgtODk1MC00NThiLThkNjMtOWM5MGIzYjEyNmY5XkEyXkFqcGdeQXVyNTI4MjkwNjA@._V1_SY500_CR0,0,355,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('808020e6-f2b0-4bba-abd1-95772afeaae8','Kanji Watanabe is a civil servant. He has worked in the same department for 30 years. His life is pretty boring and monotonous, though he once used to have passion and drive. Then one day he discovers that he has stomach cancer and has less than a year to live. After the initial depression he sets about living for the first time in over 20 years. Then he realises that his limited time left is not just for living life to the full but to leave something meaningful behind...                Written by grantss tags: Drama Takashi Shimura Nobuo Kaneko Shin''ichi Himori ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('1604e030-5928-47c3-ba56-7a39b0d2830c','Tonari no Totoro','1988','MV5BMjE3NzY5ODQwMV5BMl5BanBnXkFtZTcwNzY1NzcxNw@@._V1_SY345_SX250_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('1604e030-5928-47c3-ba56-7a39b0d2830c','Two young girls, Satsuki and her younger sister Mei, move into a house in the country with their father to be closer to their hospitalized mother. Satsuki and Mei discover that the nearby forest is inhabited by magical creatures called Totoros (pronounced toe-toe-ro). They soon befriend these Totoros, and have several magical adventures.                Written by Christopher E. Meadows <cmeadows@nyx.cs.du.edu> tags: Animation Family Fantasy Hitoshi Takagi Noriko Hidaka Chika Sakamoto ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('07260eeb-517d-420e-a1d1-da3d410d77dd','On the Waterfront','1954','MV5BMjFiZDg3MDgtOTdiNC00ZGFiLWIyN2ItNDhkNGI5ZThkOTE3XkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SX246_CR0,0,246,377_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('07260eeb-517d-420e-a1d1-da3d410d77dd','Terry Malloy dreams about being a prize fighter, while tending his pigeons and running errands at the docks for Johnny Friendly, the corrupt boss of the dockers union. Terry witnesses a murder by two of Johnny''s thugs, and later meets the dead man''s sister and feels responsible for his death. She introduces him to Father Barry, who tries to force him to provide information for the courts that will smash the dock racketeers.                Written by Colin Tinto <cst@imdb.com> tags: Crime Drama Thriller Marlon Brando Karl Malden Lee J. Cobb ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('73fee9c5-d428-4566-b7a6-e74bc575ce99','Ran','1985','MV5BZDBjZTM4ZmEtOTA5ZC00NTQzLTkyNzYtMmUxNGU2YjI5YjU5L2ltYWdlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY500_CR0,0,353,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('73fee9c5-d428-4566-b7a6-e74bc575ce99','Japanese warlord Hidetori Ichimonji decides the time has come to retire and divide his fiefdom among his three sons. His eldest and middle sons - Taro and Jiro - agree with his decision and promise to support him for his remaining days. The youngest son Saburo disagrees with all of them arguing that there is little likelihood the three brothers will remain united. Insulted by his son''s brashness, the warlord banishes Saburo. As the warlord begins his retirement, he quickly realizes that his two eldest sons selfish and have no intention of keeping their promises. It leads to war and only banished Saburo can possibly save him.                Written by garykmcd tags: Action Drama Tatsuya Nakadai Akira Terao Jinpachi Nezu ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('62516b1c-cbce-4bdd-a7d3-9a39622b4e2e','Room','2015','MV5BMjE4NzgzNzEwMl5BMl5BanBnXkFtZTgwMTMzMDE0NjE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('62516b1c-cbce-4bdd-a7d3-9a39622b4e2e','ROOM tells the extraordinary story of Jack, a spirited 5-year-old who is looked after by his loving and devoted mother. Like any good mother, Ma dedicates herself to keeping Jack happy and safe, nurturing him with warmth and love and doing typical things like playing games and telling stories. Their life, however, is anything but typical--they are trapped--confined to a 10-by-10-foot space that Ma has euphemistically named Room. Ma has created a whole universe for Jack within Room, and she will stop at nothing to ensure that, even in this treacherous environment, Jack is able to live a complete and fulfilling life. But as Jack''s curiosity about their situation grows, and Ma''s resilience reaches its breaking point, they enact a risky plan to escape, ultimately bringing them face-to-face with what may turn out to be the scariest thing yet: the real world.                Written by A24 tags: Drama Brie Larson Jacob Tremblay Sean Bridgers ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('8b30d528-1597-4229-927d-502db814e5d6','The Gold Rush','1925','MV5BZjEyOTE4MzMtNmMzMy00Mzc3LWJlOTQtOGJiNDE0ZmJiOTU4L2ltYWdlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY459_CR0,0,335,459_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('8b30d528-1597-4229-927d-502db814e5d6','A lone prospector ventures into Alaska looking for gold. He gets mixed up with some burly characters and falls in love with the beautiful Georgia. He tries to win her heart with his singular charm.                Written by John J. Magee <magee@helix.mgh.harvard.edu> tags: Adventure Comedy Drama Charles Chaplin Mack Swain Tom Murray ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('49bc4c80-c005-4fe4-b56e-9c32b908c0d0','Hacksaw Ridge','2016','MV5BMjQ1NjM3MTUxNV5BMl5BanBnXkFtZTgwMDc5MTY5OTE@._V1_SY500_CR0,0,323,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('49bc4c80-c005-4fe4-b56e-9c32b908c0d0','The true story of Desmond Doss, the conscientious objector who, at the Battle of Okinawa, won the Medal of Honor for his incredible bravery and regard for his fellow soldiers. We see his upbringing and how this shaped his views, especially his religious view and anti-killing stance. We see Doss''s trials and tribulations after enlisting in the US Army and trying to become a medic. Finally, we see the hell on Earth that was Hacksaw Ridge.                Written by grantss tags: Drama History War Andrew Garfield Sam Worthington Luke Bracey ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('1bcdd556-dab3-4f20-8b3f-619f105e97ee','El secreto de sus ojos','2009','MV5BMTgwNTI3OTczOV5BMl5BanBnXkFtZTcwMTM3MTUyMw@@._V1_SX338_CR0,0,338,499_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('1bcdd556-dab3-4f20-8b3f-619f105e97ee','In 1999, retired Argentinian federal justice agent Benjamín Espósito is writing a novel, using an old closed case as the source material. That case is the brutal rape and murder of Liliana Coloto. In addition to seeing the extreme grief of the victim''s husband Ricardo Morales, Benjamín, his assistant Pablo Sandoval, and newly hired department chief Irene Menéndez-Hastings were personally affected by the case as Benjamín and Pablo tracked the killer, hence the reason why the unsatisfactory ending to the case has always bothered him. Despite the department already having two other suspects, Benjamín and Pablo ultimately were certain that a man named Isidoro Gómez is the real killer. Although he is aware that historical accuracy is not paramount for the novel, the process of revisiting the case is more an issue of closure for him. He tries to speak to the key players in the case, most specifically Irene, who still works in the justice department and who he has always been attracted to ...                Written by Huggo tags: Drama Mystery Thriller Ricardo Darín Soledad Villamil Pablo Rago ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('5f1b201c-4d11-42bb-a75f-7ea88a827605','The Bridge on the River Kwai','1957','MV5BZTZmNjgyZmUtOTI2MC00MDI4LWJjOTgtNjc2YTc4YTk0ZWJjL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY500_CR0,0,323,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('5f1b201c-4d11-42bb-a75f-7ea88a827605','The film deals with the situation of British prisoners of war during World War II who are ordered to build a bridge to accommodate the Burma-Siam railway. Their instinct is to sabotage the bridge but, under the leadership of Colonel Nicholson, they are persuaded that the bridge should be constructed as a symbol of British morale, spirit and dignity in adverse circumstances. At first, the prisoners admire Nicholson when he bravely endures torture rather than compromise his principles for the benefit of the Japanese commandant Saito. He is an honorable but arrogant man, who is slowly revealed to be a deluded obsessive. He convinces himself that the bridge is a monument to British character, but actually is a monument to himself, and his insistence on its construction becomes a subtle form of collaboration with the enemy. Unknown to him, the Allies have sent a mission into the jungle, led by Warden and an American, Shears, to blow up the bridge.                Written by alfiehitchie tags: Adventure Drama War William Holden Alec Guinness Jack Hawkins ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d79e3c5d-35a8-4686-9ba7-284b1169dabe','Blade Runner','1982','MV5BZWZlYmEyYTItNGRjYy00ZmMxLWEzMWItM2Q2NjZlNTMwMjQ3XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d79e3c5d-35a8-4686-9ba7-284b1169dabe','In the futuristic year of 2019, Los Angeles has become a dark and depressing metropolis, filled with urban decay. Rick Deckard, an ex-cop, is a "Blade Runner". Blade runners are people assigned to assassinate "replicants". The replicants are androids that look like real human beings. When four replicants commit a bloody mutiny on the Off World colony, Deckard is called out of retirement to track down the androids. As he tracks the replicants, eliminating them one by one, he soon comes across another replicant, Rachel, who evokes human emotion, despite the fact that she''s a replicant herself. As Deckard closes in on the leader of the replicant group, his true hatred toward artificial intelligence makes him question his own identity in this future world, including what''s human and what''s not human.                Written by blazesnakes9 tags: Sci-Fi Thriller Harrison Ford Rutger Hauer Sean Young ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('edff639c-31ff-4ae4-a3b3-a3610e34bac3','Mr. Smith Goes to Washington','1939','MV5BZTYwYjYxYzgtMDE1Ni00NzU4LWJlMTEtODQ5YmJmMGJhZjI5L2ltYWdlXkEyXkFqcGdeQXVyMDI2NDg0NQ@@._V1_SY500_CR0,0,331,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('edff639c-31ff-4ae4-a3b3-a3610e34bac3','Naive and idealistic Jefferson Smith, leader of the Boy Rangers, is appointed on a lark by the spineless governor of his state. He is reunited with the state''s senior senator--presidential hopeful and childhood hero, Senator Joseph Paine. In Washington, however, Smith discovers many of the shortcomings of the political process as his earnest goal of a national boys'' camp leads to a conflict with the state political boss, Jim Taylor. Taylor first tries to corrupt Smith and then later attempts to destroy Smith through a scandal.                Written by James Yu <jamestyu@ccwf.cc.utexas.edu> tags: Comedy Drama James Stewart Jean Arthur Claude Rains ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('1a1cec78-7e6f-48d2-b0db-bba3304e81a6','Hauru no ugoku shiro','2004','MV5BZTRhY2QwM2UtNWRlNy00ZWQwLTg3MjktZThmNjQ3NTdjN2IxXkEyXkFqcGdeQXVyMzg2MzE2OTE@._V1_SY500_CR0,0,358,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('1a1cec78-7e6f-48d2-b0db-bba3304e81a6','A love story between an 18-year-old girl named Sophie, cursed by a witch into an old woman''s body, and a magician named Howl. Under the curse, Sophie sets out to seek her fortune, which takes her to Howl''s strange moving castle. In the castle, Sophie meets Howl''s fire demon, named Karishifâ. Seeing that she is under a curse, the demon makes a deal with Sophie--if she breaks the contract he is under with Howl, then Karushifâ will lift the curse that Sophie is under, and she will return to her 18-year-old shape.                Written by Sophie Ball tags: Animation Adventure Family Chieko Baishô Takuya Kimura Tatsuya Gashûin ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('78d38bc8-2292-471f-9f69-0cc502cd0fe8','Det sjunde inseglet','1957','MV5BYWRiNDU4ODUtZDRlOS00YzRhLTk3M2ItNWQ4MGQwYWFlNTllXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY410_SX290_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('78d38bc8-2292-471f-9f69-0cc502cd0fe8','A Knight and his squire are home from the crusades. Black Death is sweeping their country. As they approach home, Death appears to the knight and tells him it is his time. The knight challenges Death to a chess game for his life. The Knight and Death play as the cultural turmoil envelopes the people around them as they try, in different ways, to deal with the upheaval the plague has caused.                Written by John Vogel <jlvogel@comcast.net> tags: Drama Fantasy Max von Sydow Gunnar Björnstrand Bengt Ekerot ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('89a6382b-ab0f-4557-bb51-44897962605a','Lock, Stock and Two Smoking Barrels','1998','MV5BMTAyN2JmZmEtNjAyMy00NzYwLThmY2MtYWQ3OGNhNjExMmM4XkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('89a6382b-ab0f-4557-bb51-44897962605a','Four Jack-the-lads find themselves heavily - seriously heavily - in debt to an East End hard man and his enforcers after a crooked card game. Overhearing their neighbours in the next flat plotting to hold up a group of out-of-their-depth drug growers, our heros decide to stitch up the robbers in turn. In a way the confusion really starts when a pair of antique double-barrelled shotguns go missing in a completely different scam.                Written by Anonymous tags: Comedy Crime Jason Flemyng Dexter Fletcher Nick Moran ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('eda423ba-67df-445e-b178-e4d0296e4e1c','Judgment at Nuremberg','1961','MV5BNDc2ODQ5NTE2MV5BMl5BanBnXkFtZTcwODExMjUyNA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('eda423ba-67df-445e-b178-e4d0296e4e1c','It has been three years since the most important Nazi leaders had already been tried. This trial is about 4 judges who used their offices to conduct Nazi sterilization and cleansing policies. Retired American judge, Dan Haywood has a daunting task ahead of him. The Cold War is heating up and no one wants any more trials as Germany, and Allied governments, want to forget the past. But is that the right thing to do is the question that the tribunal must decide.                Written by Tony Fontana <tony.fontana@spacebbs.com> tags: Drama War Spencer Tracy Burt Lancaster Richard Widmark ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('e5f94a30-3c4c-4857-90f9-6f9c8bc6d6d4','Casino','1995','MV5BMTcxOWYzNDYtYmM4YS00N2NkLTk0NTAtNjg1ODgwZjAxYzI3XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('e5f94a30-3c4c-4857-90f9-6f9c8bc6d6d4','This Martin Scorsese film depicts the Janus-like quality of Las Vegas--it has a glittering, glamorous face, as well as a brutal, cruel one. Ace Rothstein and Nicky Santoro, mobsters who move to Las Vegas to make their mark, live and work in this paradoxical world. Seen through their eyes, each as a foil to the other, the details of mob involvement in the casinos of the 1970''s and ''80''s are revealed. Ace is the smooth operator of the Tangiers casino, while Nicky is his boyhood friend and tough strongman, robbing and shaking down the locals. However, they each have a tragic flaw--Ace falls in love with a hustler, Ginger, and Nicky falls into an ever-deepening spiral of drugs and violence.                Written by Tad Dibbern <DIBBERN_D@a1.mscf.upenn.edu> tags: Biography Crime Drama Robert De Niro Sharon Stone Joe Pesci ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('dcff7bf6-e5fd-4fdb-bfcd-399781821135','Incendies','2010','MV5BMjEyNTA3Njk4M15BMl5BanBnXkFtZTcwMzkzMzY3Mw@@._V1_SY500_CR0,0,348,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('dcff7bf6-e5fd-4fdb-bfcd-399781821135','A mother''s last wishes send twins Jeanne and Simon on a journey to the Middle East in search of their tangled roots. Adapted from Wajdi Mouawad''s acclaimed play, Incendies tells the powerful and moving tale of two young adults'' voyage to the core of deep-rooted hatred, never-ending wars and enduring love.                Written by Mylène Chollet tags: Drama Mystery War Lubna Azabal Mélissa Désormeaux-Poulin Maxim Gaudette ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d3d0f648-22c0-4839-a0dd-7adf43c42d4b','A Beautiful Mind','2001','MV5BMzcwYWFkYzktZjAzNC00OGY1LWI4YTgtNzc5MzVjMDVmNjY0XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,336,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d3d0f648-22c0-4839-a0dd-7adf43c42d4b','From the heights of notoriety to the depths of depravity, John Forbes Nash, Jr. experienced it all. A mathematical genius, he made an astonishing discovery early in his career and stood on the brink of international acclaim. But the handsome and arrogant Nash soon found himself on a painful and harrowing journey of self-discovery. After many years of struggle, he eventually triumphed over his tragedy, and finally - late in life - received the Nobel Prize.                Written by Universal Pictures and DreamWorks Pictures tags: Biography Drama Russell Crowe Ed Harris Jennifer Connelly ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('e121748b-5542-4f14-974b-b86636d39906','Eskiya','1996','MV5BOGQ4ZjFmYjktOGNkNS00OWYyLWIyZjgtMGJjM2U1ZTA0ZTlhXkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SY400_SX283_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('e121748b-5542-4f14-974b-b86636d39906','The epic adventures of the legendary Baran the Bandit following his release from prison. After serving 35 years, it is no surprise that the world has changed dramatically. Still, Baran can''t help but be shocked to discover that his home village is now underwater thanks to the construction of a new dam. He then heads for Istanbul to get revenge upon his former best friend, the man who snitched on him and stole his lover Keje. Along the way, Baran teams up with Cumali, a tough young punk who finds the thief''s old-fashioned ways rather quaint. When Cumali gets into deep trouble with a crime boss, Baran adds another vengeful task to his roster.                Written by Anonymous tags: Crime Drama Thriller Sener Sen Ugur Yücel Sermin Hürmeriç ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('6adc98ab-f9c8-4764-a87f-a11a57c1f16a','The Elephant Man','1980','MV5BMDVjNjIwOGItNDE3Ny00OThjLWE0NzQtZTU3YjMzZTZjMzhkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,335,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('6adc98ab-f9c8-4764-a87f-a11a57c1f16a','John Merrick (whose real name was Joseph, as this is based on a true story) is an intelligent and friendly man, but he is hated by his Victorian-era English society because he is severely deformed. Once he is discovered by a doctor, however, he is saved from his life in a freak show and he is treated like the human being that he really is.                Written by Sam Cibula tags: Biography Drama Anthony Hopkins John Hurt Anne Bancroft ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2af24852-6b1a-4424-babd-78a72ebd211b','Smultronstället','1957','MV5BMjgwNjI3NTM1MF5BMl5BanBnXkFtZTgwNzY3MTUyMjE@._V1_SY250_SX171_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2af24852-6b1a-4424-babd-78a72ebd211b','With the exception of his elderly housekeeper Miss Agda who he treats almost like a surrogate platonic wife, widowed seventy-eight year old Dr. Isak Borg, a former medical doctor and professor, has retreated from any human contact, partly his own want but partly the decision of others who do not want to spend time with him because of his cold demeanor. He is traveling from his home in Stockholm to Lund to accept an honorary degree. Instead of flying as was the original plan, he decides to take the day long drive instead. Along for the ride is his daughter-in-law Marianne, who had been staying with him for the month but has now decided to go home. The many stops and encounters along the way make him reminisce about various parts of his life. Those stops which make him reminisce directly are at his childhood summer home, at the home of his equally emotionally cold mother, and at a gas station where the attendants praise him as a man for his work. But the lives of other people they ...                Written by Huggo tags: Drama Romance Victor Sjöström Bibi Andersson Ingrid Thulin ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('09a32d70-1764-4338-ab4a-be644c552a2d','The General','1926','MV5BODQxMzMyNTY5Nl5BMl5BanBnXkFtZTcwMDMyNTk3OA@@._V1_SY205_SX131_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('09a32d70-1764-4338-ab4a-be644c552a2d','Johnnie loves his train ("The General") and Annabelle Lee. When the Civil War begins he is turned down for service because he''s more valuable as an engineer. Annabelle thinks it''s because he''s a coward. Union spies capture The General with Annabelle on board. Johnnie must rescue both his loves.                Written by Ed Stephan <stephan@cc.wwu.edu> tags: Action Adventure Comedy Buster Keaton Marion Mack Glen Cavender ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('ace91a8a-57f1-4a13-aa2d-6a3adc4db0d4','V for Vendetta','2005','MV5BOTI5ODc3NzExNV5BMl5BanBnXkFtZTcwNzYxNzQzMw@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('ace91a8a-57f1-4a13-aa2d-6a3adc4db0d4','Tells the story of Evey Hammond and her unlikely but instrumental part in bringing down the fascist government that has taken control of a futuristic Great Britain. Saved from a life-and-death situation by a man in a Guy Fawkes mask who calls himself V, she learns a general summary of V''s past and, after a time, decides to help him bring down those who committed the atrocities that led to Britain being in the shape that it is in.                Written by ameelmore tags: Action Drama Thriller Hugo Weaving Natalie Portman Rupert Graves ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('c076acd3-1b47-48f5-9c0b-29bcdac4c158','Warrior','2011','MV5BMTk4ODk5MTMyNV5BMl5BanBnXkFtZTcwMDMyNTg0Ng@@._V1_SY500_CR0,0,324,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('c076acd3-1b47-48f5-9c0b-29bcdac4c158','Two brothers face the fight of a lifetime - and the wreckage of their broken family - within the brutal, high-stakes world of Mixed Martial Arts (MMA) fighting in Lionsgate''s action/drama, WARRIOR. A former Marine, haunted by a tragic past, Tommy Riordan returns to his hometown of Pittsburgh and enlists his father, a recovered alcoholic and his former coach, to train him for an MMA tournament awarding the biggest purse in the history of the sport. As Tommy blazes a violent path towards the title prize, his brother, Brendan, a former MMA fighter unable to make ends meet as a public school teacher, returns to the amateur ring to provide for his family. Even though years have passed, recriminations and past betrayals keep Brendan bitterly estranged from both Tommy and his father. But when Brendan''s unlikely rise as an underdog sets him on a collision course with Tommy, the two brothers must finally confront the forces that tore them apart, all the while waging the most intense, ...                Written by MConnell tags: Action Drama Sport Tom Hardy Nick Nolte Joel Edgerton ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('a4f9b144-b3c8-4719-90d6-82a778fdca68','The Wolf of Wall Street','2013','MV5BMjIxMjgxNTk0MF5BMl5BanBnXkFtZTgwNjIyOTg2MDE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('a4f9b144-b3c8-4719-90d6-82a778fdca68','Jordan Belfort is a Long Island penny stockbroker who served 22 months in prison for defrauding investors in a massive 1990s securities scam that involved widespread corruption on Wall Street and in the corporate banking world, including shoe designer Steve Madden.                Written by anonymous tags: Biography Comedy Crime Leonardo DiCaprio Jonah Hill Margot Robbie ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('bef6e72f-5af2-479b-b231-c1f2cd75f83c','A Wednesday','2008','MV5BMTAzODEzMjE1MTJeQTJeQWpwZ15BbWU3MDE3NjQ5Mzk@._V1_SY492_SX324_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('bef6e72f-5af2-479b-b231-c1f2cd75f83c','A man calls up the Mumbai police, and tells them he has placed five different bombs in the city -- all set to go off in some time. He wants four terrorists in exchange. Does he get them? Who is behind it all? What''s his motive? Is there more than meets the eye?                Written by Saurabh Roy tags: Crime Drama Mystery Anupam Kher Naseeruddin Shah Jimmy Shergill ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('bae67ea4-96e1-4857-ab10-08cc732a00ba','Sunrise: A Song of Two Humans','1927','MV5BMjIzNzg4MzcxNV5BMl5BanBnXkFtZTgwMTgzNTE0MTE@._V1_SY250_SX162_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('bae67ea4-96e1-4857-ab10-08cc732a00ba','In this fable-morality subtitled "A Song of Two Humans", the "evil" temptress is a city woman who bewitches farmer Anses and tries to convince him to murder his neglected wife, Indre.                Written by alfiehitchie tags: Drama Romance George O''Brien Janet Gaynor Margaret Livingston ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('72b7d157-fba5-43e9-94cc-f16a741c51c6','Gran Torino','2008','MV5BMTQyMTczMTAxMl5BMl5BanBnXkFtZTcwOTc1ODE0Mg@@._V1_SY250_CR0,0,176,250_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('72b7d157-fba5-43e9-94cc-f16a741c51c6','Walt Kowalski is a widower who holds onto his prejudices despite the changes in his Michigan neighborhood and the world around him. Kowalski is a grumpy, tough-minded, unhappy old man who can''t get along with either his kids or his neighbors. He is a Korean War veteran whose prize possession is a 1972 Gran Torino he keeps in mint condition. When his neighbor Thao, a young Hmong teenager under pressure from his gang member cousin, tries to steal his Gran Torino, Kowalski sets out to reform the youth. Drawn against his will into the life of Thao''s family, Kowalski is soon taking steps to protect them from the gangs that infest their neighborhood.                Written by alfiehitchie tags: Drama Clint Eastwood Bee Vang Christopher Carley ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('0e8f84f4-a766-40f9-95ce-f12f450c0397','Trainspotting','1996','MV5BMzA5Zjc3ZTMtMmU5YS00YTMwLWI4MWUtYTU0YTVmNjVmODZhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('0e8f84f4-a766-40f9-95ce-f12f450c0397','A wild, freeform, Rabelaisian trip through the darkest recesses of Edinburgh low-life, focusing on Mark Renton and his attempt to give up his heroin habit, and how the latter affects his relationship with family and friends: Sean Connery wannabe Sick Boy, dimbulb Spud, psycho Begbie, 14-year-old girlfriend Diane, and clean-cut athlete Tommy, who''s never touched drugs but can''t help being curious about them...                Written by Michael Brooke <michael@everyman.demon.co.uk> tags: Drama Ewan McGregor Ewen Bremner Jonny Lee Miller ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('f2e20850-8604-4e2d-8699-ece6cd6325b1','La passion de Jeanne d''Arc','1928','MV5BMjQxMjk5ODUxMl5BMl5BanBnXkFtZTgwNDI2MjUwMzE@._V1_SY500_CR0,0,346,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('f2e20850-8604-4e2d-8699-ece6cd6325b1','The sufferings of a martyr, Jeanne D''Arc (1412-1431). Jeanne appears in court where Cauchon questions her and d''Estivet spits on her. She predicts her rescue, is taken to her cell, and judges forge evidence against her. In her cell, priests interrogate her and judges deny her the Mass. Threatened first in a torture chamber and then offered communion if she will recant, she refuses. At a cemetery, in front of a crowd, a priest and supporters urge her to recant; she does, and Cauchon announces her sentence. In her cell, she explains her change of mind and receives communion. In the courtyard at Rouen castle, she burns at the stake; the soldiers turn on the protesting crowd.                Written by <jhailey@hotmail.com> tags: Biography Drama History Maria Falconetti Eugene Silvain André Berley ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('079f0caf-fd49-44e5-8f22-8a2b921299c1','Rang De Basanti','2006','MV5BMjEwNzA5MjAwN15BMl5BanBnXkFtZTcwMzY5MzIzMQ@@._V1_SY250_SX175_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('079f0caf-fd49-44e5-8f22-8a2b921299c1','A young idealistic English filmmaker, Sue, arrives in India to make a film on Indian revolutionaries Bhagat Singh, Chandrashekhar Azad and their contemporaries and their fight for freedom from the British Raj. Owing to a lack of funds, she recruits students from Delhi University to act in her docu-drama. She finds DJ, who graduated five years ago but still wants to be a part of the University because he doesn''t think there''s too much out there in the real world to look forward to. Karan, the son of Industrialist Rajnath Singhania, who shares an uncomfortable relationship with his father, but continues to live off him, albeit very grudgingly. Aslam, is a middle class Muslim boy, who lives in the by-lanes near Jama Masjid, poet, philosopher and guide to his friends. Sukhi, the group''s baby, innocent, vulnerable and with a weakness for only one thing - girls. Laxman Pandey, the fundamentalist in the group, the only one who still believes that politics can make the world a better place ...                Written by fish_wow6 tags: Comedy Drama History Aamir Khan Soha Ali Khan Siddharth ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('63a472e5-1d93-4223-9a5a-b633b18e0c1e','Dial M for Murder','1954','MV5BMTkyNzc4ODk4N15BMl5BanBnXkFtZTcwMDE5ODEwNA@@._V1_SY250_SX166_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('63a472e5-1d93-4223-9a5a-b633b18e0c1e','In London, wealthy Margot Mary Wendice had a brief love affair with the American writer Mark Halliday while her husband and professional tennis player Tony Wendice was on a tennis tour. Tony quits playing to dedicate to his wife and finds a regular job. She decides to give him a second chance for their marriage. When Mark arrives from America to visit the couple, Margot tells him that she had destroyed all his letters but one that was stolen. Subsequently she was blackmailed, but she had never retrieved the stolen letter. Tony arrives home, claims that he needs to work and asks Margot to go with Mark to the theater. Meanwhile Tony calls Captain Lesgate (aka Charles Alexander Swann who studied with him at college) and blackmails him to murder his wife, so that he can inherit her fortune. But there is no perfect crime, and things do not work as planned.                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Crime Film-Noir Thriller Ray Milland Grace Kelly Robert Cummings ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2f4cbac2-e801-4fcf-8118-681089ae2c72','The Big Lebowski','1998','MV5BZTFjMjBiYzItNzU5YS00MjdiLWJkOTktNDQ3MTE3ZjY2YTY5XkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2f4cbac2-e801-4fcf-8118-681089ae2c72','When "The Dude" Lebowski is mistaken for a millionaire Lebowski, two thugs urinate on his rug to coerce him into paying a debt he knows nothing about. While attempting to gain recompense for the ruined rug from his wealthy counterpart, he accepts a one-time job with high pay-off. He enlists the help of his bowling buddy, Walter, a gun-toting Jewish-convert with anger issues. Deception leads to more trouble, and it soon seems that everyone from porn empire tycoons to nihilists want something from The Dude.                Written by J. Lake tags: Comedy Crime Mystery Jeff Bridges John Goodman Julianne Moore ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('c67b24e9-e30e-4274-8467-8af0be83b345','The Deer Hunter','1978','MV5BMTYzYmRmZTQtYjk2NS00MDdlLTkxMDAtMTE2YTM2ZmNlMTBkXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,339,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('c67b24e9-e30e-4274-8467-8af0be83b345','Michael, Steven and Nick are young factory workers from Pennsylvania who enlist into the Army to fight in Vietnam. Before they go, Steven marries the pregnant Angela, and their wedding party also serves as the men''s farewell party. After some time and many horrors, the three friends fall in the hands of the Vietcong and are brought to a prison camp in which they are forced to play Russian roulette against each other. Michael makes it possible for them to escape, but they soon get separated again.                Written by Leon Wolters <wolters@strw.LeidenUniv.nl> tags: Drama War Robert De Niro Christopher Walken John Cazale ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2df8b3af-69fc-4675-817d-ac1a65d47b59','Gone with the Wind','1939','MV5BYWQwOWVkMGItMDU2Yy00YjIzLWJkMjEtNmVkZjE3MjMwYzEzXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,326,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2df8b3af-69fc-4675-817d-ac1a65d47b59','Scarlett is a woman who can deal with a nation at war, Atlanta burning, the Union Army carrying off everything from her beloved Tara, the carpetbaggers who arrive after the war. Scarlett is beautiful. She has vitality. But Ashley, the man she has wanted for so long, is going to marry his placid cousin, Melanie. Mammy warns Scarlett to behave herself at the party at Twelve Oaks. There is a new man there that day, the day the Civil War begins. Rhett Butler. Scarlett does not know he is in the room when she pleads with Ashley to choose her instead of Melanie.                Written by Dale O''Connor <daleoc@interaccess.com> tags: Drama History Romance Clark Gable Vivien Leigh Thomas Mitchell ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('b7c8dee1-9497-4c3c-beb9-5f2cc1a12ef6','Fargo','1996','MV5BMTgxNzY3MzUxOV5BMl5BanBnXkFtZTcwMDA0NjMyNA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('b7c8dee1-9497-4c3c-beb9-5f2cc1a12ef6','Jerry works in his father-in-law''s car dealership and has gotten himself in financial problems. He tries various schemes to come up with money needed for a reason that is never really explained. It has to be assumed that his huge embezzlement of money from the dealership is about to be discovered by father-in-law. When all else falls through, plans he set in motion earlier for two men to kidnap his wife for ransom to be paid by her wealthy father (who doesn''t seem to have the time of day for son-in-law). From the moment of the kidnapping, things go wrong and what was supposed to be a non-violent affair turns bloody with more blood added by the minute. Jerry is upset at the bloodshed, which turns loose a pregnant sheriff from Brainerd, MN who is tenacious in attempting to solve the three murders in her jurisdiction.                Written by Anonymous tags: Crime Drama Thriller William H. Macy Frances McDormand Steve Buscemi ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('42b7dc85-3a38-4f8b-9f09-d3cce26c45af','Tôkyô monogatari','1953','MV5BYmUwN2VjZjAtZjg1Ny00OTk3LTg5MWYtMTA0OGM5Njk5NmQxXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,352,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('42b7dc85-3a38-4f8b-9f09-d3cce26c45af','An elderly couple journey to Tokyo to visit their children and are confronted by indifference, ingratitude and selfishness. When the parents are packed off to a resort by their impatient children, the film deepens into an unbearably moving meditation on mortality.                Written by Paul Watabe tags: Drama Chishû Ryû Chieko Higashiyama Sô Yamamura ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('f4414515-3db5-43f4-b1e0-46b55bb45f02','Finding Nemo','2003','MV5BZTAzNWZlNmUtZDEzYi00ZjA5LWIwYjEtZGM1NWE1MjE4YWRhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,335,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('f4414515-3db5-43f4-b1e0-46b55bb45f02','A clown fish named Marlin lives in the Great Barrier Reef loses his son, Nemo. After he ventures into the open sea, despite his father''s constant warnings about many of the ocean''s dangers. Nemo is abducted by a boat and netted up and sent to a dentist''s office in Sydney. So, while Marlin ventures off to try to retrieve Nemo, Marlin meets a fish named Dory, a blue tang suffering from short-term memory loss. The companions travel a great distance, encountering various dangerous sea creatures such as sharks, anglerfish and jellyfish, in order to rescue Nemo from the dentist''s office, which is situated by Sydney Harbor. While the two are doing this, Nemo and the other sea animals in the dentist''s fish tank plot a way to return to Sydney Harbor to live their lives free again.                Written by Anonymous tags: Animation Adventure Comedy Albert Brooks Ellen DeGeneres Alexander Gould ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('9848ffbb-761f-4c58-a41d-c84534ddf2ed','The Sixth Sense','1999','MV5BMWM4NTFhYjctNzUyNi00NGMwLTk3NTYtMDIyNTZmMzRlYmQyXkEyXkFqcGdeQXVyMTAwMzUyOTc@._V1_SX250_CR0,0,250,369_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('9848ffbb-761f-4c58-a41d-c84534ddf2ed','Malcom Crowe (Bruce Willis)is a child psychologist who receives an award on the same night that he is visited by a very unhappy ex-patient. After this encounter, Crowe takes on the task of curing a young boy with the same ills as the ex-patient (Donnie Wahlberg) . This boy "sees dead people". Crowe spends a lot of time with the boy much to the dismay of his wife (Olivia Williams). Cole''s mom (Toni Collette) is at her wit''s end with what to do about her son''s increasing problems. Crowe is the boy''s only hope.                Written by Jeff Mellinger <jmell@uclink4.berkeley.edu> tags: Drama Mystery Thriller Bruce Willis Haley Joel Osment Toni Collette ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('4419fea3-6608-4835-ba13-7c7bb70a2ee4','The Thing','1982','MV5BNGViZWZmM2EtNGYzZi00ZDAyLTk3ODMtNzIyZTBjN2Y1NmM1XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('4419fea3-6608-4835-ba13-7c7bb70a2ee4','A US research station, Antarctica, early-winter 1982. The base is suddenly buzzed by a helicopter from the nearby Norwegian research station. They are trying to kill a dog that has escaped from their base. After the destruction of the Norwegian chopper the members of the US team fly to the Norwegian base, only to discover them all dead or missing. They do find the remains of a strange creature the Norwegians burned. The Americans take it to their base and deduce that it an alien life form. After a while it is apparent that the alien can take over and assimilate into other life forms, including humans, and can spread like a virus. This means that anyone at the base could be inhabited by The Thing, and tensions escalate.                Written by grantss tags: Horror Mystery Sci-Fi Kurt Russell Wilford Brimley Keith David ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('4a70ab1c-5b4f-4ce2-b512-bb5a61b1fae0','Cool Hand Luke','1967','MV5BOWFlNzZhYmYtYTI5YS00MDQyLWIyNTUtNTRjMWUwNTEzNjA0XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SY500_CR0,0,332,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('4a70ab1c-5b4f-4ce2-b512-bb5a61b1fae0','Luke Jackson is a cool, gutsy prisoner in a Southern chain gang, who, while refusing to buckle under to authority, keeps escaping and being recaptured. The prisoners admire Luke because, as Dragline explains it, "You''re an original, that''s what you are!" Nevertheless, the camp staff actively works to crush Luke until he finally breaks.                Written by alfiehitchie tags: Crime Drama Paul Newman George Kennedy Strother Martin ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('e1e58274-7aad-447c-8181-5d1360848cab','No Country for Old Men','2007','MV5BMjA5Njk3MjM4OV5BMl5BanBnXkFtZTcwMTc5MTE1MQ@@._V1_SY408_CR0,0,277,408_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('e1e58274-7aad-447c-8181-5d1360848cab','In rural Texas, welder and hunter Llewelyn Moss (Josh Brolin) discovers the remains of several drug runners who have all killed each other in an exchange gone violently wrong. Rather than report the discovery to the police, Moss decides to simply take the two million dollars present for himself. This puts the psychopathic killer, Anton Chigurh (Javier Bardem), on his trail as he dispassionately murders nearly every rival, bystander and even employer in his pursuit of his quarry and the money. As Moss desperately attempts to keep one step ahead, the blood from this hunt begins to flow behind him with relentlessly growing intensity as Chigurh closes in. Meanwhile, the laconic Sherrif Ed Tom Bell (Tommy Lee Jones) blithely oversees the investigation even as he struggles to face the sheer enormity of the crimes he is attempting to thwart.                Written by Kenneth Chisholm (kchishol@rogers.com) tags: Crime Drama Thriller Tommy Lee Jones Javier Bardem Josh Brolin ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('ccc9faa2-f763-406b-bd3a-6f35f0fb6e73','Rebecca','1940','MV5BYTcxYWExOTMtMWFmYy00ZjgzLWI0YjktNWEzYzJkZTg0NDdmL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY500_CR0,0,348,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('ccc9faa2-f763-406b-bd3a-6f35f0fb6e73','A shy ladies'' companion, staying in Monte Carlo with her stuffy employer, meets the wealthy Maxim de Winter. She and Max fall in love, marry and return to Manderley, his large country estate in Cornwall. Max is still troubled by the death of his first wife, Rebecca, in a boating accident the year before. The second Mrs. de Winter clashes with the housekeeper, Mrs. Danvers, and discovers that Rebecca still has a strange hold on everyone at Manderley.                Written by Col Needham <col@imdb.com> tags: Drama Mystery Romance Laurence Olivier Joan Fontaine George Sanders ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('db40484c-efed-4680-ac5e-2da242ddc18c','How to Train Your Dragon','2010','MV5BMjA5NDQyMjc2NF5BMl5BanBnXkFtZTcwMjg5ODcyMw@@._V1_SY500_CR0,0,336,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('db40484c-efed-4680-ac5e-2da242ddc18c','Long ago up North on the Island of Berk, the young Viking, Hiccup, wants to join his town''s fight against the dragons that continually raid their town. However, his macho father and village leader, Stoik the Vast, will not allow his small, clumsy, but inventive son to do so. Regardless, Hiccup ventures out into battle and downs a mysterious Night Fury dragon with his invention, but can''t bring himself to kill it. Instead, Hiccup and the dragon, whom he dubs Toothless, begin a friendship that would open up both their worlds as the observant boy learns that his people have misjudged the species. But even as the two each take flight in their own way, they find that they must fight the destructive ignorance plaguing their world.                Written by Kenneth Chisholm (kchishol@rogers.com) tags: Animation Action Adventure Jay Baruchel Gerard Butler Christopher Mintz-Plasse ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('c41a91cc-dda1-40cd-9648-4c1a7832cd66','Kill Bill: Vol. 1','2003','MV5BMTU1NDg1Mzg4M15BMl5BanBnXkFtZTYwMDExOTc3._V1_SY351_SX225_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('c41a91cc-dda1-40cd-9648-4c1a7832cd66','The lead character, called ''The Bride,'' was a member of the Deadly Viper Assassination Squad, led by her lover ''Bill.'' Upon realizing she was pregnant with Bill''s child, ''The Bride'' decided to escape her life as a killer. She fled to Texas, met a young man, who, on the day of their wedding rehearsal was gunned down by an angry and jealous Bill (with the assistance of the Deadly Viper Assassination Squad). Four years later, ''The Bride'' wakes from a coma, and discovers her baby is gone. She, then, decides to seek revenge upon the five people who destroyed her life and killed her baby. The saga of Kill Bill Volume I begins.                Written by JD tags: Action Crime Thriller Uma Thurman David Carradine Daryl Hannah ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('df94cb6d-c89b-4c89-a065-1ab2b29c4396','Into the Wild','2007','MV5BMTAwNDEyODU1MjheQTJeQWpwZ15BbWU2MDc3NDQwNw@@._V1_SY332_SX225_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('df94cb6d-c89b-4c89-a065-1ab2b29c4396','Based on a true story. After graduating from Emory University, Christopher McCandless abandoned his possessions, gave his entire savings account to charity, and hitchhiked to Alaska to live in the wilderness. Along the way, Christopher encounters a series of characters who shape his life.                Written by Lisa Kelley tags: Adventure Biography Drama Emile Hirsch Vince Vaughn Catherine Keener ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('b807a992-8de3-4cc5-a926-4c1f2e8ff9a9','Mary and Max','2009','MV5BMTQ1NDIyNTA1Nl5BMl5BanBnXkFtZTcwMjc2Njk3OA@@._V1_SY250_CR0,0,175,250_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('b807a992-8de3-4cc5-a926-4c1f2e8ff9a9','In the mid-1970''s, a homely, friendless Australian girl of 8 picks a name out of a Manhattan phone book and writes to him; she includes a chocolate bar. She''s Mary Dinkle, the only child of an alcoholic mother and a distracted father. He''s Max Horowitz, an overweight man with Asperger''s, living alone in New York. He writes back, with chocolate. Thus begins a 20-year correspondence, interrupted by a stay in an asylum and a few misunderstandings. Mary falls in love with a neighbor, saves money to have a birthmark removed and deals with loss. Max has a friendship with a neighbor, tries to control his weight, and finally gets the dream job. Will the two ever meet face to face?                Written by <jhailey@hotmail.com> tags: Animation Comedy Drama Toni Collette Philip Seymour Hoffman Eric Bana ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('0adafcf6-f4fa-49e9-af68-81b43c9c2213','There Will Be Blood','2007','MV5BMjAxODQ4MDU5NV5BMl5BanBnXkFtZTcwMDU4MjU1MQ@@._V1_SX255_CR0,0,255,377_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('0adafcf6-f4fa-49e9-af68-81b43c9c2213','The intersecting life stories of Daniel Plainview and Eli Sunday in early twentieth century California presents miner-turned-oilman Daniel Plainview, a driven man who will do whatever it takes to achieve his goals. He works hard but also takes advantage of those around him at their expense if need be. His business partner/son (H.W.) is, in reality, an "acquired" child whose true biological single-parent father (working on one of Daniel''s rigs) died in a workplace accident. Daniel is deeply protective of H.W. if only for what H.W. brings to the partnership. Eli Sunday is one in a pair of twins whose family farm Daniel purchases for the major oil deposit located on it. Eli, a local preacher and a self-proclaimed faith healer, wants the money from the sale of the property to finance his own church. The lives of the two competitive men often clash as Daniel pumps oil off the property and tries to acquire all the surrounding land at bargain prices to be able to build a pipeline to the ...                Written by Huggo / edited by statmanjeff tags: Drama History Daniel Day-Lewis Paul Dano Ciarán Hinds ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('260aeddd-7c44-4507-a502-84864e7a4ff5','Gone Girl','2014','MV5BMTk0MDQ3MzAzOV5BMl5BanBnXkFtZTgwNzU1NzE3MjE@._V1_SY500_CR0,0,339,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('260aeddd-7c44-4507-a502-84864e7a4ff5','On the occasion of his fifth wedding anniversary, Nick Dunne reports that his wife, Amy, has gone missing. Under pressure from the police and a growing media frenzy, Nick''s portrait of a blissful union begins to crumble. Soon his lies, deceits and strange behavior have everyone asking the same dark question: Did Nick Dunne kill his wife?                Written by Twentieth Century Fox tags: Crime Drama Mystery Ben Affleck Rosamund Pike Neil Patrick Harris ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('544d9e11-0c03-4166-aa59-1cec2003af3f','Munna Bhai M.B.B.S.','2003','MV5BY2JlMGI1YWQtYjBhMi00NTJiLTgxMzUtMGU4NThlZTAyYmMxXkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('544d9e11-0c03-4166-aa59-1cec2003af3f','In India gangsters are called Bhai (brothers). One such Bhai is Munna, who is feared by everyone in Mumbai, a big city in India. He cons his village-based parents into believing that he is a doctor. He gets a shock when he finds out that they are coming to verify for themselves. So he transforms a rooming shanty house into a makeshift hospital, populated by patients, people he beats up. His parents arrive, are pleased, and happy that their son is well settled. They would like him to get married to a doctor''s daughter. The marriage is arranged, but before the engagement, the bride''s father finds out the truth about Munna, and cancels the marriage. Munna''s humiliated parents see the truth for themselves, and hurt, they return home, leaving Munna with a strong desire to hurt the doctor - and force him to let his daughter get married to him - even if it means getting admitted in the medical college - with forged documents!!!                Written by Sumitra (corrected by Carrie) tags: Comedy Drama Romance Sunil Dutt Sanjay Dutt Arshad Warsi ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('c52a0293-4cec-4515-bd06-8cc8ce102319','Idi i smotri','1985','MV5BY2E3NzNlZGYtMGNmMS00ZGUzLThiYWItYjk0MDQ4MmFlZmUzXkEyXkFqcGdeQXVyNTc2MDU0NDE@._V1_SY480_SX320_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('c52a0293-4cec-4515-bd06-8cc8ce102319','During WWII, a Belarusian boy is thrust into the atrocities of war, fighting with a hopelessly unequipped Soviet resistance movement against ruthless German forces. Witnessing scenes of abject terror and surviving horrifying situations, he loses his innocence and then his mind.                Written by TheDistantPanic tags: Drama War Aleksey Kravchenko Olga Mironova Liubomiras Laucevicius ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('f33b1418-43b4-4dd3-a2f9-136de36d994d','Hera Pheri','2000','MV5BZTYwNDdmNmEtOGZhYS00MjRiLWIzOWUtNDQxNzg4ZTVjMGE1XkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_SY500_SX350_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('f33b1418-43b4-4dd3-a2f9-136de36d994d','Baburao is a landlord in India, who is very near-sighted, and always in financial trouble. He has a tenant named Raja, who has not paid his rent for several months. He also has anther tenant named Shyam, who has come to the city to look for a job in his late father''s place, but is unable to find employment. The three men quarrel amongst themselves frequently. Then one day, the three men get a phone call from a kidnapper named Kabira, and decide to make use of this phone call to overcome their financial problems - pretend to be the real kidnappers, increase the ransom amount, keep the incremental amount for themselves, and then give the original ransom demanded to Kabira. Will they get away with this idea?                Written by Tanya tags: Comedy Crime Thriller Akshay Kumar Paresh Rawal Sunil Shetty ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('4a3901d3-980f-4f1c-85d8-3b01add07301','It Happened One Night','1934','MV5BZmViYmM5OTYtNGQ4Ny00YjQyLThiNjEtYTE4MGZhZTNmZjcyL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,324,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('4a3901d3-980f-4f1c-85d8-3b01add07301','Ellie Andrews has just tied the knot with society aviator King Westley when she is whisked away to her father''s yacht and out of King''s clutches. Ellie jumps ship and eventually winds up on a bus headed back to her husband. Reluctantly she must accept the help of out-of- work reporter Peter Warne. Actually, Warne doesn''t give her any choice: either she sticks with him until he gets her back to her husband, or he''ll blow the whistle on Ellie to her father. Either way, Peter gets what (he thinks!) he wants .... a really juicy newspaper story.                Written by A.L.Beneteau <albl@inforamp.net> tags: Comedy Romance Clark Gable Claudette Colbert Walter Connolly ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('fb4b634e-8fe7-466f-9dac-1dfc853331ae','Sholay','1975','MV5BOWQ0YTUzYzItYjI0MC00OTZmLWE1MWQtY2EzMzU2MTlmMmJjXkEyXkFqcGdeQXVyMDkwNTkwNg@@._V1_SY383_CR0,0,292,383_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('fb4b634e-8fe7-466f-9dac-1dfc853331ae','Sholay means embers in Hindi. In this particular movie, a Police Officer, who''s family was killed by a bandit named Gabbar Singh, decides to fight fire with fire and recruits two convicts, Jai and Veeru to capture Gabbar. He approaches them in jail, puts the proposal in front of them, and they agree to bring in Gabbar Singh alive - for a hefty price. After their discharge from jail, they travel by train to the village where the Police Officer lives - now with only his widowed daughter-in-law. The three band together to fight one of the most elusive and dreaded bandits of all time. Will the two ex-cons be able to bring Gabbar alive to the Police Officer?                Written by Calista tags: Action Adventure Comedy Dharmendra Sanjeev Kumar Hema Malini ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('9884e4a1-84cc-45e0-9132-37b13a0468a6','Life of Brian','1979','MV5BMzAwNjU1OTktYjY3Mi00NDY5LWFlZWUtZjhjNGE0OTkwZDkwXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,318,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('9884e4a1-84cc-45e0-9132-37b13a0468a6','The story of Brian of Nazareth, born on the same day as Jesus of Nazareth, who takes a different path in life that leads to the same conclusion. Brian joins a political resistance movement aiming to get the Romans out of Judea. Brian scores a victory of sorts when he manages to paint political slogans on an entire wall in the city of Jerusalem. The movement is not very effective but somehow Brian becomes a prophet and gathers his own following. His fate is sealed however and he lives a very short life.                Written by garykmcd tags: Comedy Graham Chapman John Cleese Michael Palin ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('e8c68df0-37a4-407d-bf1e-32b4cb5a5217','Andaz Apna Apna','1994','MV5BNWE2MjhjMmMtNzQyMS00MmI0LTlkOTUtZDVlZjFlNmZkNDgyL2ltYWdlXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_SY462_CR0,0,331,462_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('e8c68df0-37a4-407d-bf1e-32b4cb5a5217','Amar and Prem are two rivals who belonging to middle-class families with no scope of future advancement. Both individually, without the knowledge of the other decide to take matters into their hands and find ways to get rich quickly. Chaos and acrimony result when both find each other at loggerheads when both arrive to win the hand of the daughter of multi-millionaire Ram Gopal Bajaj amidst considerable upheavals when Shyam, the twin brother of Ram, decides to do away with Ram and assume his identity and thus take over the estate of the Bajaj family.                Written by Sonia tags: Comedy Family Romance Aamir Khan Salman Khan Raveena Tandon ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('f5175fd7-7c5e-45fe-a712-2e127b0dfefb','Shutter Island','2010','MV5BMTMxMTIyNzMxMV5BMl5BanBnXkFtZTcwOTc4OTI3Mg@@._V1_SY377_CR0,0,250,377_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('f5175fd7-7c5e-45fe-a712-2e127b0dfefb','It''s 1954, and up-and-coming U.S. marshal Teddy Daniels is assigned to investigate the disappearance of a patient from Boston''s Shutter Island Ashecliffe Hospital. He''s been pushing for an assignment on the island for personal reasons, but before long he wonders whether he hasn''t been brought there as part of a twisted plot by hospital doctors whose radical treatments range from unethical to illegal to downright sinister. Teddy''s shrewd investigating skills soon provide a promising lead, but the hospital refuses him access to records he suspects would break the case wide open. As a hurricane cuts off communication with the mainland, more dangerous criminals "escape" in the confusion, and the puzzling, improbable clues multiply, Teddy begins to doubt everything - his memory, his partner, even his own sanity.                Written by alfiehitchie tags: Mystery Thriller Leonardo DiCaprio Emily Mortimer Mark Ruffalo ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('5081c4ec-6d53-4ed3-ab29-dd0e00b85150','Platoon','1986','MV5BZmJlOTE1ZTgtODdmNC00NmZhLThlZDYtNTllMmIyOTIwN2EwL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY475_CR0,0,302,475_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('5081c4ec-6d53-4ed3-ab29-dd0e00b85150','Chris Taylor is a young, naive American who gives up college and volunteers for combat in Vietnam. Upon arrival, he quickly discovers that his presence is quite nonessential, and is considered insignificant to the other soldiers, as he has not fought for as long as the rest of them and felt the effects of combat. Chris has two non-commissioned officers, the ill-tempered and indestructible Staff Sergeant Robert Barnes and the more pleasant and cooperative Sergeant Elias Grodin. A line is drawn between the two NCOs and a number of men in the platoon when an illegal killing occurs during a village raid. As the war continues, Chris himself draws towards psychological meltdown. And as he struggles for survival, he soon realizes he is fighting two battles, the conflict with the enemy and the conflict between the men within his platoon.                Written by Jeremy Thomson tags: Drama War Charlie Sheen Tom Berenger Willem Dafoe ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('1963df23-5642-4bc9-a9ce-5fcfc07db56c','Hotel Rwanda','2004','MV5BMTI2MzQyNTc1M15BMl5BanBnXkFtZTYwMjExNjc3._V1_SY250_CR0,0,166,250_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('1963df23-5642-4bc9-a9ce-5fcfc07db56c','During the 1990s, some of the worst atrocities in the history of mankind took place in the country of Rwanda--and in an era of high-speed communication and round the clock news, the events went almost unnoticed by the rest of the world. In only three months, one million people were brutally murdered. In the face of these unspeakable actions, inspired by his love for his family, an ordinary man summons extraordinary courage to save the lives of over a thousand helpless refugees, by granting them shelter in the hotel he manages.                Written by Sujit R. Varma tags: Biography Drama History Don Cheadle Sophie Okonedo Joaquin Phoenix ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('23b3540b-243b-4c1c-88ad-fc108beb18ef','Rush','2013','MV5BOWEwODJmZDItYTNmZC00OGM4LThlNDktOTQzZjIzMGQxODA4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('23b3540b-243b-4c1c-88ad-fc108beb18ef','Set against the sexy, glamorous golden age of Formula 1 racing in the 1970s, the film is based on the true story of a great sporting rivalry between handsome English playboy James Hunt (Hemsworth), and his methodical, brilliant opponent, Austrian driver Niki Lauda (Bruhl). The story follows their distinctly different personal styles on and off the track, their loves and the astonishing 1976 season in which both drivers were willing to risk everything to become world champion in a sport with no margin for error: if you make a mistake, you die.                Written by P. Morgan tags: Action Biography Drama Daniel Brühl Chris Hemsworth Olivia Wilde ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('842ff19d-e7d5-4819-ad0c-15f59cb10c2d','Network','1976','MV5BNzk5MjcxNTg2MF5BMl5BanBnXkFtZTgwMzY2MTUxMDE@._V1_SY250_CR0,0,186,250_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('842ff19d-e7d5-4819-ad0c-15f59cb10c2d','In the 1970s, terrorist violence is the stuff of networks'' nightly news programming and the corporate structure of the UBS Television Network is changing. Meanwhile, Howard Beale, the aging UBS news anchor, has lost his once strong ratings share and so the network fires him. Beale reacts in an unexpected way. We then see how this affects the fortunes of Beale, his coworkers (Max Schumacher and Diana Christensen), and the network.                Written by Bruce Janson <bruce@cs.su.oz.au> tags: Drama Faye Dunaway William Holden Peter Finch ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('d5105f68-709a-4856-82c2-a1f54b1db057','Le salaire de la peur','1953','MV5BMTQ5MzkyNDgyMF5BMl5BanBnXkFtZTgwODg2MTMzMjE@._V1_SY250_SX170_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('d5105f68-709a-4856-82c2-a1f54b1db057','In the South American jungle supplies of nitroglycerin are needed at a remote oil field. The oil company pays four men to deliver the supplies in two trucks. A tense rivalry develops between the two sets of drivers and on the rough remote roads the slightest jolt can result in death.                Written by Col Needham <col@imdb.com> tags: Adventure Drama Thriller Yves Montand Charles Vanel Peter van Eyck ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('868da2be-fb48-4898-a6fb-5a2c26c8950f','Relatos salvajes','2014','MV5BNzAzMjA1ODAxOV5BMl5BanBnXkFtZTgwODg4NTQzNDE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('868da2be-fb48-4898-a6fb-5a2c26c8950f','The film is divided into six segments. (1) "Pasternak": While being on a plane, a model and a music critic realise they have a common acquaintance called Pasternak. Soon they discover that every passenger and crew member on board know Pasternak. Is this coincidence? (2) "The Rats": A waitress recognizes her client - it''s the loan shark who caused a tragedy in her family. The cook suggests mixing rat poison with his food, but the waitress refuses. The stubborn cook, however, decides to proceed with her plan. (3) "The Strongest": Two drivers on a lone highway have an argument with tragic consequences. (4) "Little Bomb": A demolition engineer has his car towed by a truck for parking in a wrong place and he has an argument with the employee of the towing company. This event destroys his private and professional life, and he plots revenge against the corrupt towing company and the city hall. (5) "The Proposal": A reckless son of a wealthy family has an overnight hit-and-run accident, in ...                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Comedy Drama Thriller Darío Grandinetti María Marull Mónica Villa ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('81c9a391-ce40-4da5-90c0-26b1ebec223f','Stand by Me','1986','MV5BMjIxODliZDYtMGMyYy00ZDQzLTgzOTMtMWY5YWEyNTBhNzk5L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY355_CR0,0,234,355_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('81c9a391-ce40-4da5-90c0-26b1ebec223f','It''s the summer of 1959 in Castlerock, Oregon and four 12 year-old boys - Gordie, Chris, Teddy and Vern - are fast friends. After learning of the general location of the body of a local boy who has been missing for several days, they set off into woods to see it. Along the way, they learn about themselves, the meaning of friendship and the need to stand up for what is right.                Written by garykmcd tags: Adventure Drama Wil Wheaton River Phoenix Corey Feldman ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('5672ac92-9609-42d6-99c4-79022a54f314','In the Name of the Father','1993','MV5BMWQ0ZGU3NDUtYjRkNS00Yzg4LTgxMTItMjExYjg3ZjgyMGRkXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,336,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('5672ac92-9609-42d6-99c4-79022a54f314','A small time thief from Belfast, Gerry Conlon, is falsely implicated in the IRA bombing of a pub that kills several people while he is in London. Bullied by the British police, he and four of his friends are coerced into confessing their guilt. Gerry''s father and other relatives in London are also implicated in the crime. He spends 15 years in prison with his father trying to prove his innocence with the help of a British attorney, Gareth Peirce. Based on a true story.                Written by Liza Esser <essereli@student.msu.edu> tags: Biography Drama Daniel Day-Lewis Pete Postlethwaite Alison Crosbie ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2a02659a-1ad1-41c2-b279-dd53e459fa5e','Spotlight','2015','MV5BMjIyOTM5OTIzNV5BMl5BanBnXkFtZTgwMDkzODE2NjE@._V1_SY500_CR0,0,338,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2a02659a-1ad1-41c2-b279-dd53e459fa5e','When the Boston Globe''s tenacious "Spotlight" team of reporters delves into allegations of abuse in the Catholic Church, their year-long investigation uncovers a decades-long cover-up at the highest levels of Boston''s religious, legal, and government establishment, touching off a wave of revelations around the world.                Written by Open Road tags: Crime Drama History Mark Ruffalo Michael Keaton Rachel McAdams ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('ef260f78-b73e-4444-8a07-4654b6f69534','Les quatre cents coups','1959','MV5BYTQ4MjA4NmYtYjRhNi00MTEwLTg0NjgtNjk3ODJlZGU4NjRkL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,376,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('ef260f78-b73e-4444-8a07-4654b6f69534','Seemingly in constant trouble at school, 14 year-old Antoine Doinel returns at the end of every day to a drab and unhappy home life. His parents have little money and he sleeps on a couch that''s been pushed into the kitchen. He knows his mother is having an affair and his parents bicker constantly. He decides to skip school and begins a downward spiral of lies and later stealing. His parents are at their wits end and after he''s stopped by the police, they decide the best thing to do would be to let Antoine face the consequences. He''s sent to a juvenile detention facility where he doesn''t do much better. He does manage to escape however.........                Written by garykmcd tags: Crime Drama Jean-Pierre Léaud Albert Rémy Claire Maurier ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('607d9f3d-2f43-44b4-b0fb-45b0f88b54c4','Mad Max: Fury Road','2015','MV5BMTUyMTE0ODcxNF5BMl5BanBnXkFtZTgwODE4NDQzNTE@._V1_SY500_CR0,0,343,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('607d9f3d-2f43-44b4-b0fb-45b0f88b54c4','An apocalyptic story set in the furthest reaches of our planet, in a stark desert landscape where humanity is broken, and almost everyone is crazed fighting for the necessities of life. Within this world exist two rebels on the run who just might be able to restore order. There''s Max, a man of action and a man of few words, who seeks peace of mind following the loss of his wife and child in the aftermath of the chaos. And Furiosa, a woman of action and a woman who believes her path to survival may be achieved if she can make it across the desert back to her childhood homeland.                Written by Production tags: Action Adventure Sci-Fi Tom Hardy Charlize Theron Nicholas Hoult ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('f9d23a46-688c-4c9d-9074-a6d572d13aa3','Butch Cassidy and the Sundance Kid','1969','MV5BMTkyMTM2NDk5Nl5BMl5BanBnXkFtZTgwNzY1NzEyMDE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('f9d23a46-688c-4c9d-9074-a6d572d13aa3','Butch and Sundance are the two leaders of the Hole-in-the-Wall Gang. Butch is all ideas, Sundance is all action and skill. The west is becoming civilized and when Butch and Sundance rob a train once too often, a special posse begins trailing them no matter where they run. Over rock, through towns, across rivers, the group is always just behind them. When they finally escape through sheer luck, Butch has another idea, "Let''s go to Bolivia". Based on the exploits of the historical characters.                Written by John Vogel <jlvogel@comcast.net> tags: Biography Crime Drama Paul Newman Robert Redford Katharine Ross ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('ace4c4b7-e457-4668-bcc9-736d2f5efa87','12 Years a Slave','2013','MV5BMjExMTEzODkyN15BMl5BanBnXkFtZTcwNTU4NTc4OQ@@._V1_SY377_CR0,0,254,377_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('ace4c4b7-e457-4668-bcc9-736d2f5efa87','Based on an incredible true story of one man''s fight for survival and freedom. In the pre-Civil War United States, Solomon Northup, a free black man from upstate New York, is abducted and sold into slavery. Facing cruelty personified by a malevolent slave owner, as well as unexpected kindnesses, Solomon struggles not only to stay alive, but to retain his dignity. In the twelfth year of his unforgettable odyssey, Solomon''s chance meeting with a Canadian abolitionist will forever alter his life.                Written by Fox Searchlight tags: Biography Drama History Chiwetel Ejiofor Michael Kenneth Williams Michael Fassbender ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('8e46e9b8-78e6-4e08-b15c-eb08a2fb8a9f','The Grand Budapest Hotel','2014','MV5BMzM5NjUxOTEyMl5BMl5BanBnXkFtZTgwNjEyMDM0MDE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('8e46e9b8-78e6-4e08-b15c-eb08a2fb8a9f','GRAND BUDAPEST HOTEL recounts the adventures of Gustave H, a legendary concierge at a famous European hotel between the wars, and Zero Moustafa, the lobby boy who becomes his most trusted friend. The story involves the theft and recovery of a priceless Renaissance painting and the battle for an enormous family fortune -- all against the back-drop of a suddenly and dramatically changing Continent.                Written by Fox Searchlight Pictures tags: Adventure Comedy Drama Ralph Fiennes F. Murray Abraham Mathieu Amalric ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2224f112-952d-4c85-876b-6112d88810cd','Ben-Hur','1959','MV5BNjgxY2JiZDYtZmMwOC00ZmJjLWJmODUtMTNmNWNmYWI5ODkwL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY500_CR0,0,329,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2224f112-952d-4c85-876b-6112d88810cd','Judah Ben-Hur lives as a rich Jewish prince and merchant in Jerusalem at the beginning of the 1st century. Together with the new governor his old friend Messala arrives as commanding officer of the Roman legions. At first they are happy to meet after a long time but their different politic views separate them. During the welcome parade a roof tile falls down from Judah''s house and injures the governor. Although Messala knows they are not guilty, he sends Judah to the galleys and throws his mother and sister into prison. But Judah swears to come back and take revenge.                Written by Matthias Scheler <tron@lyssa.owl.de> tags: Adventure Drama History Charlton Heston Jack Hawkins Stephen Boyd ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('432f4a61-a434-47ef-8141-30f9d5f2587b','The Maltese Falcon','1941','MV5BMTc4MDEzOTMwMl5BMl5BanBnXkFtZTgwMTc2NjgyMjE@._V1_SY250_SX169_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('432f4a61-a434-47ef-8141-30f9d5f2587b','Spade and Archer is the name of a San Francisco detective agency. That''s for Sam Spade and Miles Archer. The two men are partners, but Sam doesn''t like Miles much. A knockout, who goes by the name of Miss Wonderly, walks into their office; and by that night everything''s changed. Miles is dead. And so is a man named Floyd Thursby. It seems Miss Wonderly is surrounded by dangerous men. There''s Joel Cairo, who uses gardenia-scented calling cards. There''s Kasper Gutman, with his enormous girth and feigned civility. Her only hope of protection comes from Sam, who is suspected by the police of one or the other murder. More murders are yet to come, and it will all be because of these dangerous men -- and their lust for a statuette of a bird: the Maltese Falcon.                Written by J. Spurlin tags: Crime Film-Noir Mystery Humphrey Bogart Mary Astor Gladys George ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('004fbb1b-5ecc-4c82-95d4-a48d54653869','Star Wars: Episode VII - The Force Awakens','2015','MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SY500_CR0,0,338,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('004fbb1b-5ecc-4c82-95d4-a48d54653869','30 years after the defeat of Darth Vader and the Empire, Rey, a scavenger from the planet Jakku, finds a BB-8 droid that knows the whereabouts of the long lost Luke Skywalker. Rey, as well as a rogue stormtrooper and two smugglers, are thrown into the middle of a battle between the Resistance and the daunting legions of the First Order.                Written by Noah White tags: Action Adventure Fantasy Daisy Ridley John Boyega Oscar Isaac ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('073d32ed-b623-495d-bfc4-8e822cfb23df','Le notti di Cabiria','1957','MV5BZjllMDcyY2YtZDc5Zi00NDRlLThkZmItMTJhOWZmYjM0NmQyXkEyXkFqcGdeQXVyNTc1NDM0NDU@._V1_SY500_CR0,0,348,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('073d32ed-b623-495d-bfc4-8e822cfb23df','Rome, 1957. A woman, Cabiria, is robbed and left to drown by her boyfriend, Giorgio. Rescued, she resumes her life and tries her best to find happiness in a cynical world. Even when she thinks her struggles are over and she has found happiness and contentment, things may not be what they seem.                Written by grantss tags: Drama Giulietta Masina François Périer Franca Marzi ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('16cfad9f-c3e2-4eb2-82da-1d89f7f88c2e','Persona','1966','MV5BMTc1OTgxNjYyNF5BMl5BanBnXkFtZTcwNjM2MjM2NQ@@._V1_SY500_CR0,0,317,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('16cfad9f-c3e2-4eb2-82da-1d89f7f88c2e','A young nurse, Alma, is put in charge of Elisabeth Vogler: an actress who is seemingly healthy in all respects, but will not talk. As they spend time together, Alma speaks to Elisabeth constantly, never receiving any answer. Alma eventually confesses her secrets to a seemingly sympathetic Elisabeth and finds that her own personality is being submerged into Elisabeth''s persona.                Written by Kathy Li tags: Drama Thriller Bibi Andersson Liv Ullmann Margaretha Krook ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('aecbf1a7-b1c5-4cce-b7ae-94cf830df5e7','Million Dollar Baby','2004','MV5BMTkxNzA1NDQxOV5BMl5BanBnXkFtZTcwNTkyMTIzMw@@._V1_SY500_CR0,0,339,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('aecbf1a7-b1c5-4cce-b7ae-94cf830df5e7','Wanting to learn from the best, aspiring boxer Maggie Fitzgerald wants Frankie Dunn to train her. At the outset he flatly refuses saying he has no interest in training a girl. Frankie leads a lonely existence, alienated from his only daughter and having few friends. Maggie''s rough around the edges but shows a lot of grit in the ring and he eventually relents. Maggie not only proves to be the boxer he always dreamed of having under his wing but a friend who fills the great void he''s had in his life. Maggie''s career skyrockets but an accident in the ring leads her to ask Frankie for one last favor.                Written by garykmcd tags: Drama Sport Hilary Swank Clint Eastwood Morgan Freeman ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('ed67d976-9072-45ce-9d80-807e55cd6bf7','Amores perros','2000','MV5BZjYwYzliYTktMzk3MS00MzE0LTllM2QtMDk3MTFmOGQ2MjE0XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY419_SX282_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('ed67d976-9072-45ce-9d80-807e55cd6bf7','Three interconnected stories about the different strata of life in Mexico City all resolve with a fatal car accident. Octavio is trying to raise enough money to run away with his sister-in-law, and decides to enter his dog Cofi into the world of dogfighting. After a dogfight goes bad, Octavio flees in his car, running a red light and causing the accident. Daniel and Valeria''s new-found bliss is prematurely ended when she loses her leg in the accident. El Chivo is a homeless man who cares for stray dogs and is there to witness the collision.                Written by Anonymous tags: Drama Thriller Emilio Echevarría Gael García Bernal Goya Toledo ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('5404189e-0243-4ee4-998c-1e9cc39b4f28','Jurassic Park','1993','MV5BMjM2MDgxMDg0Nl5BMl5BanBnXkFtZTgwNTM2OTM5NDE@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('5404189e-0243-4ee4-998c-1e9cc39b4f28','Huge advancements in scientific technology have enabled a mogul to create an island full of living dinosaurs. John Hammond has invited four individuals, along with his two grandchildren, to join him at Jurassic Park. But will everything go according to plan? A park employee attempts to steal dinosaur embryos, critical security systems are shut down and it now becomes a race for survival with dinosaurs roaming freely over the island.                Written by Film_Fan tags: Adventure Sci-Fi Thriller Sam Neill Laura Dern Jeff Goldblum ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('afdd0692-4bd1-4de5-843c-980dd2e82c98','The Princess Bride','1987','MV5BMGM4M2Q5N2MtNThkZS00NTc1LTk1NTItNWEyZjJjNDRmNDk5XkEyXkFqcGdeQXVyMjA0MDQ0Mjc@._V1_SY500_CR0,0,338,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('afdd0692-4bd1-4de5-843c-980dd2e82c98','An elderly man reads the book "The Princess Bride" to his sick and thus currently bedridden adolescent grandson, the reading of the book which has been passed down within the family for generations. The grandson is sure he won''t like the story, with a romance at its core, he preferring something with lots of action and "no kissing". But the grandson is powerless to stop his grandfather, whose feelings he doesn''t want to hurt. The story centers on Buttercup, a former farm girl who has been chosen as the princess bride to Prince Humperdinck of Florian. Buttercup does not love him, she who still laments the death of her one true love, Westley, five years ago. Westley was a hired hand on the farm, his stock answer of "as you wish" to any request she made of him which she came to understand was his way of saying that he loved her. But Westley went away to sea, only to be killed by the Dread Pirate Roberts. On a horse ride to clear her mind of her upcoming predicament of marriage, Buttercup...                Written by Huggo tags: Adventure Family Fantasy Cary Elwes Mandy Patinkin Robin Wright ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('4fe45c3f-137d-4c8d-bec3-26ef909dd686','Salinui chueok','2003','MV5BZWYyYTk5ODQtNjFjZi00ZDFlLWEwNGEtNGQwZTdmYWRhNmU3L2ltYWdlXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('4fe45c3f-137d-4c8d-bec3-26ef909dd686','In 1986, in the province of Gyunggi, in South Korea, a second young and beautiful woman is found dead, raped and tied and gagged with her underwear. Detective Park Doo-Man and Detective Cho Yong-koo, two brutal and stupid local detectives without any technique, investigate the murder using brutality and torturing the suspects, without any practical result. The Detective Seo Tae-Yoon from Seoul comes to the country to help the investigations and is convinced that a serial-killer is killing the women. When a third woman is found dead in the same "modus-operandi", the detectives find leads of the assassin.                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Crime Drama Mystery Kang-ho Song Sang-kyung Kim Roe-ha Kim ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('22fede4d-4e1e-44a8-a23a-ee7512fdb866','Hachi: A Dog''s Tale','2009','MV5BMTAxMDA2MjM4NDReQTJeQWpwZ15BbWU3MDE0NTgxMTM@._V1_SY215_SX150_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('22fede4d-4e1e-44a8-a23a-ee7512fdb866','In Bedridge, Professor Parker Wilson finds an abandoned dog at the train station and takes it home with the intention of returning the animal to its owner. He finds that the dog is an Akita and names it Hachiko. However, nobody claims the dog so his family decides to keep Hachi.                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Drama Family Richard Gere Joan Allen Cary-Hiroyuki Tagawa ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('f199a3da-388b-4a83-8199-9aa3b6788b8b','Stalker','1979','MV5BNDY2NjU0NDAxOF5BMl5BanBnXkFtZTgwNjQ4MTI2MTE@._V1_SY250_CR0,0,175,250_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('f199a3da-388b-4a83-8199-9aa3b6788b8b','Near a gray and unnamed city is the Zone, an alien place guarded by barbed wire and soldiers. Over his wife''s objections, a man rises in the early morning and leaves her with their disabled daughter to meet two men. He''s a Stalker, one of a handful who have the mental gifts (and who risk imprisonment) to lead people into the Zone to the Room, a place where one''s secret hopes come true. His clients are a burned out popular writer, cynical, and questioning his talent; and a quiet scientist more concerned about his knapsack than the journey. In the deserted Zone, the approach to the Room must be indirect. As they draw near, the rules seem to change and the stalker faces a crisis.                Written by <jhailey@hotmail.com> tags: Drama Sci-Fi Alisa Freyndlikh Aleksandr Kaydanovskiy Anatoliy Solonitsyn ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('7a376526-99a2-477a-9a64-d73aae7d2e1f','Kaze no tani no Naushika','1984','MV5BODJiNmUzYmQtZTNhNS00NjY0LThmYjMtOTliOTM1NTdkYzY1XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,353,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('7a376526-99a2-477a-9a64-d73aae7d2e1f','An animated fantasy-adventure. Set 1,000 years from now, the earth is ravaged by pollution and war. In the Valley of the Wind lives Nausicaa, princess of her people. Their land borders on a toxic jungle, filled with dangerous over-sized insects. Meanwhile two nearby nations are bitterly engaged in a war and the Valley of the Wind is stuck in the middle...                Written by grantss tags: Animation Adventure Fantasy Sumi Shimamoto Mahito Tsujimura Hisako Kyôda ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('baa24390-8bac-4f4b-b3fd-254e3ea3bb03','The Truman Show','1998','MV5BMDIzODcyY2EtMmY2MC00ZWVlLTgwMzAtMjQwOWUyNmJjNTYyXkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_SY500_SX337_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('baa24390-8bac-4f4b-b3fd-254e3ea3bb03','In this movie, Truman is a man whose life is a fake one... The place he lives is in fact a big studio with hidden cameras everywhere, and all his friends and people around him, are actors who play their roles in the most popular TV-series in the world: The Truman Show. Truman thinks that he is an ordinary man with an ordinary life and has no idea about how he is exploited. Until one day... he finds out everything. Will he react?                Written by Chris Makrozahopoulos <makzax@hotmail.com> tags: Comedy Drama Sci-Fi Jim Carrey Ed Harris Laura Linney ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('0f740440-0f58-4cdf-9bf8-5ecb4196b1ee','Before Sunrise','1995','MV5BZDdiZTAwYzAtMDI3Ni00OTRjLTkzN2UtMGE3MDMyZmU4NTU4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,336,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('0f740440-0f58-4cdf-9bf8-5ecb4196b1ee','American tourist Jesse and French student Celine meet by chance on the train from Budapest to Vienna. Sensing that they are developing a connection, Jesse asks Celine to spend the day with him in Vienna, and she agrees. So they pass the time before his scheduled flight the next morning together. How do two perfect strangers connect so intimately over the course of a single day? What is that special thing that bonds two people so strongly? As their bond turns to love, what will happen to them the next morning when Jesse flies away?                Written by randywong70@comcast.net tags: Drama Romance Ethan Hawke Julie Delpy Andrea Eckert ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('482b6a36-d253-4181-a887-be3dcc920479','The Grapes of Wrath','1940','MV5BNzJiOGI2MjctYjUyMS00ZjkzLWE2ZmUtOTg4NTZkOTNhZDc1L2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY431_SX290_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('482b6a36-d253-4181-a887-be3dcc920479','Tom Joad returns to his home after a jail sentence to find his family kicked out of their farm due to foreclosure. He catches up with them on his Uncles farm, and joins them the next day as they head for California and a new life... Hopefully.                Written by Colin Tinto <cst@imdb.com> tags: Drama History Henry Fonda Jane Darwell John Carradine ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('dda7c6d4-ac34-42ec-9e02-ce6b5e1fa242','Touch of Evil','1958','MV5BMDFkMDEwM2MtOWY4MS00YWNhLTg5ZWItYzAxZTJlN2Y1NjBmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SX320_CR0,0,320,481_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('dda7c6d4-ac34-42ec-9e02-ce6b5e1fa242','Mexican Narcotics officer Ramon Miguel ''Mike'' Vargas has to interrupt his honeymoon on the Mexican-US border when an American building contractor is killed after someone places a bomb in his car. He''s killed on the US side of the border but it''s clear that the bomb was planted on the Mexican side. As a result, Vargas delays his return to Mexico City where he has been mounting a case against the Grandi family crime and narcotics syndicate. Police Captain Hank Quinlan is in charge on the US side and he soon has a suspect, a Mexican named Manolo Sanchez. Vargas is soon onto Quinlan and his Sergeant, Pete Menzies, when he catches them planting evidence to convict Sanchez. With his new American wife, Susie, safely tucked away in a hotel on the US side of the border - or so he thinks - he starts to review Quinlan''s earlier cases. While concentrating on the corrupt policeman however, the Grandis have their own plans for Vargas and they start with his wife Susie.                Written by garykmcd tags: Crime Drama Film-Noir Charlton Heston Orson Welles Janet Leigh ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('794f2975-4252-447e-83cb-e08fece928c5','Annie Hall','1977','MV5BMTU1NDM2MjkwM15BMl5BanBnXkFtZTcwODU3OTYwNA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('794f2975-4252-447e-83cb-e08fece928c5','Alvy Singer, a forty year old twice divorced, neurotic, intellectual Jewish New York stand-up comic, reflects on the demise of his latest relationship, to Annie Hall, an insecure, flighty, Midwestern WASP aspiring nightclub singer. Unlike his previous relationships, Alvy believed he may have worked out all the issues in his life through fifteen years of therapy to make this relationship with Annie last, among those issues being not wanting to date any woman that would want to date him, and thus subconsciously pushing those women away. Alvy not only reviews the many ups and many downs of their relationship, but also reviews the many facets of his makeup that led to him starting to date Annie. Those facets include growing up next to Coney Island in Brooklyn, being attracted to the opposite sex for as long as he can remember, and enduring years of Jewish guilt with his constantly arguing parents.                Written by Huggo tags: Comedy Romance Woody Allen Diane Keaton Tony Roberts ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('ccbc804d-ead0-4ee9-9358-9a8133487e37','Rocky','1976','MV5BMTY5MDMzODUyOF5BMl5BanBnXkFtZTcwMTQ3NTMyNA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('ccbc804d-ead0-4ee9-9358-9a8133487e37','Rocky Balboa is a struggling boxer trying to make the big time, working as a debt collector for a pittance. When heavyweight champion Apollo Creed visits Philadelphia, his managers want to set up an exhibition match between Creed and a struggling boxer, touting the fight as a chance for a "nobody" to become a "somebody". The match is supposed to be easily won by Creed, but someone forgot to tell Rocky, who sees this as his only shot at the big time.                Written by Murray Chapman <muzzle@cs.uq.oz.au> tags: Drama Sport Sylvester Stallone Talia Shire Burt Young ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('3c809187-35e5-4f96-841e-cc02fbe62f3a','Harry Potter and the Deathly Hallows: Part 2','2011','MV5BMTY2MTk3MDQ1N15BMl5BanBnXkFtZTcwMzI4NzA2NQ@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('3c809187-35e5-4f96-841e-cc02fbe62f3a','Harry, Ron, and Hermione continue their quest of finding and destroying the Dark Lord''s three remaining Horcruxes, the magical items responsible for his immortality. But as the mystical Deathly Hallows are uncovered, and Voldemort finds out about their mission, the biggest battle begins and life as they know it will never be the same again.                Written by Jordan tags: Adventure Drama Fantasy Daniel Radcliffe Emma Watson Rupert Grint ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('8803657b-5d58-41f0-bf2a-115713539d4e','Gandhi','1982','MV5BMzJiZDRmOWUtYjE2MS00Mjc1LTg1ZDYtNTQxYWJkZTg1OTM4XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SX241_CR0,0,241,377_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('8803657b-5d58-41f0-bf2a-115713539d4e','In 1893, Gandhi is thrown off a South African train for being an Indian and traveling in a first class compartment. Gandhi realizes that the laws are biased against Indians and decides to start a non-violent protest campaign for the rights of all Indians in South Africa. After numerous arrests and the unwanted attention of the world, the government finally relents by recognizing rights for Indians, though not for the native blacks of South Africa. After this victory, Gandhi is invited back to India, where he is now considered something of a national hero. He is urged to take up the fight for India''s independence from the British Empire. Gandhi agrees, and mounts a non-violent non-cooperation campaign of unprecedented scale, coordinating millions of Indians nationwide. There are some setbacks, such as violence against the protesters and Gandhi''s occasional imprisonment. Nevertheless, the campaign generates great attention, and Britain faces intense public pressure. Too weak from World ...                Written by gavin (gunmasterM@hotmail.com) tags: Biography Drama History Ben Kingsley John Gielgud Candice Bergen ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('84044d55-e4dd-480a-ac39-0c984f2ceba5','Drishyam','2015','MV5BMTYyMjgyNDY3N15BMl5BanBnXkFtZTgwOTMzNTE5NTE@._V1_SY500_CR0,0,346,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('84044d55-e4dd-480a-ac39-0c984f2ceba5','Vijay Salgaonkar runs a cable TV network in a remote and hilly village of Goa. He lives a happy life with his wife Nandini and two daughters. A 4th grade dropout & an orphan, Vijay has worked his way towards success with his sheer hard-work & street smartness in order to provide for his family who mean the world to him. A man of few means & needs; Vijay believes life is to be lived simplistically - his wife, while she loves him devoutly, wants Vijay''s thrifty & miserly behavior to end and wants the world for her family. If there''s one thing that Vijay cannot live without, it is his passion for watching films. Such is his ''filmy'' obsession that he doesn''t mind staying up all night at his office binging on TV movies! In a bizarre turn of events; a teenage boy goes missing; he is the son of a headstrong & no-nonsense cop; IG Meera Deshmukh and the Salgaonkar family is the prime suspect! Will a humble & middle class man, be able to protect his family from oppression of the powerful? How ...                Written by Dhruvi Dokania tags: Crime Drama Mystery Ajay Devgn Shriya Saran Tabu ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('64ab2d82-9507-42d7-9312-ee2c40a4443b','Les diaboliques','1955','MV5BMGJmNmU5OTAtOTQyYy00MmM3LTk4MzUtMGFiZDYzODdmMmU4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,373,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('64ab2d82-9507-42d7-9312-ee2c40a4443b','The wife and mistress of a sadistic boarding school headmaster plot to kill him. They drown him in the bathtub and dump the body in the school''s filthy swimming pool... but when the pool is drained, the body has disappeared - and subsequent reported sightings of the headmaster slowly drive his ''killers'' (and the audience) up the wall with almost unbearable suspense...                Written by Michael Brooke <michael@everyman.demon.co.uk> tags: Crime Drama Horror Simone Signoret Véra Clouzot Paul Meurisse ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('da22cc04-da32-4a17-92b6-d520098af4da','The Bourne Ultimatum','2007','MV5BNGNiNmU2YTMtZmU4OS00MjM0LTlmYWUtMjVlYjAzYjE2N2RjXkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('da22cc04-da32-4a17-92b6-d520098af4da','Bourne is once again brought out of hiding, this time inadvertently by London-based reporter Simon Ross who is trying to unveil Operation Blackbriar--an upgrade to Project Treadstone--in a series of newspaper columns. Bourne sets up a meeting with Ross and realizes instantly they''re being scanned. Information from the reporter stirs a new set of memories, and Bourne must finally, ultimately, uncover his dark past whilst dodging The Company''s best efforts in trying to eradicate him.                Written by Corey Hatch tags: Action Mystery Thriller Matt Damon Edgar Ramírez Joan Allen ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('ca2348e4-1676-4fe3-921e-d4f8f8de96dd','Donnie Darko','2001','MV5BZWQyN2ZkODktMTBkNS00OTBjLWJhOGYtNGU4YWVkNTY0ZDZkXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,341,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('ca2348e4-1676-4fe3-921e-d4f8f8de96dd','Donnie Darko doesn''t get along too well with his family, his teachers and his classmates; but he does manage to find a sympathetic friend in Gretchen, who agrees to date him. He has a compassionate psychiatrist, who discovers hypnosis is the means to unlock hidden secrets. His other companion may not be a true ally. Donnie has a friend named Frank - a large bunny which only Donnie can see. When an engine falls off a plane and destroys his bedroom, Donnie is not there. Both the event, and Donnie''s escape, seem to have been caused by supernatural events. Donnie''s mental illness, if such it is, may never allow him to find out for sure.                Written by J. Spurlin tags: Drama Sci-Fi Thriller Jake Gyllenhaal Jena Malone Mary McDonnell ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('3e593cab-1820-4e10-9c27-1b597fd8c747','Prisoners','2013','MV5BMTg0NTIzMjQ1NV5BMl5BanBnXkFtZTcwNDc3MzM5OQ@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('3e593cab-1820-4e10-9c27-1b597fd8c747','How far would you go to protect your family? Keller Dover is facing every parent''s worst nightmare. His six-year-old daughter, Anna, is missing, together with her young friend, Joy, and as minutes turn to hours, panic sets in. The only lead is a dilapidated RV that had earlier been parked on their street. Heading the investigation, Detective Loki arrests its driver, Alex Jones, but a lack of evidence forces his release. As the police pursue multiple leads and pressure mounts, knowing his child''s life is at stake the frantic Dover decides he has no choice but to take matters into his own hands. But just how far will this desperate father go to protect his family?                Written by Warner Bros. Pictures tags: Crime Drama Mystery Hugh Jackman Jake Gyllenhaal Viola Davis ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('ef9cd025-abbc-44fc-811f-1ec67f4fcf62','Monsters, Inc.','2001','MV5BMTY1NTI0ODUyOF5BMl5BanBnXkFtZTgwNTEyNjQ0MDE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('ef9cd025-abbc-44fc-811f-1ec67f4fcf62','A city of monsters with no humans called Monstropolis centers around the city''s power company, Monsters, Inc. The lovable, confident, tough, furry blue behemoth-like giant monster named James P. Sullivan (better known as Sulley) and his wisecracking best friend, short, green cyclops monster Mike Wazowski, discover what happens when the real world interacts with theirs in the form of a 2-year-old baby girl dubbed "Boo," who accidentally sneaks into the monster world with Sulley one night. And now it''s up to Sulley and Mike to send Boo back in her door before anybody finds out, especially two evil villains such as Sulley''s main rival as a scarer, chameleon-like Randall (a monster that Boo is very afraid of), who possesses the ability to change the color of his skin, and Mike and Sulley''s boss Mr. Waternoose, the chairman and chief executive officer of Monsters, Inc.                Written by Anthony Pereyra {hypersonic91@yahoo.com} tags: Animation Adventure Comedy Billy Crystal John Goodman Mary Gibbs ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('3c140123-d362-49ce-ba6a-2fce77105f23','8½','1963','MV5BMTQ4MTA0NjEzMF5BMl5BanBnXkFtZTgwMDg4NDYxMzE@._V1_SY500_CR0,0,359,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('3c140123-d362-49ce-ba6a-2fce77105f23','Guido is a film director, trying to relax after his last big hit. He can''t get a moment''s peace, however, with the people who have worked with him in the past constantly looking for more work. He wrestles with his conscience, but is unable to come up with a new idea. While thinking, he starts to recall major happenings in his life, and all the women he has loved and left. An autobiographical film of Fellini, about the trials and tribulations of film making.                Written by Colin Tinto <cst@imdb.com> tags: Drama Fantasy Marcello Mastroianni Anouk Aimée Claudia Cardinale ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2ef19b08-196e-4e8c-ba39-ba872b40f725','The Terminator','1984','MV5BODE1MDczNTUxOV5BMl5BanBnXkFtZTcwMTA0NDQyNA@@._V1_SY500_CR0,0,333,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2ef19b08-196e-4e8c-ba39-ba872b40f725','A cyborg is sent from the future on a deadly mission. He has to kill Sarah Connor, a young woman whose life will have a great significance in years to come. Sarah has only one protector - Kyle Reese - also sent from the future. The Terminator uses his exceptional intelligence and strength to find Sarah, but is there any way to stop the seemingly indestructible cyborg ?                Written by Colin Tinto <cst@imdb.com> tags: Action Sci-Fi Arnold Schwarzenegger Linda Hamilton Michael Biehn ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('13341783-10ee-4d5f-bc6a-cbbd98ffcb38','Catch Me If You Can','2002','MV5BMTY5MzYzNjc5NV5BMl5BanBnXkFtZTYwNTUyNTc2._V1_SY359_CR0,0,242,359_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('13341783-10ee-4d5f-bc6a-cbbd98ffcb38','New Rochelle, the 1960s. High schooler Frank Abagnale Jr. idolizes his father, who''s in trouble with the IRS. When his parents separate, Frank runs away to Manhattan with $25 in his checking account, vowing to regain dad''s losses and get his parents back together. Just a few years later, the FBI tracks him down in France; he''s extradited, tried, and jailed for passing more than $4,000,000 in bad checks. Along the way, he''s posed as a Pan Am pilot, a pediatrician, and an attorney. And, from nearly the beginning of this life of crime, he''s been pursued by a dour FBI agent, Carl Hanratty. What starts as cat and mouse becomes something akin to father and son.                Written by <jhailey@hotmail.com> tags: Biography Crime Drama Leonardo DiCaprio Tom Hanks Christopher Walken ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('aaa20f20-8f27-4c65-839b-32460bd40458','The Wizard of Oz','1939','MV5BNjUyMTc4MDExMV5BMl5BanBnXkFtZTgwNDg0NDIwMjE@._V1_SY500_CR0,0,335,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('aaa20f20-8f27-4c65-839b-32460bd40458','In this charming film based on the popular L. Frank Baum stories, Dorothy and her dog Toto are caught in a tornado''s path and somehow end up in the land of Oz. Here she meets some memorable friends and foes in her journey to meet the Wizard of Oz who everyone says can help her return home and possibly grant her new friends their goals of a brain, heart and courage.                Written by Dale Roloff tags: Adventure Family Fantasy Judy Garland Frank Morgan Ray Bolger ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('7f7de336-e77a-437d-9507-b0527f2947d4','The Message','1976','MV5BMjk3YjJmYTctMTAzZC00MzE4LWFlZGMtNDM5OTMyMDEzZWIxXkEyXkFqcGdeQXVyNTI4MjkwNjA@._V1_SY428_SX289_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('7f7de336-e77a-437d-9507-b0527f2947d4','In the 7th century, Mohammed is visited by Angel Gabriel who urges him to lead the people of Mecca and worship God. But they''re exiled in Medina before returning to Mecca to take up arms against their oppressors and liberate their city in the name of God.                Written by Anonymous tags: Adventure Biography Drama Anthony Quinn Irene Papas Michael Ansara ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('ac738dd4-898b-415c-a32b-9042da7fabe0','Groundhog Day','1993','MV5BZWIxNzM5YzQtY2FmMS00Yjc3LWI1ZjUtNGVjMjMzZTIxZTIxXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY500_CR0,0,335,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('ac738dd4-898b-415c-a32b-9042da7fabe0','A weather man is reluctantly sent to cover a story about a weather forecasting "rat" (as he calls it). This is his fourth year on the story, and he makes no effort to hide his frustration. On awaking the ''following'' day he discovers that it''s Groundhog Day again, and again, and again. First he uses this to his advantage, then comes the realisation that he is doomed to spend the rest of eternity in the same place, seeing the same people do the same thing EVERY day.                Written by Rob Hartill tags: Comedy Fantasy Romance Bill Murray Andie MacDowell Chris Elliott ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('440abd6f-fa29-4b5f-978d-61612e71bfa7','Arrival','2016','MV5BMTExMzU0ODcxNDheQTJeQWpwZ15BbWU4MDE1OTI4MzAy._V1_SY500_CR0,0,320,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('440abd6f-fa29-4b5f-978d-61612e71bfa7','When mysterious spacecraft touch down across the globe, an elite team - led by expert linguist Louise Banks - is brought together to investigate. As mankind teeters on the verge of global war, Banks and the team race against time for answers - and to find them, she will take a chance that could threaten her life, and quite possibly humanity. tags: Drama Mystery Sci-Fi Amy Adams Jeremy Renner Forest Whitaker ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('82e0845b-6f4f-41a7-95ff-ad20cc32b071','John Wick: Chapter 2','2017','MV5BMjE2NDkxNTY2M15BMl5BanBnXkFtZTgwMDc2NzE0MTI@._V1_SY500_CR0,0,324,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('82e0845b-6f4f-41a7-95ff-ad20cc32b071','After returning to the criminal underworld to repay a debt, John Wick discovers that a large bounty has been put on his life. tags: Action Crime Thriller Keanu Reeves Riccardo Scamarcio Ian McShane ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('a736c788-e2d8-4a2a-99f1-0ac8ab124f13','Twelve Monkeys','1995','MV5BN2Y2OWU4MWMtNmIyMy00YzMyLWI0Y2ItMTcyZDc3MTdmZDU4XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY500_CR0,0,335,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('a736c788-e2d8-4a2a-99f1-0ac8ab124f13','An unknown and lethal virus has wiped out five billion people in 1996. Only 1% of the population has survived by the year 2035, and is forced to live underground. A convict (James Cole) reluctantly volunteers to be sent back in time to 1996 to gather information about the origin of the epidemic (who he''s told was spread by a mysterious "Army of the Twelve Monkeys") and locate the virus before it mutates so that scientists can study it. Unfortunately Cole is mistakenly sent to 1990, six years earlier than expected, and is arrested and locked up in a mental institution, where he meets Dr. Kathryn Railly, a psychiatrist, and Jeffrey Goines, the insane son of a famous scientist and virus expert.                Written by Giancarlo Cairella <vertigo@imdb.com> tags: Mystery Sci-Fi Thriller Bruce Willis Madeleine Stowe Brad Pitt ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('0f090fab-930c-40dc-aa90-f7d75b961e0c','La haine','1995','MV5BNDNiOTA5YjktY2Q0Ni00ODgzLWE5MWItNGExOWRlYjY2MjBlXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_SX350_CR0,0,350,499_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('0f090fab-930c-40dc-aa90-f7d75b961e0c','The film follows three young men and their time spent in the French suburban "ghetto," over a span of twenty-four hours. Vinz, a Jew, Saïd, an Arab, and Hubert, a black boxer, have grown up in these French suburbs where high levels of diversity coupled with the racist and oppressive police force have raised tensions to a critical breaking point. During the riots that took place a night before, a police officer lost his handgun in the ensuing madness, only to leave it for Vinz to find. Now, with a newfound means to gain the respect he deserves, Vinz vows to kill a cop if his friend Abdel dies in the hospital, due the beating he received while in police custody.                Written by b4arr2y tags: Crime Drama Vincent Cassel Hubert Koundé Saïd Taghmaoui ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('b42ad198-5ccb-462e-953b-a2ad50e86cf9','Barry Lyndon','1975','MV5BNmY0MWY2NDctZDdmMi00MjA1LTk0ZTQtZDMyZTQ1NTNlYzVjXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SY500_SX329_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('b42ad198-5ccb-462e-953b-a2ad50e86cf9','In the Eighteenth Century, in a small village in Ireland, Redmond Barry is a young farm boy in love with his cousin Nora Brady. When Nora gets engaged to the British Captain John Quin, Barry challenges him to a duel of pistols. He wins and escapes to Dublin but is robbed on the road. Without an alternative, Barry joins the British Army to fight in the Seven Years War. He deserts and is forced to join the Prussian Army where he saves the life of his captain and becomes his protégé and spy of the Irish gambler Chevalier de Balibari. He helps Chevalier and becomes his associate until he decides to marry the wealthy Lady Lyndon. They move to England and Barry, in his obsession of nobility, dissipates her fortune and makes a dangerous and revengeful enemy.                Written by Claudio Carvalho, Rio de Janeiro, Brazil tags: Adventure Drama History Ryan O''Neal Marisa Berenson Patrick Magee ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('236831b7-b797-4055-8877-75e62fc9202e','Jaws','1975','MV5BMmVmODY1MzEtYTMwZC00MzNhLWFkNDMtZjAwM2EwODUxZTA5XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY499_CR0,0,325,499_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('236831b7-b797-4055-8877-75e62fc9202e','It''s a hot summer on Amity Island, a small community whose main business is its beaches. When new Sheriff Martin Brody discovers the remains of a shark attack victim, his first inclination is to close the beaches to swimmers. This doesn''t sit well with Mayor Larry Vaughn and several of the local businessmen. Brody backs down to his regret as that weekend a young boy is killed by the predator. The dead boy''s mother puts out a bounty on the shark and Amity is soon swamped with amateur hunters and fisherman hoping to cash in on the reward. A local fisherman with much experience hunting sharks, Quint, offers to hunt down the creature for a hefty fee. Soon Quint, Brody and Matt Hooper from the Oceanographic Institute are at sea hunting the Great White shark. As Brody succinctly surmises after their first encounter with the creature, they''re going to need a bigger boat.                Written by garykmcd tags: Adventure Drama Thriller Roy Scheider Robert Shaw Richard Dreyfuss ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('f06c3f7d-c0da-48e9-b3fd-73dc7cc5b9ff','Mou gaan dou','2002','MV5BMWJmNzNmYjctMGM5OS00NGIwLTkxOTktNGI4MTdlNTk4MjgwXkEyXkFqcGdeQXVyMjQ1NjEyNzE@._V1_SY500_CR0,0,331,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('f06c3f7d-c0da-48e9-b3fd-73dc7cc5b9ff','Chan Wing Yan, a young police officer, has been sent undercover as a mole in the local mafia. Lau Kin Ming, a young mafia member, infiltrates the police force. Years later, their older counterparts, Chen Wing Yan and Inspector Lau Kin Ming, respectively, race against time to expose the mole within their midst.                Written by Aya tags: Crime Drama Mystery Andy Lau Tony Leung Chiu-Wai Anthony Chau-Sang Wong ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2ad69f73-14dc-4dfe-b2fa-6e9ecbbf420d','Zootopia','2016','MV5BOTMyMjEyNzIzMV5BMl5BanBnXkFtZTgwNzIyNjU0NzE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2ad69f73-14dc-4dfe-b2fa-6e9ecbbf420d','From the largest elephant to the smallest shrew, the city of Zootopia is a mammal metropolis where various animals live and thrive. When Judy Hopps becomes the first rabbit to join the police force, she quickly learns how tough it is to enforce the law. Determined to prove herself, Judy jumps at the opportunity to solve a mysterious case. Unfortunately, that means working with Nick Wilde, a wily fox who makes her job even harder.                Written by Jwelch5742 tags: Animation Adventure Comedy Ginnifer Goodwin Jason Bateman Idris Elba ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('fdc869f2-14bb-429d-ade1-8f6857f8ef42','The Best Years of Our Lives','1946','MV5BMTk1NTAxNzg3Nl5BMl5BanBnXkFtZTcwNjU4OTQwNw@@._V1_SY250_SX167_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('fdc869f2-14bb-429d-ade1-8f6857f8ef42','The story concentrates on the social re-adjustment of three World War II servicemen, each from a different station of society. Al Stephenson returns to an influential banking position, but finds it hard to reconcile his loyalties to ex-servicemen with new commercial realities. Fred Derry is an ordinary working man who finds it difficult to hold down a job or pick up the threads of his marriage. Having had both hands burnt off during the war, Homer Parrish is unsure that his fiancée''s feelings are still those of love and not those of pity. Each of the veterans faces a crisis upon his arrival, and each crisis is a microcosm of the experiences of many American warriors who found an alien world awaiting them when they came marching home.                Written by alfiehitchie tags: Drama Romance War Fredric March Dana Andrews Myrna Loy ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('4d35db01-95cf-4e87-a4de-701a8868370a','La battaglia di Algeri','1966','MV5BYzliNzI5NzgtNDZmNy00NGU4LTg2OTQtMDEzZTRiNDU3ZTI1XkEyXkFqcGdeQXVyMDUyOTUyNQ@@._V1_SY500_SX367_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('4d35db01-95cf-4e87-a4de-701a8868370a','A film commissioned by the Algerian government that shows the Algerian revolution from both sides. The French foreign legion has left Vietnam in defeat and has something to prove. The Algerians are seeking independence. The two clash. The torture used by the French is contrasted with the Algerian''s use of bombs in soda shops. A look at war as a nasty thing that harms and sullies everyone who participates in it.                Written by John Vogel <jlvogel@comcast.net> tags: Drama War Brahim Hadjadj Jean Martin Yacef Saadi ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('30d29619-103d-4c87-85d9-e886c76bab7a','Dog Day Afternoon','1975','MV5BODExZmE2ZWItYTIzOC00MzI1LTgyNTktMDBhNmFhY2Y4OTQ3XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX333_CR0,0,333,499_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('30d29619-103d-4c87-85d9-e886c76bab7a','Based upon a real-life story that happened in the early seventies in which the Chase Manhattan Bank in Gravesend, Brooklyn, was held siege by a gay bank robber determined to steal enough money for his male lover to undergo a sex change operation. On a hot summer afternoon, the First Savings Bank of Brooklyn is held up by Sonny and Sal, two down-and-out characters. Although the bank manager and female tellers agree not to interfere with the robbery, Sonny finds that there''s actually nothing much to steal, as most of the cash has been picked up for the day. Sonny then gets an unexpected phone call from Police Captain Moretti, who tells him the place is surrounded by the city''s entire police force. Having few options under the circumstances, Sonny nervously bargains with Moretti, demanding safe escort to the airport and a plane out of the country in return for the bank employees'' safety.                Written by alfiehitchie tags: Biography Crime Drama Al Pacino John Cazale Penelope Allen ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('33a9d438-1b49-4de7-9546-89c616cb29db','Strangers on a Train','1951','MV5BZjk5Nzg4OTMtNzNjYi00NjQzLTg3NzEtOWUyNWRkZDk4ODllL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_SY500_CR0,0,329,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('33a9d438-1b49-4de7-9546-89c616cb29db','Bruno Anthony thinks he has the perfect plot to rid himself of his hated father and when he meets tennis player Guy Haines on a train, he thinks he''s found the partner he needs to pull it off. His plan is relatively simple. Two strangers each agree to kill someone the other person wants disposed of. For example, Guy could kill his father and he could get rid of Guy''s wife Miriam, freeing him to marry Anne Morton, the beautiful daughter of a U.S. Senator. Guy dismisses it all out of hand but Bruno goes ahead with his half of the ''bargain'' and disposes of Miriam. When Guy balks, Bruno makes it quite clear that he will plant evidence to implicate Guy in her murder if he doesn''t get rid of his father. Guy had also made some unfortunate statements about Miriam after she had refused him a divorce. It all leads the police to believe Guy is responsible for the murder, forcing him to deal with Bruno''s mad ravings.                Written by garykmcd tags: Crime Film-Noir Thriller Farley Granger Robert Walker Ruth Roman ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('b30846d3-2f7b-4be9-a26d-7281ece4fbba','The Help','2011','MV5BMTM5OTMyMjIxOV5BMl5BanBnXkFtZTcwNzU4MjIwNQ@@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('b30846d3-2f7b-4be9-a26d-7281ece4fbba','Set in Mississippi during the 1960s, Skeeter (Stone) is a southern society girl who returns from college determined to become a writer, but turns her friends'' lives -- and a Mississippi town -- upside down when she decides to interview the black women who have spent their lives taking care of prominent southern families. Aibileen (Davis), Skeeter''s best friend''s housekeeper, is the first to open up -- to the dismay of her friends in the tight-knit black community. Despite Skeeter''s life-long friendships hanging in the balance, she and Aibileen continue their collaboration and soon more women come forward to tell their stories -- and as it turns out, they have a lot to say. Along the way, unlikely friendships are forged and a new sisterhood emerges, but not before everyone in town has a thing or two to say themselves when they become unwittingly -- and unwillingly -- caught up in the changing times.                Written by Walt Disney Pictures tags: Drama Emma Stone Viola Davis Octavia Spencer ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('80d6cc5c-b4fb-4641-950b-b7fedb968e75','Gangs of Wasseypur','2012','MV5BMTc5NjY4MjUwNF5BMl5BanBnXkFtZTgwODM3NzM5MzE@._V1_SY500_CR0,0,337,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('80d6cc5c-b4fb-4641-950b-b7fedb968e75','Shahid Khan is exiled after impersonating the legendary Sultana Daku in order to rob British trains. Now outcast, Shahid becomes a worker at Ramadhir Singh''s colliery, only to spur a revenge battle that passes on to generations. At the turn of the decade, Shahid''s son, the philandering Sardar Khan vows to get his father''s honor back, becoming the most feared man of Wasseypur.                Written by anonymous tags: Action Crime Drama Manoj Bajpayee Richa Chadha Nawazuddin Siddiqui ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('2c3d71ed-974f-415f-aad8-efe357a662b1','Sin City','2005','MV5BODZmYjMwNzEtNzVhNC00ZTRmLTk2M2UtNzE1MTQ2ZDAxNjc2XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY433_SX290_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('2c3d71ed-974f-415f-aad8-efe357a662b1','Four tales of crime adapted from Frank Miller''s popular comics, focusing around a muscular brute who''s looking for the person responsible for the death of his beloved Goldie, a man fed up with Sin City''s corrupt law enforcement who takes the law into his own hands after a horrible mistake, a cop who risks his life to protect a girl from a deformed pedophile, and a hitman looking to make a little cash.                Written by Tom Benton tags: Crime Thriller Mickey Rourke Clive Owen Bruce Willis ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('44c83542-50b4-4a6e-bb7f-d6d1451f26fc','Pirates of the Caribbean: The Curse of the Black Pearl','2003','MV5BMjAyNDM4MTc2N15BMl5BanBnXkFtZTYwNDk0Mjc3._V1_SY237_CR0,0,160,237_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('44c83542-50b4-4a6e-bb7f-d6d1451f26fc','This swash-buckling tale follows the quest of Captain Jack Sparrow, a savvy pirate, and Will Turner, a resourceful blacksmith, as they search for Elizabeth Swann. Elizabeth, the daughter of the governor and the love of Will''s life, has been kidnapped by the feared Captain Barbossa. Little do they know, but the fierce and clever Barbossa has been cursed. He, along with his large crew, are under an ancient curse, doomed for eternity to neither live, nor die. That is, unless a blood sacrifice is made.                Written by the lexster tags: Action Adventure Fantasy Johnny Depp Geoffrey Rush Orlando Bloom ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('71d47089-28c0-4f44-b74d-6750e027a62b','PK','2014','MV5BMTYzOTE2NjkxN15BMl5BanBnXkFtZTgwMDgzMTg0MzE@._V1_SY500_CR0,0,344,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('71d47089-28c0-4f44-b74d-6750e027a62b','P. K. is a comedy of ideas about a stranger in the city, who asks questions that no one has asked before. They are innocent, child-like questions, but they bring about catastrophic answers. People who are set in their ways for generations, are forced to reappraise their world when they see it from PK''s innocent eyes. In the process PK makes loyal friends and powerful foes. Mends broken lives and angers the establishment. P. K.''s childlike curiosity transforms into a spiritual odyssey for him and millions of others. The film is an ambitious and uniquely original exploration of complex philosophies. It is also a simple and humane tale of love, laughter and letting-go. Finally, it is a moving saga about a friendship between strangers from worlds apart.                Written by Abhijat Joshi tags: Comedy Drama Romance Aamir Khan Anushka Sharma Sanjay Dutt ');

INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('0c94baf1-7cd9-44ee-868a-cc5435a49563','The Imitation Game','2014','MV5BNjI3NjY1Mjg3MV5BMl5BanBnXkFtZTgwMzk5MDQ3MjE@._V1_SY500_CR0,0,340,500_AL_.jpg','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('0c94baf1-7cd9-44ee-868a-cc5435a49563','Based on the real life story of legendary cryptanalyst Alan Turing, the film portrays the nail-biting race against time by Turing and his brilliant team of code-breakers at Britain''s top-secret Government Code and Cypher School at Bletchley Park, during the darkest days of World War II.                Written by Studio Canal tags: Biography Drama Thriller Benedict Cumberbatch Keira Knightley Matthew Goode ');

